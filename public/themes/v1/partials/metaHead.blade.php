{!! SEO::generate() !!}
@if(Input::get('page'))
    <link rel="canonical" href="{{ URL::current() }}" />
@endif
<meta name="csrf_token" content="{{ csrf_token() }}" data-reload="{{ action('\App\Http\Controllers\AjaxController@getToken') }}">
<link rel="shortcut icon" href="{{ asset("favicon.ico") }}" type="image/x-icon">
<link rel="stylesheet" href="{{ asset("themes/v1/assets/fonts.min.css") }}">
<link rel="stylesheet" href="{{ asset("themes/v1/assets/core.min.css") }}">
<link rel="stylesheet" href="{{ asset("themes/v1/assets/app.min.css") }}">
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script src="{{ asset("themes/v1/assets/core.min.js") }}"></script>
<script src="{{ asset("themes/v1/assets/app.min.js") }}"></script>