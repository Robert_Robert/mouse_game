<?php $lister = App\Models\Lister::findBySlugOrFail('igry-nedeli'); ?>

@if($lister->games->count())
<div class="top10">
    <div class="title">Игры недели</div>
    <ol>
        @foreach($lister->games as $game)
        <li><a href="{{ $game->url() }}" title="{{ $game->title }}">{{ App\Lib\Helper::maxLen($game->title, 22) }}</a></li>
        @endforeach
    </ol>
    <div class="right_sidebsr_footer">
        <div class="right_sidebar_border"></div>
    </div>
</div>
@endif
