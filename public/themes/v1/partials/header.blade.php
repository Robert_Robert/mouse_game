<header @if(Theme::has('background')) class="watch_dogs @if(!Theme::has('background_url')) noimage @endif" @endif>
    <a name="top"></a>
    <div class="wrapper">

	    <div class="logo">
	        @if( Route::currentRouteAction() == 'App\Http\Controllers\TextController@getIndex')
	            <img src="{{ asset("themes/v1/assets/images/logo.svg") }}" title="MouseGame" alt="MouseGame"/>
	        @else
	             <a href="{{ action('\App\Http\Controllers\TextController@getIndex') }}" title="MouseGame">
                    <img src="{{ asset("themes/v1/assets/images/logo.svg") }}" title="MouseGame" alt="MouseGame"/>
                </a>
	        @endif
	    </div>
		<div class="login">
		    @if(Auth::guest())
                <div class="not_log">
                    <a href="{{ action('\App\Http\Controllers\AuthController@getLogin') }}" id="login">вход</a>/<a href="{{ action('\App\Http\Controllers\AuthController@getRegister') }}" id="registration" >регистрация</a>
                </div>
			@else
			    <div class="log">
			        <a href="{{ action('\App\Http\Controllers\ProfileController@getIndex') }}" id="personal" >{{ Auth::user()->name }}</a><a href="{{ action('\App\Http\Controllers\AuthController@getLogout') }}" id="logout"></a>
                </div>
			@endif
	    </div>
		<div class="search">
		    <form method="GET" action="{{ route('search')  }}">
		        <label for="query_field_search_form">
		            <abbr>Поиск...</abbr>
		            <input type="text" name="query" value="" id="query_field_search_form" />
		        </label>
		        <input type="submit" value="" />
		    </form>
		</div>
		<div class="social">
		    <a href="#" class="tw"></a>
		    <a href="#" class="vk"></a>
		    <a href="#" class="google"></a>
		    <a href="#" class="fb"></a>
		</div>
		<div class="menu_block">
            <div class="filter">
                {!! $MenuRusChar !!}
                {!! $MenuEngChar !!}
            </div>
            <div class="mainmenu">
                <nav>
                    <ul>
                        <li><a href="{{ route('text.view', ['slug' => 'about']) }}" title="О магазине">О магазине</a></li>
                        <li><a href="{{ route('text.view', ['slug' => 'support']) }}" title="Поддержка">Поддержка</a></li>
                        <li><a href="{{ route('text.view', ['slug' => 'faq']) }}" title="FAQ">FAQ</a></li>
                    </ul>
                </nav>
            </div>
            <a href="{{ action('\App\Http\Controllers\CartController@getIndex') }}" class="minicart" id="miniCart">
                <div class="total">{{ Cart::getTotalQuantity() }}</div>
            </a>
		</div>
	</div>
</header>
@if(Theme::has('background_url'))
    <a href="{{ Theme::get('background_url') }}" class="watch_dogs"></a>
@endif
