<footer @if(Theme::has('background')) class="watch_dogs @if(!Theme::has('background_url')) noimage @endif" @endif>
<div class="f_box">
							<div class="wrapper">
								<div class="payments">
									<span><img src="{{ asset("themes/v1/assets/images/footer_VISA.svg") }}" class="lazy" alt="Visa" height="12px"/></span>
									<span><img src="{{ asset("themes/v1/assets/images/footer_MASTERCARD.svg") }}" class="lazy" alt="MasterCard" height="21px"/></span>
									<span><img src="{{ asset("themes/v1/assets/images/footer_WEBMONEY.svg") }}" class="lazy" alt="WebMoney" height="14px"/></span>
									<span><img src="{{ asset("themes/v1/assets/images/footer_QIWI.svg") }}" class="lazy" alt="QiWi" height="35px"/></span>
									<span><img src="{{ asset("themes/v1/assets/images/footer_MTS.svg") }}" class="lazy" alt="MTS" height="14px"/></span>
									<span><img src="{{ asset("themes/v1/assets/images/footer_MEGAFON.svg") }}" class="lazy" alt="Мегафон" height="13px"/></span>
									<span><img src="{{ asset("themes/v1/assets/images/footer_YANDEX.svg") }}" class="lazy" alt="Яндекс.Деньги" height="18px"/></span>
								</div>
								<div class="to_top">
									<a href="#top" title="Наверх"></a>
								</div>
								<div class="copy">
									MOUSEGAME.ru распространяет только легальный контент<br />&copy; 2014. MG.
								</div>
								<div class="top"></div>
							</div>
						</div>
					</footer>