<div class="news">
<div class="title">НОВОСТИ</div>
    <ul class="news_list">
        @foreach(\App\Models\News::orderBy('published_at', 'DESC')->limit(5)->get() as $news)
        	<li class="item">
        		<div class="ttl"><a href="{{ route('news.view', ['slug' => $news->slug]) }}">{{ $news->title }}</a></div>
        	    <div class="intro">{{ $news->summary() }}</div>
            </li>
        @endforeach
    </ul>
</div>