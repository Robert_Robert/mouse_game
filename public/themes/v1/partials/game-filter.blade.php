<aside>
    <div class="catalog_filter @if(Route::currentRouteAction() == 'App\Http\Controllers\GameController@getIndex') no_big_games @endif">
	    <ul>
		    <li class="{{ Route::currentRouteName() == 'game.recommendations' ? 'active' : '' }}">
			    <a href="{{ route('game.recommendations') }}">Рекомендации</a>
		    </li>
			<li class="{{ Route::currentRouteName() == 'game.sale' ? 'active' : '' }}">
			    <a href="{{ route('game.sale') }}">Скидки</a>
			</li>
			<li class="{{ Route::currentRouteName() == 'game.new' ? 'active' : '' }}">
			    <a href="{{ route('game.new') }}">Новинки</a>
			</li>
			<li class="{{ Route::currentRouteName() == 'game.bestsaller' ? 'active' : '' }}">
			    <a href="{{ route('game.bestsaller') }}">Лидеры продаж</a>
			</li>
			<li class="{{ Route::currentRouteName() == 'game.preorder' ? 'active' : '' }}">
			    <a href="{{ route('game.preorder') }}">Предзаказ</a>
			</li>
		</ul>
	</div>
</aside>