<aside id="left_sidebar">
    {!! Theme::partial('leftSidebar/gameTypes') !!}
    {!! Theme::partial('leftSidebar/gamePlatforms') !!}
    {!! Theme::partial('leftSidebar/news') !!}
    {!! Theme::partial('leftSidebar/subscribe') !!}
</aside>