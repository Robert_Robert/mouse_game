@if($games instanceof Illuminate\Pagination\LengthAwarePaginator)
    <div class="pager_sort">
        @if($games->lastPage() != 1 && $games->count() > 0)
            {!! with(new App\Lib\Pagination\Presenters\FrontV1Presenter($games))->render() !!}
        @endif
        <div class="sorting">
            @if( ! Theme::get('pager_sort_disable', false))
                <div class="label">Сортировать по </div>
                <div class="select big">
                    <div class="ar">▼</div>
                    <input type="text" value="{{ Sorter::title('sort') }}" name="sort-title" class="type" readonly />
                    <input type="hidden" value="{{ Sorter::get('sort') }}" name="sort" />
                    <ul>
                        <li data-value="rate">по популярности</li>
                        <li data-value="realprice">по цене</li>
                        <li data-value="title">по названию</li>
                    </ul>
                </div>
            @else
                <div class="label">Отображать по </div>
            @endif
            <div class="select small">
                <div class="ar">▼</div>
                <input type="text" value="{{ Sorter::title('display') }}" name="display-title" class="count" readonly />
                <input type="hidden" value="{{ Sorter::get('display') }}" name="display" />
                <ul>
                    <li>16</li>
                    <li>32</li>
                    <li>64</li>
                </ul>
            </div>
            <div class="label"> позиций </div>
        </div>
        <div class="cf"></div>
    </div>
@endif
@if($games instanceof Illuminate\Support\Collection || $games instanceof Illuminate\Pagination\LengthAwarePaginator)
    <div class="catalog">
        @if($games->count() > 0)
            @foreach($games as $game)
                {!! Theme::partial('game-lists-cat', ['game' => $game, 'class' => 'object']) !!}
            @endforeach
        @else
            <p>По вашему запросу ничего не найдено. Попробуйте повторить попытку позже или уточните критерии подбора.</p>
        @endif
    </div>
@endif
@if($games instanceof Illuminate\Pagination\LengthAwarePaginator && $games->lastPage() != 1 && $games->count() > 0)
    {!! with(new App\Lib\Pagination\Presenters\FrontV1Presenter($games))->render() !!}
@endif
<div class="cf"></div>
