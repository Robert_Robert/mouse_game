<aside id="cart_sidebar">
    <?php $lister = App\Models\Lister::findBySlugOrFail('preorder'); ?>
    @if($lister->games->count())
    <div class="preorder block">
        <div class="title">Предзаказ</div>
        <ul>
            @foreach($lister->games as $game)
                <li><a href="{{ $game->url() }}" title="{{ $game->title }}">{{ App\Lib\Helper::maxLen($game->title, 25) }}</a></li>
            @endforeach
        </ul>
        <div class="sidebar_footer">
            <div class="sidebar_border"></div>
        </div>
    </div>
    @endif

    <?php $lister = App\Models\Lister::findBySlugOrFail('sale'); ?>
    @if($lister->games->count())
    <div class="actions block">
        <div class="title">Акции</div>
        <ul>
            @foreach($lister->games->sortBy('sale') as $game)
                <li>
                    <a href="{{ $game->url() }}" title="{{ $game->title }}">{{ App\Lib\Helper::maxLen($game->title, 15) }}</a>
                    <span class="percent">{{ $game->sale }}%</span>
                </li>
            @endforeach
        </ul>
        <div class="sidebar_footer">
            <div class="sidebar_border"></div>
        </div>
    </div>
    @endif
</aside>