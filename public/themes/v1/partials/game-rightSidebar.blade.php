<?php $game = Theme::get('game'); ?>
<aside id="game_sidebar">
    @if($game->price)
        <div class="bay" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <div class="price">
                <div class="cell">
                    @if(empty($game->fullprice))
                        <div class="new only"><span itemprop="price" class="number">{{ $game->price }}</span>&nbsp;<span class="rub">руб.</span></div>
                    @else
                        <div class="old"><span class="number">{{ $game->price }}</span>&nbsp;<span class="rub">руб.</span></div>
                        <div class="new"><span itemprop="price" class="number">{{ $game->fullprice }}</span>&nbsp;<span class="rub">руб.</span></div>
                    @endif
                </div>
            </div>
            <form method="GET" action="{{ action('\App\Http\Controllers\CartController@getAddItem', ['id' => $game->getKey()]) }}" id="addItemToCart">
                <input type="submit" value="Купить"/>
            </form>
            <meta itemprop="priceCurrency" content="RUB" />
            <link itemprop="availability" href="{{ $game->status->schemaName }}">
        </div>
    @endif
    <div class="{{ $game->status->slug }}"> {{ $game->status->name }} </div>
    <div class="params">
        <p><strong>Платформа:</strong> {!! Theme::widget('moreNext', ['data' => $game->platforms->toArray(), 'action' => '\App\Http\Controllers\GameController@getPlatforms'])->render() !!}</p>
        <p><strong>Дата выхода:</strong> {{ $game->release_text }} г.</p>
        <p><strong>Жанр:</strong> {!! Theme::widget('moreNext', ['data' => $game->janrs->toArray(), 'action' => '\App\Http\Controllers\GameController@getType'])->render() !!}</p>
        <p><strong>Язык:</strong> {!! Theme::widget('moreNext', ['data' => $game->langtext->toArray(), 'action' => ''])->render() !!}</p>
        <p><strong>Режимы игры:</strong> {!! Theme::widget('moreNext', ['data' => $game->mode->toArray(), 'action' => '\App\Http\Controllers\GameController@getMode'])->render() !!}</p>
        <p><strong>Регион:</strong> {!! Theme::widget('moreNext', ['data' => $game->regions->toArray(), 'action' => ''])->render() !!}</p>
        <p><strong>Разработчик:</strong> {!! a_tag(action('\App\Http\Controllers\GameController@getDevelopers', ['developer' => $game->developers->first()->slug ]), $game->developers->first()->name) !!}</p>
        <p><strong>Издатель:</strong> {!!  a_tag(action('\App\Http\Controllers\GameController@getVendors', ['vendor' => $game->vendors->first()->slug]), $game->vendors->first()->name) !!}</p>
    </div>
    @if($game->years->first())
        <a href="{{ action('\App\Http\Controllers\GameController@getYear', ['year' => $game->years->first()->slug]) }}" title="{{ $game->years->first()->full_title }}">
            <div class="reiting reiting_{{ $game->years->first()->slug }}"></div>
        </a>
    @endif
    <div class="social">
       <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
       <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
       <div class="ya-share2" data-services="vkontakte,facebook,twitter,gplus" data-counter=""></div>
    </div>
    <div class="referals"><a href="/partnership-descr.php"><img src="{{ asset("themes/v1/assets/images/referal.svg") }}" alt=""/></a></div>
</aside>