<div class="{{ $class }}" itemscope itemtype="http://schema.org/Offer">
    <a href="{{ route('game.view', ['slug' => $game->slug]) }}" itemprop="url" title="{{ $game->title }}">
        <img src="{{ $game->thumb('game-list') }}" alt="{{ $game->title }}" itemprop="image" class="lazy" title="{{ $game->slug }}" />
        <meta content="{{ $game->summary(100) }}" itemprop="description" />
        <div class="descr">
            <div class="title">
                <div class="cell">{!! $game->splitTitle(16, 2) !!}</div>
            </div>
            <div class="price">
                @if($game->realprice)
                    @if(empty($game->fullprice))
                        <b itemprop="price">{{ $game->price }}</b> <span class="rub">руб.</span>
                    @else
                        <span class="old"><s>{{ $game->price }}</s> <span class="rub">руб.</span></span>
                        <span class="new"><b itemprop="price">{{ $game->fullprice }}</b> <span class="rub">руб.</span></span>
                    @endif
                    <meta itemprop="priceCurrency" content="RUR" />
                @endif
            </div>
        </div>
    </a>
</div>