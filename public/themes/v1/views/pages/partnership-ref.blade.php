<div class="catalog_filter">
      <ul>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipUsers') }}" class="link">Привлеченные пользователи</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnership') }}" class="link">Отчет по продажам</a></li>
        <li class="active"><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipRef') }}" class="link">Источники переходов</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipDesc') }}" class="link">Описание программы</a></li>
      </ul>
    </div>
    <div class="calendar_filter">
    	<form action="#" method="post">
    	<label>Статистика за период: с</label><input type="text" name="from" value="12.01.2012" />
        <label>по</label><input type="text" name="from" value="16.08.2012" />
        <input type="submit" name="from" value="Выбрать"  />
        </form>
    </div>
    <div class="purchases">
    	<table class="ref">
        	<tr>
            	<th>HTTP refferer</th>
                <th>Дата</th>
                <th>IP адрес</th>
            </tr>
            @foreach(App\Models\ReferralSource::where('referrer_id', Auth::user()->getKey())->orderBy('created_at', 'DESC')->get() as $source)
                <tr>
                    <td class="title">{{ $source->source }}</td>
                    <td class="date">{{ $source->text_date_time_created_at }}</td>
                    <td class="num">{{ $source->ip }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="back">
    	<a href="{{ action('\App\Http\Controllers\ProfileController@getIndex') }}">Вернуться в личный кабинет</a>
    </div>