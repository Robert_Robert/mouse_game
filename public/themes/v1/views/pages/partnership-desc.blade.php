<div class="catalog_filter">
    <ul>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipUsers') }}" class="link">Привлеченные пользователи</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnership') }}" class="link">Отчет по продажам</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipRef') }}" class="link">Источники переходов</a></li>
        <li class="active"><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipDesc') }}" class="link">Описание программы</a></li>
    </ul>
</div>
<div class="content">
    <div class="filter_content">
        {!! $page->content !!}
        <p><strong>Ваш реферальный код: </strong>  <span class="ref-link">{{ action('\App\Http\Controllers\TextController@getIndex', ['code' => Auth::user()->getKey()]) }}</span></p>
    </div>
    <div class="back">
    	<a href="{{ action('\App\Http\Controllers\ProfileController@getIndex') }}">Вернуться в личный кабинет</a>
    </div>
</div>