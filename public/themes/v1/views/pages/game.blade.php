<?php
    $videoObj = empty($game->video) ? null :  new App\Lib\Video($game->video);
?>
<div class="slider game_pageslider @if(empty($game->photos)) nothumbs @endif">
    <div class="mainimage">
        @if( ! $videoObj)
            <a href="{{ $game->thumb('big') }}" class="fancy" rel="slider">
                <img src="{{ $game->thumb('main-image') }}" alt="{{ $game->title }}" title="{{ $game->title }}" itemprop="image"/>
            </a>
        @else
            <div class="embedVideo" style="background: url('{{ $game->thumb('main-image') }}')">
                {!! $videoObj->getEmbed(556, 278) !!}
            </div>
        @endif
    </div>
    @if(!empty($game->photos))
        <div class="thumbs">
            @if($videoObj)
                <div class="thumb" data-type="video" data-width="556" data-height="278" data-embed="{!! $videoObj->getVideo() !!}" data-full="{{ $game->thumb('big') }}" data-main="{{ $game->thumb('main-image') }}" data-title="{{ $game->title }}">
                    <img src="{!! $game->videoThumb('play') !!}" alt="{{ $game->title }}" class="lazy" />
                    <meta itemprop="name" content="{{ $game->title }}" />
                </div>
            @else
                <div itemscope itemtype="http://schema.org/ImageObject" class="thumb" data-type="image" data-full="{{ $game->thumb('big') }}" data-main="{{ $game->thumb('main-image') }}" data-title="{{ $game->title }}">
                    <img src="{{ $game->thumb('slide') }}" alt="{{ $game->title }}" itemprop="thumbnail" class="lazy" />
                    <meta itemprop="name" content="{{ $game->title }}" />
                </div>
            @endif
            @foreach($game->photos as $photo)
                 <div itemscope itemtype="http://schema.org/ImageObject" class="thumb" data-type="image" data-full="{{ App\Lib\Helper::image('big', $photo->src) }}" data-main="{{ App\Lib\Helper::image('main-image', $photo->src) }}" data-title="{{ $photo->title }}">
                    <img src="{{ App\Lib\Helper::image('slide', $photo->src) }}" class="lazy" alt="{{ $photo->title or $game->title }}" itemprop="thumbnail"/>
                    <meta itemprop="name" content="{{ $photo->title or $game->title }}" />
                 </div>
            @endforeach
        </div>
    @endif
</div>
<div class="page_filter">
    <ul id="game_page">
        <li class="active"><a href="#desc">Описание</a></li>
        <li><a href="#recenzia">Рецензии</a></li>
        <li><a href="#sys_req">Системные требования</a></li>
        <li><a href="#install">Установка</a></li>
    </ul>
</div>
<div class="content">
        <div class="filter_content">
            <div id="desc" class="active tabbed">
                @if(!empty($game->metacritic))
                    <div class="metacritic"><span>{{ $game->metacritic }}</span> <small>из 100</small></div>
                @endif
                    @if(!empty($game->description))
                        <div itemprop="description">{!! $game->description !!}
                            <p class="text-right"><a href="#content_resume" id="more_link" class="show_hidden">Далее...</a></p>
                            <div id="content_resume" class="content_resume hide">
                                {!! $game->content !!}
                            </div>
                        </div>
                        <div class="clr"></div>
                    @endif
                    @if($game->addons->count() > 0)
                        <div class="more addons">
                        <div class="ttl">Дополнения к этой игре:</div>
                        @foreach($game->addons as $addon)
                            {!! Theme::partial('game-lists-cat', ['game' => $addon, 'class' => 'item']) !!}
                        @endforeach
                        <div class="clr"></div>
                        </div>
                    @endif
                    @if($game->relevants->count() > 0)
                         <div class="more similar">
                        <div class="ttl">Похожие игры:</div>
                        @foreach($game->relevants as $relevant)
                            {!! Theme::partial('game-lists-cat', ['game' => $relevant, 'class' => 'item']) !!}
                        @endforeach
                        <div class="clr"></div>
                        </div>
                    @endif
            </div>
            <div id="recenzia" class="tabbed">

            </div>
            <div id="sys_req" class="tabbed">
                {!! $game->requirement !!}
            </div>
            <div id="install" class="tabbed">
                {!! $game->install !!}
            </div>
        </div>
    </div>
