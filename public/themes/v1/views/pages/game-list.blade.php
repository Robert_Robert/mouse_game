@if(!empty($bigGames))
    <div class="slider">
        @foreach($bigGames as $i => $game)
            <div class="slide" itemscope itemtype="http://schema.org/Offer">
                <a href="{{ $game->url() }}" title="{{ $game->title }}" itemprop="url">
                    <img src="{{ $game->thumb('game-card') }}" alt="{{ $game->title }}" title="{{ $game->title }}" width="369" itemprop="image" />
                </a>
                <div class="descr">
                     <meta content="{{ $game->summary(100) }}" itemprop="description" />
                    <div class="title">{{ $game->title }}</div>
                    <div class="price">
                        @if(empty($game->fullprice))
                            <span itemprop="price" class="num">{{ $game->price }}</span> &#8381;
                        @else
                            <span itemprop="price" class="num">{{ $game->fullprice }}</span> &#8381;
                        @endif
                        <meta itemprop="priceCurrency" content="RUR" />
                    </div>
                </div>
            </div>
            @if($i % 2 == 1)
                <div class="clr"></div>
            @endif
        @endforeach
    </div>
    <div class="clr"></div>
@endif

<div class="catalog_block">
	{!! Theme::partial('game-filter') !!}
    <article>
        {!! Theme::partial('game-lists', ['games' => $games]) !!}
        <div id="content_resume" class="seo_text content_resume" itemprop="description">
            @if( ! Input::get('page'))
                {!! $content or '' !!}
            @endif
        </div>
    </article>
</div>