<div class="news_banner">
    <img src="{{ $news->thumb('main-image') }}" />
</div>
<h1 class="title">{{ Theme::get('h1') }}</h1>
<div class="date">{{ $news->text_date_published_at }}</div>
<div class="clr"></div>
<div class="info">
    {!! $news->content !!}
</div>
<a class="backto" href="/news/">Перейти ко всем новостям.</a>