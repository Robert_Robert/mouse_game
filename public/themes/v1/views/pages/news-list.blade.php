<div class="newslist">
    @foreach($news as $item)
    <div class="item">
        <div class="prev">
            <a href="{{ $item->url() }}" title="{{ $item->title }}">
                <img src="{{ $item->thumb('news-list') }}" alt="{{ $item->title }}" title="{{ $item->title }}"/>
            </a>
        </div>
        <div class="body">
            <div class="date">{{ $item->text_date_published_at }}</div>
            <div class="title">
                <a href="{{ $item->url() }}" title="{{ $item->title }}">
                    {{ $item->title }}
                </a>
            </div>
            <div class="clr"></div>
            <div class="intro">
                {{ $item->summary(200) }}
            </div>
        </div>
        <div class="clr"></div>
    </div>
    @endforeach
    {!! with(new App\Lib\Pagination\Presenters\FrontV1Presenter($news))->render() !!}
</div>


<div class="cf"></div>
<div id="content_resume" class="seo_text content_resume" itemprop="description">
    @if( ! Input::get('page'))
        {!! $content or '' !!}
    @endif
</div>