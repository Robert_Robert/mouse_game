<?php $sliders = App\Models\Slider::orderBy('sort', 'ASC')->get(); ?>
@if($sliders->count() > 0)
    <figure>
	    <div class="main_slider">
		    <div class="wrp">
			    @foreach($sliders as $slider)
                    <div class="slide">
                        @if(!empty($slider->link))
                            <a href="{{ $slider->link }}" title="{{ $slider->left }}">
                                <img src="{{ asset($slider->image) }}" alt="{{ $slider->left }}"/>
                            </a>
                        @else
                            <img src="{{ asset($slider->image) }}" alt="{{ $slider->left }}"/>
                        @endif
                        <div class="descr">
                            <div class="title">{{ $slider->left }}</div>
                            <div class="price">{{ $slider->right }}</div>
                        </div>
                    </div>
				@endforeach
			</div>
		</div>
	</figure>
    <aside id="right_sidebar">
        {!! Theme::partial('topWeakGames') !!}
    </aside>

    <div class="clr"></div>
@endif

<div class="catalog_block">
	{!! Theme::partial('game-filter') !!}
    <article>
        {!! Theme::partial('game-lists', ['games' => $games]) !!}
        <div id="content_resume" class="seo_text content_resume" itemprop="description">
            @if( ! Input::get('page'))
                {!! $content or '' !!}
            @endif
        </div>
    </article>
</div>