<div class="catalog_filter">
      <ul>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipUsers') }}" class="link">Привлеченные пользователи</a></li>
        <li class="active"><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnership') }}" class="link">Отчет по продажам</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipRef') }}" class="link">Источники переходов</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipDesc') }}" class="link">Описание программы</a></li>
      </ul>
    </div>
    <div class="calendar_filter">
    	<form action="#" method="post">
    	<label>Статистика за период: с</label><input type="text" name="from" value="12.01.2012" />
        <label>по</label><input type="text" name="from" value="16.08.2012" />
        <input type="submit" name="from" value="Выбрать"  />
        </form>
    </div>
    <div class="purchases">
    	<table>
        	<tr>
            	<th>Наименование</th>
                <th><span>Дата</span></th>
                <th>Зачислено</th>
            </tr>
            <tr>
            	<td class="title">SimCity - 1 шт.</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">20 руб.</td>
            </tr>
            <tr>
            	<td class="title">Tomb Raider - 1 шт.</td>
                <td class="date">07.08.2012, 09:07</td>
                <td class="num">45 руб.</td>
            </tr>
            <tr>
            	<td class="title">Omerta-City of Gangsters - 1 шт.</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">20 руб.</td>
            </tr>
        </table>
    </div>
    <div class="back">
    	<a href="{{ action('\App\Http\Controllers\ProfileController@getIndex') }}">Вернуться в личный кабинет</a>
    </div>
