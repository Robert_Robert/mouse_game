<div class="catalog_block product_letter">
    {!! Theme::partial('game-lists', ['games' => $games]) !!}
    <div id="content_resume" class="seo_text content_resume" itemprop="description">
        @if( ! Input::get('page'))
            {!! $content or '' !!}
        @endif
    </div>
</div>