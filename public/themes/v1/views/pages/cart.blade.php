@if(Cart::isEmpty())
    <p>Ваша корзина пуста</p>
@else
    <div class="cartTable">
          <table>
            <tr>
              <th class="name">Наименование</th>
              <th>Количество</th>
              <th class="prices">Цена</th>
              <th></th>
            </tr>
            <?php $games = App\Models\Game::whereIn('id', Cart::getContent()->keys())->get(); ?>
            @foreach($games as $game)
            <?php $item = Cart::get($game->getKey()); ?>
            <tr class="product" data-id="{{ $game->getKey() }}">
              <td class="title"><a href="{{ $game->url() }}">{{ $game->title }}</a></td>
              <td class="count">
                <form method="GET" action="{{ action('\App\Http\Controllers\CartController@getSetItem', ['id' => $game->getKey()]) }}">
                    <div class="counter">
                        <span class="plus"></span><span class="minus"></span>
                        <input type="text" value="{{ $item->quantity }}" name="count" />
                    </div>
                </form>
              </td>
              <td class="price">{{ $item->price }} <span class="rub"></span></td>
              <td class="removeItem">
                <a href="{{ action('\App\Http\Controllers\CartController@getDelItem', ['id' => $game->getKey()]) }}"></a>
              </td>
            </tr>
            @endforeach
          </table>
        </div>
        <form id="cartForm" method="POST" action="{{ action('\App\Http\Controllers\CartController@postSaveOrder') }}" class="ajax">
        <div class="totals">
          <div class="count">
            <div class="label">Количество:</div>
            <div class="value"><span id="count_quantity">{{ Cart::getTotalQuantity() }}</span> шт.</div>
          </div>
          <div class="price">
            <div class="label">Общая цена:</div>
            <div class="value"><span id="sum_price">{{ Cart::getSubTotal() }}</span> <span class="rub">руб.</span></div>
          </div>
        </div>
        <div class="payments">
        	<table>
            	<tr>
                	<th></th>
                    <th colspan="2" class="pay_method">Способ оплаты</th>
                    <th class="com">Комиссия</th>
                </tr>
                <tr>
                	<td class="logo"><div class="cell visa"><img src="/themes/v1/assets/images/footer_VISA.svg" alt=""/></div></td>
                    <td class="input"><span><input type="radio" value="visa" name="method" /></span></td>
                    <td>Оплата банковской картой Visa</td>
                    <td class="com">0%</td>
                </tr>
                <tr>
                	<td class="logo"><div class="cell masterCard"><img src="/themes/v1/assets/images/footer_MASTERCARD.svg" alt=""/></div></td>
                    <td class="input"><span><input type="radio" value="mastercard" name="method" /></span></td>
                    <td>Оплата банковской картой MasterCard</td>
                    <td class="com">0%</td>
                </tr>
                <tr>
                	<td class="logo"><div class="cell webmoney"><img src="/themes/v1/assets/images/footer_WEBMONEY.svg" alt=""/></div></td>
                    <td class="input"><span><input type="radio" value="webmoney" name="method" /></span></td>
                    <td>Платежная система Webmoney</td>
                    <td class="com">0.8%</td>
                </tr>
                <tr>
                	<td class="logo"><div class="cell kiwi"><img src="/themes/v1/assets/images/footer_QIWI.svg" alt=""/></div></td>
                    <td class="input"><span><input type="radio" value="kiwi" name="method" /></span></td>
                    <td>Оплата через Киви Кошелек</td>
                    <td class="com">0%</td>
                </tr>
                <tr>
                	<td class="logo"><div class="cell mts"><img src="/themes/v1/assets/images/footer_MTS.svg" alt=""/></div></td>
                    <td class="input"><span><input type="radio" value="mts" name="method" /></span></td>
                    <td>SMS для абонентов МТС</td>
                    <td class="com">7.5%</td>
                </tr>
                <tr>
                	<td class="logo"><div class="cell megafon"><img src="/themes/v1/assets/images/footer_MEGAFON.svg" alt=""/></div></td>
                    <td class="input"><span><input type="radio" value="megafon" name="method" /></span></td>
                    <td>SMS для абонентов МегаФон</td>
                    <td class="com">11%</td>
                </tr>
                <tr>
                	<td class="logo"><div class="cell yandex"><img src="/themes/v1/assets/images/footer_YANDEX.svg" alt=""/></div></td>
                    <td class="input"><span><input type="radio" value="yandex" name="method" /></span></td>
                    <td>Платежная система Яндекс.Деньги</td>
                    <td class="com">0%</td>
                </tr>
            </table>
        </div>
        <div class="paymentTotal">
        	<div class="com">
        	    <div>Комиссия платежной системы:</div>
        	    <div class="gray">
        	        <span id="sum_commission">0</span> <span class="rub">руб.</span>
        	    </div>
        	</div>
            <div class="total">
                <div class="pay_summ">Сумма к оплате:</div>
                <div class="gray">
                    <span id="sum_pay">{{ Cart::getSubTotal() }}</span> <span class="rub">руб.</span>
                </div>
            </div>

                <input type="submit" value="Оплатить">
        </div>
    </form>
@endif