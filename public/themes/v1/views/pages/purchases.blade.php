<div class="page_filter">
      <ul>
        <li><a href="#">Неоплаченные</a></li>
        <li><a href="#">Оплаченные</a></li>
        <li class="active"><a href="#">Все</a></li>
        <li><a href="#">Отмененные</a></li>
      </ul>
    </div>
    <div class="purchases">
    	<table>
        	<tr>
            	<th><span>Наименование</span></th>
                <th><span class="underline">Дата</span></th>
                <th><span>Номер</span></th>
                <th><span class="underline">Сумма</span></th>
                <th><span class="underline">Статус</span></th>
            </tr>
            <tr>
            	<td class="title">SimCity - 1 шт.</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">24050210</td>
                <td class="sum">1199 руб.</td>
                <td class="status">В ожидании</td>
            </tr>
            <tr>
            	<td class="title">Tomb Raider - 1 шт.</td>
                <td class="date">07.08.2012, 09:07</td>
                <td class="num">22607991</td>
                <td class="sum">499 руб.</td>
                <td class="status">Завершен</td>
            </tr>
            <tr>
            	<td class="title">Omerta-City of Gangsters - 1 шт.</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">22606607</td>
                <td class="sum">299 руб.</td>
                <td class="status">Отменен</td>
            </tr>
            <tr>
            	<td class="title">Guild Wars 2 Digital Delux - 1 шт.<br />Defiance - 1 шт.<br />GTA 4 -1 шт. </td>
                <td class="date">14.05.2012, 14:44</td>
                <td class="num">18802061</td>
                <td class="sum">4379 руб.</td>
                <td class="status">Завершен</td>
            </tr>
            <tr>
            	<td class="title">Far Cry - 1 шт.</td>
                <td class="date">30.12.2011, 07:01</td>
                <td class="num">13271792</td>
                <td class="sum">599 руб.</td>
                <td class="status">Завершен</td>
            </tr>
        </table>
    </div>
    <div class="back">
    	<a href="{{ action('\App\Http\Controllers\ProfileController@getIndex') }}">Вернуться в личный кабинет</a>
    </div>
