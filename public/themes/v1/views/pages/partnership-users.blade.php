<div class="catalog_filter">
      <ul>
        <li class="active"><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipUsers') }}" class="link">Привлеченные пользователи</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnership') }}" class="link">Отчет по продажам</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipRef') }}" class="link">Источники переходов</a></li>
        <li><a href="{{ action('\App\Http\Controllers\ProfileController@getPartnershipDesc') }}" class="link">Описание программы</a></li>
      </ul>
    </div>
    <div class="purchases">
    	<table class="ref_user">
        	<tr>
            	<th>Пользователь</th>
                <th>Дата</th>
                <th>Общая сумма покупок</th>
            </tr>
            @foreach(App\Models\ReferralSource::where('referrer_id', Auth::user()->getKey())->whereNotNull('user_id')->orderBy('created_at', 'DESC')->get() as $source)
              <tr>
                <td class="title">{{ $source->user->name }}</td>
                <td class="date">{{ $source->text_date_time_created_at }}</td>
                <td class="num">{{ $source->user->ordersPayed->sum('price') }} <span class="rub">руб.</span></td>
              </tr>
                        @endforeach
        </table>
    </div>
    <div class="back">
    	<a href="{{ action('\App\Http\Controllers\ProfileController@getIndex') }}">Вернуться в личный кабинет</a>
    </div>