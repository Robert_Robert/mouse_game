@if(Session::get('status'))
    <p>{{ Session::get('status') }}</p>
@endif

@if (count($errors) > 0)
    <div class="errors">
	    <strong>Ошибка!</strong>
		<ul>
		    @foreach ($errors->all() as $error)
			    <li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<form method="POST" action="{{ action('\App\Http\Controllers\AuthController@getForgot') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="label">Введите e-mail, который Вы указывали при регистрации</div>
    {!! Form::input('text', 'email') !!}

    <div class="clr"></div>
    <input type="submit" value="Отправить" />
</form>
