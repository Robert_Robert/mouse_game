@if (count($errors) > 0)
    <div class="errors">
	    <strong>Ошибка!</strong>
		<ul>
		    @foreach ($errors->all() as $error)
			    <li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<form action="{{ action('\App\Http\Controllers\AuthController@postRegister') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="label">Введите Ваше имя/nickname</div>
    {!! Form::input('text', 'name', old('name')) !!}

    <div class="label">Введите Ваш e-mail</div>
    {!! Form::input('text', 'email', old('email')) !!}

    <div class="label">Введите Ваш пароль</div>
    {!! Form::password('password') !!}

    <div class="label">Подтвердите Ваш пароль</div>
    {!! Form::password('password_confirmation') !!}

    <div>
        {!! Form::checkbox('rule', 1, old('rule')) !!}
    	<label>Я согласен (-на) с условиями пользовательского соглашения</label>
    </div>

    {{--<div>
    	<input type="checkbox" /> <label>Я хочу получать новости о новинках и акциях по электронной почте</label>
    </div>--}}

   	<div class="clr"></div>
    <input type="submit" value="Регистрация" class="registration" />
</form>
