@if(Session::get('status'))
    <p>{{ Session::get('status') }}</p>
@endif
@if (count($errors) > 0)
    <div class="errors">
	    <strong>Ошибка!</strong>
		<ul>
		    @foreach ($errors->all() as $error)
			    <li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<form  method="POST" action="{{ action('\App\Http\Controllers\AuthController@postLogin') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="label">Введите Ваш e-mail</div>
    {!! Form::input('text', 'email', old('email')) !!}

    <div class="label">Введите Ваш пароль</div>
    {!! Form::password('password') !!}

    <a href="{{ action('\App\Http\Controllers\AuthController@getForgot') }}" class="forgot">Забыли пароль?</a>

    <input type="submit" value="Вход" name="submit" class="login" />
</form>
