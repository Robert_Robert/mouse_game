<div class="sorting">
          <div class="label">Сортировать по </div>
      	<div class="select big">
      	    <div class="ar">▼</div>
      		<input value="{{ Sorter::title('sort') }}" name="sort" class="type" readonly />
      		<ul>
      		    <li data-value="rate">по популярности</li>
      			<li data-value="price">по цене</li>
      			<li data-value="title">по названию</li>
      		</ul>
      	</div>
      	<div class="select small">
      	    <div class="ar">▼</div>
      		<input value="16" name="display" class="count" readonly />
      		<ul>
      		    <li>16</li>
      			<li>32</li>
      			<li>64</li>
      		</ul>
      	</div>
      	<div class="label"> позиций </div>
      </div>