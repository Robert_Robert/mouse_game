<?php
    $url = array();
    $nextUrl = '';
    $data = new Illuminate\Support\Collection($data);

    //foreach($data->take(2) as $item){
    foreach($data as $item){
        $title = get_key($item, 'name', '', 'is_scalar');
        $slug = get_key($item, 'slug', '', 'is_scalar');
        if(!empty($action)){
            $url[] = a_tag(action($action, $slug), $title);
        }else{
            $url[] = $title;
        }
    }
    /*if($data->count() > 2){
        $url[] = a_tag('#', 'еще...', ['class' => 'more_next']);
        $nextUrl = array();
        foreach($data->slice(2) as $item){
             $title = get_key($item, 'name', '', 'is_scalar');
             $slug = get_key($item, 'slug', '', 'is_scalar');
             if(!empty($action)){
                $nextUrl[] = a_tag(action($action, $slug), $title);
             }else{
                $nextUrl[] = $title;
             }
        }
        $nextUrl = html_wrap('span', implode(", ", $nextUrl), ['class' => 'hide']);
    }*/
    $url = implode(", ", $url) . $nextUrl;
?>

{!! $url !!}