<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
			$theme->breadcrumb()->setTemplate('<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li class="firstcrumb" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<meta itemprop="position" content="1" />
            			<a href="{{ url("/") }}" title="Главная страница MouseGame" itemprop="item">
            				<meta itemprop="name" content="Главная страница MouseGame" />
            				<img src="{{ asset("themes/v1/assets/images/home.png") }}" alt="Главная страница MouseGame" title="Главная страница MouseGame" />
            			</a>
            	 	</li>
					@foreach ($crumbs as $i => $crumb)
						@if ($i != (count($crumbs) - 1))
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<meta itemprop="position" content="{{ $i + 2 }}" />
								<a href="{{ $crumb["url"] }}" title="{{ $crumb["label"] }}" itemprop="item">
									{{ $crumb["label"] }}
									<meta itemprop="name" content="{{ $crumb["label"] }}" />
								</a>
							</li>
						@else
							<li class="lastcrumb">
								{{ $crumb["label"] }}
							</li>
						@endif
					@endforeach
				</ul>
			</div>');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
			 $theme->partialComposer('leftSidebar/gameTypes', function($view){
				 $menu = '';

				 $active = (Route::currentRouteAction() == 'App\Http\Controllers\GameController@getIndex');
				 $url = a_tag(action('\App\Http\Controllers\GameController@getIndex'), 'Все игры', ['class' => 'i-damir-all color-cyan-hover']);
				 $menu .= html_wrap('li', $url, $active ? ['class' => 'active'] : []);

				 $janrs = App\Models\Game\Janr::where('icon', '!=', '')->orderBy('sort', 'ASC')->get();
				 foreach($janrs as $janr) {
					 $active = (
						 Route::currentRouteAction() == 'App\Http\Controllers\GameController@getType' &&
						 get_key(Route::current()->parameters(), 'one', '', 'is_scalar') == $janr->slug
					 );
					 $url = action('\App\Http\Controllers\GameController@getType', ['type' => $janr->slug]);

					 $menu .= html_wrap('li',
						 a_tag($url, $janr->name, ['class' => $janr->icon]),
						 $active ? ['class' => 'active'] : []
					 );
				 }
				 $menu = html_wrap('nav', html_wrap('ul', $menu));
				 $view->with('menu', $menu);
			 });

			$theme->partialComposer('leftSidebar/gamePlatforms', function($view){
				$menu = '';

				$platforms = App\Models\Game\Platform::where('icon', '!=', '')->orderBy('sort', 'ASC')->get();
				foreach($platforms as $platform) {
					$active = (
						Route::currentRouteAction() == 'App\Http\Controllers\GameController@getPlatforms' &&
						get_key(Route::current()->parameters(), 'one', '', 'is_scalar') == $platform->slug
					);
					$url = action('\App\Http\Controllers\GameController@getPlatforms', ['type' => $platform->slug]);
					$menu .= html_wrap('li',
						a_tag($url, $platform->name, ['class' => $platform->icon]),
						$active ? ['class' => 'active'] : []
					);
				}
				$menu = html_wrap('nav', html_wrap('ul', $menu));
				$view->with('menu', $menu);
			});

             $theme->partialComposer('header', function($view){
				 $dbChars = DB::table((new App\Models\Game)->getTable())
					 ->select('char', DB::raw('count(`id`) as total'))
					 ->groupBy('char')
					 ->lists('total', 'char');

				 $chars = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я'];
				 $url = action('\App\Http\Controllers\GameController@getIndex');
				 $menu = a_tag($url, 'Все', $url == Request::url() ? ['class' => 'all current'] : ['class' => 'all']);

				 foreach($chars as $char){
					 $lowerChar = \Illuminate\Support\Str::lower($char);
					 $count = get_key($dbChars, $lowerChar, 0);
					 if($count > 0){
						 $url = action('\App\Http\Controllers\GameController@getChar', compact('lowerChar'));
						 $menu .= a_tag($url, $char, $url == Request::url() ? ['class' => 'current'] : []);
					 }else{
						 $menu .= a_tag('#', $char, ['class' => 'not']);
					 }
				 }
				 $menu = html_wrap('div', html_wrap('nav', $menu), ['class' => 'top']);
				 $view->with('MenuRusChar', $menu);

				 $chars = range('A', 'Z');
				 $menu = a_tag(action('\App\Http\Controllers\GameController@getChar', '0-9'), '0..9', ['class' => 'all']);
				 foreach($chars as $char){
					 $lowerChar = \Illuminate\Support\Str::lower($char);
					 $count = get_key($dbChars, $lowerChar, 0);
					 if($count > 0){
						 $url = action('\App\Http\Controllers\GameController@getChar', compact('lowerChar'));
						 $menu .= a_tag($url, $char, $url == Request::url() ? ['class' => 'current'] : []);
					 }else{
						 $menu .= a_tag('#', $char, ['class' => 'not']);
					 }
				 }
				 $menu = html_wrap('div', html_wrap('nav', $menu), ['class' => 'bottom']);
				 $view->with('MenuEngChar', $menu);
			 });
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);