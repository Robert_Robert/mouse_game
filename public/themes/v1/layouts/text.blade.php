<!DOCTYPE html>
<html lang="ru" itemscope @if(Theme::has('itemtype')) itemtype="{{ Theme::get('itemtype') }}" @else itemtype="http://schema.org/WebPage" @endif>
<head>
    <meta charset="utf-8" />
	{!! Theme::partial('metaHead') !!}

</head>
    <body class="body_new" @if(Theme::has('background')) style="background: #fff url('/{{ Theme::get('background') }}') no-repeat top center" @endif>
        	<div class="wrapper-border @if(Theme::has('background')) watch_dogs @if(!Theme::has('background_url')) noimage @endif @endif">
        		<div id="wrapper_all" @if(Theme::has('background')) class="watch_dogs @if(!Theme::has('background_url')) noimage @endif" @endif>
        			<div class="top_panel"></div>
                        {!! Theme::partial('header') !!}
        			    <?php $crumb = Route::currentRouteAction() != 'App\Http\Controllers\GameController@getIndex' && count(Theme::breadcrumb()->getCrumbs()); ?>
                        <div class="wrapper @if(Theme::has('background')) watch_dogs @if(!Theme::has('background_url')) noimage @endif @endif">
                        {!! Theme::partial('leftSidebar') !!}
        				<section id="middle" class="{{ ($class = Theme::get('middle_class')) ? $class : 'contentPage' }}">
                            <div class="pagetitle">
                              <h1 itemprop="headline">{{ Theme::get('h1') }}</h1>
                            </div>
                            @if($crumb)
                                {!! Theme::breadcrumb()->render() !!}
                            @endif
                            <div class="content">
                                {!! Theme::content() !!}
                            </div>
                        </section>
        				<div class="clr"></div>
        			</div>
        			<div id="footer_guarantor"></div>
        		</div>
            </div>
        {!! Theme::partial('footer') !!}
    </body>
</html>