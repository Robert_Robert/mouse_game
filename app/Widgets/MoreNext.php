<?php namespace App\Widgets;

use Teepluss\Theme\Theme;
use Teepluss\Theme\Widget;

class MoreNext extends Widget {

    /**
     * Widget template.
     *
     * @var string
     */
    public $template = 'moreNext';

    /**
     * Watching widget tpl on everywhere.
     *
     * @var boolean
     */
    public $watch = false;

    /**
     * Arrtibutes pass from a widget.
     *
     * @var array
     */
    public $attributes = array();

    /**
     * Turn on/off widget.
     *
     * @var boolean
     */
    public $enable = true;

    /**
     * Code to start this widget.
     *
     * @return void
     */
    public function init(Theme $theme){}

    /**
     * Logic given to a widget and pass to widget's view.
     *
     * @return array
     */
    public function run(){
        return $this->getAttributes();
    }

}