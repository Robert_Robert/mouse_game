<?php namespace App\Services\Validation;

use Illuminate\Validation\Validator;
use App\Models\Keyword;

class CustomValidation extends Validator {

	private $errorSeoKeys = [];

	public function validateSeoKeywords($attribute, $value, $parameters)
	{
		$seoId = (int)get_key($parameters, 0, null, 'is_scalar');

		$keys = (is_scalar($value)) ? explode(',' ,$value) : $value;
		$keys = is_array($keys) ? $keys : [];

		$this->errorSeoKeys[$attribute] = [];
		$keys = array_clean(array_map('trim', $keys), array('', 0, null));
		if(!empty($keys)){
			$existsKeys = Keyword::whereIn('name', $keys);
			$existsKeys->whereHas('seo', function($q) use($seoId){
				$q->where('seo.id','<>',$seoId);
			});
			if($existsKeys->count()){
				$this->errorSeoKeys[$attribute] = $existsKeys->get()->lists('name')->all();
			}
		}
		return !(bool)$this->errorSeoKeys[$attribute];
	}

	protected function replaceSeoKeywords($message, $attribute, $rule, $parameters)
	{
		$keys = get_key($this->errorSeoKeys, $attribute, [], 'is_array');
		return str_replace(':seokeywords', implode(", ", $keys), $message);
	}
} 