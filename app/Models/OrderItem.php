<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

class OrderItem extends Model{
	use LocalizedDateTrait;

	protected $table = 'order_items';

	protected $fillable = array(
		'title',
		'price',
		'quantity',
		'order_id',
		'keys',
		'sent',
		'data',
		'created_at',
		'updated_at'
	);

	public function status(){
		return $this->belongsTo(Order::class, 'status_id');
	}
}
