<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use AgelxNash\SEOTools\Traits\SEOTrait as SEOToolsTrait;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Interfaces\SeoInterface;
use Watson\Validating\ValidatingTrait;
use App\Lib\Helper;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

/**
 * Class News
 * @package App\Models
 * #@property int $id
 * #@property string $title
 * #@property string $description
 * @property string $image
 * @property string $content
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 */
class News extends Model implements SluggableInterface, SeoInterface{
	use ValidatingTrait, SluggableTrait, SEOToolsTrait, ReplaceBuilder, LocalizedDateTrait;

	protected $table = 'news';

	protected $sluggable = array(
		'build_from' => 'title',
		'save_to'    => 'slug',
	);
	/**
	 * @var array
	 */
	protected $dates = array(
		'published_at'
	);

	protected $fillable = array(
		'title',
		'description',
		'image',
		'content',
		'slug',
		'published_at',
		'created_at',
		'updated_at'
	);


	protected $rules = [
		'published_at' => 'required',
		'title'   => 'required|unique:news',
		'content'    => 'required|min:3'
	];

	protected $defaultSeoFields = [
		'priority' => 0.3,
		'robots' => 'index,follow',
		'state' => 'static',
		'frequency' => 'monthly'
	];

	public function thumb($tpl){
		return Helper::image($tpl, $this->image);
	}

	public function url($method = null){
		switch($method){
			case 'lists':{
				$out = action('\App\Http\Controllers\NewsController@getIndex');
				break;
			}
			default:{
				if($this->exists){
					$out = action('\App\Http\Controllers\NewsController@showNews', ['slug' => $this->slug]);
				}else{
					$out = '#';
				}
			}
		}
		return $out;
	}

	/**
	 * @return array
	 */
	public function getImageFields()
	{
		return [
			'image' => ['news/', function($directory, $originalName, $extension)
			{
				return $originalName;
			}]
		];
	}

	/**
	 * @param $query
	 * @return mixed
	 */
	public function scopeDefaultSort($query)
	{
		return $query->orderBy('published_at', 'DESC');
	}

	/**
	 * @param int $len
	 * @return string
	 */
	public function summary($len = 50){
		$out = strip_tags($this->description);
		if(empty($out)){
			return (new \AgelxNash\SEOTools\Summary($this->content, 'notags,len:'.$len))->run(1);
		}
		return $out;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function seo()
	{
		return $this->morphOne(Seo::class, 'seoble', 'document_type', 'document_id');
	}

	public function getTextDatePublishedAtAttribute()
	{
		return $this->takeCarbonAttributes("published_at", '%d %f %Y');
	}

	public function getHumanPublishedAtAttribute()
	{
		return $this->getHumanTimestampAttribute("published_at");
	}
}
