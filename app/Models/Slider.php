<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;
use SleepingOwl\Admin\Traits\OrderableModel;
use App\Lib\Helper;

/**
 * Class Slider
 * @package App\Models\Slider
 *
 * @property int $id
 * @property string $left Текст слева
 * @property string $right Текст справа
 * @property string $image Картинка
 * @property string $link Ссылка
 * @property int $sort Сортировка
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Slider extends Model{
	use LocalizedDateTrait, OrderableModel;

	protected $table = 'sliders';

	protected $fillable = array(
		'left',
		'right',
		'image',
		'link',
		'sort',
		'created_at',
		'updated_at'
	);

	public function getOrderField() {
		return 'sort';
	}

	public function thumb($tpl){
		return Helper::image($tpl, $this->image);
	}
}