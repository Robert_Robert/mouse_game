<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use AgelxNash\SEOTools\Traits\SEOTrait as SEOToolsTrait;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Interfaces\SeoInterface;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

/**
 * Class Text
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $content
 * @property mixed $slug
 * @property bool $system
 * @property bool $hide
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Seo $seo
 * @property string $title
 */
class Text extends Model implements SluggableInterface, SeoInterface{
	use SluggableTrait, SEOToolsTrait, ReplaceBuilder, LocalizedDateTrait;

	protected $table = 'texts';

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);

	protected $fillable = array(
		'name',
		'content',
		'slug',
		'system',
		'hide',
		'created_at',
		'updated_at',

		'seo'
	);

	protected $defaultSeoFields = [
		'priority' => 0.5,
		'robots' => 'index,follow',
		'state' => 'static',
		'frequency' => 'monthly'
	];

	public function url($mode = null){
		switch($mode){
			case 'news':
				$out = action('\App\Http\Controllers\NewsController@getIndex');
				break;
			case 'search':
				$out = route('search');
				break;
			default:
				switch($this->slug){
					case 'purchases':
						$out = action('\App\Http\Controllers\ProfileController@getPurchases');
						break;
					case 'profile-edit':
						$out = action('\App\Http\Controllers\ProfileController@getEdit');
						break;
					case 'partnership-descr':
						$out = action('\App\Http\Controllers\ProfileController@getPartnershipDesc');
						break;
					case 'partnership-users':
						$out = action('\App\Http\Controllers\ProfileController@getPartnershipUsers');
						break;
					case 'partnership-ref':
						$out = action('\App\Http\Controllers\ProfileController@getPartnershipRef');
						break;
					case 'partnership':
						$out = action('\App\Http\Controllers\ProfileController@getPartnership');
						break;
					case 'profile':
						$out = action('\App\Http\Controllers\ProfileController@getIndex');
						break;
					default:
						$out = action('\App\Http\Controllers\TextController@showText', ['type' => $this->slug]);
				}
		}
		return $out;
	}
	public function getTitleAttribute(){
		return $this->name;
	}
	public function setTitleAttribute($val){
		$this->name = $val;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function seo()
	{
		return $this->morphOne(Seo::class, 'seoble', 'document_type', 'document_id');
	}
}
