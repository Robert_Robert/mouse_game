<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Interfaces\SeoInterface;
use AgelxNash\SEOTools\Traits\SEOTrait as SEOToolsTrait;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

/**
 * Class User
 * @package App\Models
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $roles
 * @property int $userpermissions
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract, SeoInterface {

	use Authenticatable, CanResetPassword, HasRoleAndPermission, ReplaceBuilder, SEOToolsTrait, LocalizedDateTrait;
		/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'roles', 'userpermissions', 'confirmed', 'confirmation_code', 'reg_referer_page', 'reg_ip', 'referrer_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	protected $defaultSeoFields = [
		'priority' => 0.1,
		'robots' => 'noindex,follow',
		'state' => 'dynamic',
		'frequency' => 'monthly'
	];

	public function url($mode = null){
		return '#';
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function seo()
	{
		return $this->morphOne(Seo::class, 'seoble', 'document_type', 'document_id');
	}

	/**
	 * Пользователь который меня пригласил
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function referrer()
	{
		return $this->belongsTo(User::class, 'referrer_id');
	}

	/**
	 * Пользователи которые зарегистрировались
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function referrals()
	{
		return $this->hasMany(User::class, 'referrer_id');
	}
	public function confirmedReferral()
	{
		return $this->hasMany(User::class, 'referrer_id')->where('confirmed', '=', 1);
	}

	/**
	 * @param $value
	 */
	public function setPasswordAttribute($value)
	{
		if ( ! empty($value))
		{
			$this->attributes['password'] = \Hash::make($value);
		}
	}

	/**
	 * @param $roles
	 */
	public function setRolesAttribute($roles)
	{

		$this->detachAllRoles();
		if ( ! $roles) return;
		if ( ! $this->exists) $this->save();

		$this->attachRole($roles);
	}

	/**
	 * @param $permission
	 */
	public function setUserpermissionsAttribute($permission)
	{
		$this->detachAllPermissions();
		if ( ! $permission) return;
		if ( ! $this->exists) $this->save();

		$this->attachPermission($permission);
	}

	/**
	 * @return bool
	 */
	public function isSudoWithoutPermission(){
		return !$this->userpermissions()->count();
	}

	public function orders(){
		return $this->hasMany(Order::class, 'user_id');
	}

	public function ordersPayed(){
		return $this->hasMany(Order::class, 'user_id')->where('pay', '=', 1);
	}

}
