<?php namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;
use Bican\Roles\Models\Permission as Model;

/**
 * Class Permission
 * @package App\Models
 *
 * @property string $name Название прав доступа
 * @property string $slug Псевдоним прав доступа
 * @property string $description Описание прав доступа
 * @property string $model
 * @property \Carbon\Carbon $created_at Дата создания прав доступа
 * @property \Carbon\Carbon $updated_at Дата обновления прав доступа
 */
class Permission extends Model implements SluggableInterface{
	use SluggableTrait, ReplaceBuilder, LocalizedDateTrait;

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);
}