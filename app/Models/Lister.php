<?php namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Traits\SEOTrait;
use AgelxNash\SEOTools\Interfaces\SeoInterface;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;
use App\Models as Models;

/**
 * Class Lister
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Lister extends Model implements SluggableInterface, SeoInterface{
	use SluggableTrait, SEOTrait, ReplaceBuilder, LocalizedDateTrait;

	protected $table = 'listers';

	protected $fillable = array(
		'name',
		'slug',
		'created_at',
		'updated_at'
	);

	protected $sluggable = [
		'build_from' => 'name',
		'save_to'    => 'slug',
	];

	protected $defaultSeoFields = [
		'priority' => 0.4,
		'robots' => 'index,follow',
		'state' => 'dynamic',
		'frequency' => 'weekly'
	];

	public function url($method = null){
		return '#';
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function seo()
	{
		return $this->morphOne(Models\Seo::class, 'seoble', 'document_type', 'document_id');
	}


	/**
	 * @return array
	 */
	public static function getList(){
		return self::all()->lists('name', 'id')->all();
	}
	public function getTitleAttribute(){
		return $this->name;
	}
	public function setTitleAttribute($val){
		$this->name = $val;
	}

	/**************
	 * Games
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function games()
	{
		return $this->belongsToMany('App\Models\Game')->withPivot('id')->orderBy('pivot_id');
	}

	/**
	 * @param $list
	 * @return bool|void
	 */
	public function attachGames($list)
	{
		if (!$this->games()->get()->contains($list)) {
			return $this->games()->attach($list);
		}

		return true;
	}

	/**
	 * @param $list
	 * @return int
	 */
	public function detachGames($list)
	{
		return $this->games()->detach($list);
	}

	/**
	 * @return int
	 */
	public function detachAllGames()
	{
		return $this->games()->detach();
	}

	/**
	 * @param $query
	 * @param $list
	 * @return mixed
	 */
	public function scopeGames($query, $list)
	{
		return $query->whereHas('game', function ($query) use ($list) {
			$query->where('slug', $list);
		});
	}

	/**
	 * @param $lists
	 */
	public function setGamesAttribute($lists)
	{
		if ( ! $this->exists) $this->save();
		( ! $lists) ? $this->detachAllGames() : $this->games()->sync($lists);
	}
}