<?php namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOTools\Traits\SEOTrait;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Interfaces\SeoInterface;
use App\Lib\Helper;
use Exception;
use AgelxNash\Sorter\Facades\Sorter;

/**
 * @property mixed $title
 * @property mixed $content
 * @property mixed $description
 * @property mixed $image
 * @property mixed $rate
 * @property mixed $metacritic
 * @property mixed $price
 * @property mixed $fullprice
 * @property mixed $created_at
 * @property mixed $updated_at
 * @property mixed $requirement
 * @property mixed $install
 * @property mixed $video
 * @property mixed $release
 * @property mixed $slug
 * @property mixed $platforms
 * @property mixed $janrs
 * @property mixed $mode
 * @property mixed $lists
 * @property mixed $langtext
 * @property mixed $regions
 * @property mixed $seo
 * @property string $char
 * @property bool $published
 * @property bool $hiddened
 * @property string $realprice
 * @property string $videoImage
 * @property string $list_title
 */

class Game extends Model implements SluggableInterface, SeoInterface{
	use SluggableTrait, SEOTrait, ReplaceBuilder, LocalizedDateTrait;

	protected $table = 'games';

	protected $fillable = array(
		'title',
		'content',
		'description',
		'image',
		'rate',
		'metacritic',
		'price',
		'fullprice',
		'realprice',
		'created_at',
		'updated_at',

		'requirement',
		'install',
		'release',
		'video',
		'char',

		'platforms',
		'janrs',
		'mode',
		'langtext',
		'regions',
		'lists',
		'vendors',
		'developers',
		'years',
		'published',

		'seo',
		'slug',

		'photos',
		'list_title',
		'status_id',
		'hiddened'
	);

	protected $sluggable = [
		'build_from' => 'title',
		'save_to'    => 'slug',
	];

	protected $defaultSeoFields = [
		'priority' => 0.9,
		'robots' => 'index,follow',
		'state' => 'dynamic',
		'frequency' => 'weekly'
	];

	/**
	 * @param int $len
	 * @return string
	 */
	public function summary($len = 50){
		$out = strip_tags($this->description);
		if(empty($out)){
			return (new \AgelxNash\SEOTools\Summary($this->content, 'notags,len:'.$len))->run(1);
		}
		return $out;
	}

	public function getSaleAttribute(){
		$out = $this->fullprice > 0 ? (round($this->fullprice * 100 / $this->price) - 100) : 0;
		return $out == -100 ? -99 : $out;
	}
	public function scopeSortPagination($query)
	{
		switch(Sorter::get('sort')){
			case 'realprice':
				$asc = true;
				$addSort = function($q){
					return $q->orderBy('rate', 'DESC')
						->orderBy('title', 'ASC');
				};
				break;
			case 'title':
				$asc = true;
				$addSort = function($q){
					return $q->orderBy('price', 'ASC')
						->orderBy('rate', 'DESC');
				};
				break;
			case 'rate':
			default:
				$asc = false;
				$addSort = function($q){
					return $q->orderBy('price', 'ASC')
						->orderBy('title', 'ASC');
				};
		}
		return $addSort($query->orderBy(Sorter::get('sort'), $asc ? 'asc' : 'desc'))
			->where('published', '=', 1)
			->paginate(Sorter::get('display'));
	}

	public function status(){
		return $this->belongsTo(Status::class, 'status_id');
	}

	public function getPhotosAttribute($value){
		try {
			$images = json_decode($value);
		} catch (Exception $e) {
			$images = [];
		}
		if (! is_array($images)) {
			$images =[];
		}
		return $images;
	}

	public function setPhotosAttribute($photos){
		$cadena = json_encode($photos);
	 	$this->attributes['photos'] = $cadena;
	}

	public function getPhotosCuratedAttribute() {
		$result = array();
		foreach($this->photos as $image) {
			$image->src = str_replace('uploads/', '', $image->src);
			$result[] = $image;
		}
		return $result;
	}

	public function url($method = null){
		switch($method){
			case 'lists':{
				$out = action('\App\Http\Controllers\GameController@getIndex');
				break;
			}
			default:{
				if($this->exists){
					$out = action('\App\Http\Controllers\GameController@showGame', ['game' => $this->slug]);
				}else{
					$out = '#';
				}
			}
		}
		return $out;
	}
	public function getRealListTitleAttribute(){
		return empty($this->list_title) ? $this->title : $this->list_title;
	}
	public function splitTitle($len = 18, $lines = null){
		return Helper::splitTitle($this->real_list_title, $len, $lines);
	}
	public function thumb($tpl){
		return Helper::image($tpl, $this->image);
	}
	public function getVideoImageAttribute(){
		return $this->image;
	}
	public function videoThumb($tpl){
		return Helper::image($tpl, $this->videoImage);
	}
	public function getUserPriceAttribute(){
		return $this->realprice;//empty($this->fullprice) ? $this->price : $this->fullprice;
	}

	public function getReleaseTextAttribute(){
		return $this->takeCarbonAttributes('release', '%d %f %Y', 'Y-m-d');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function seo()
	{
		return $this->morphOne(Seo::class, 'seoble', 'document_type', 'document_id');
	}

	/**************
	 * Vendors
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function vendors()
	{
		return $this->belongsToMany('App\Models\Game\Vendor', 'game_vendors')->withPivot('updated_at', 'id')->orderBy('pivot_updated_at')->orderBy('pivot_id');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachVendors($data)
	{
		if (!$this->vendors()->get()->contains($data)) {
			return $this->vendors()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachVendors($data)
	{
		return $this->vendors()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllVendors()
	{
		return $this->vendors()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopeVendors($query, $data)
	{
		return $query->whereHas('game_vendors', function ($query) use ($data) {
			$query->where('name', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setVendorsAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllVendors() : $this->vendors()->sync($data);
	}

	/**************
	 * Developers
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function developers()
	{
		return $this->belongsToMany('App\Models\Game\Developer', 'game_developers')->withPivot('updated_at', 'id')->orderBy('pivot_updated_at')->orderBy('pivot_id');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachDevelopers($data)
	{
		if (!$this->developers()->get()->contains($data)) {
			return $this->developers()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachDevelopers($data)
	{
		return $this->developers()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllDevelopers()
	{
		return $this->developers()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopeDevelopers($query, $data)
	{
		return $query->whereHas('game_developers', function ($query) use ($data) {
			$query->where('name', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setDevelopersAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllDevelopers() : $this->developers()->sync($data);
	}

	/**************
	 * Years
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function years()
	{
		return $this->belongsToMany('App\Models\Game\Year', 'game_year')->withPivot('updated_at', 'id')->orderBy('pivot_updated_at')->orderBy('pivot_id');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachYears($data)
	{
		if (!$this->years()->get()->contains($data)) {
			return $this->years()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachYears($data)
	{
		return $this->years()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllYears()
	{
		return $this->years()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopeYears($query, $data)
	{
		return $query->whereHas('game_years', function ($query) use ($data) {
			$query->where('name', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setYearsAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllYears() : $this->years()->sync($data);
	}

	/**************
	 * List
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function lists()
	{
		return $this->belongsToMany(Lister::class)->withPivot('updated_at', 'id')->orderBy('pivot_updated_at')->orderBy('pivot_id');
	}

	/**
	 * @param $list
	 * @return bool|void
	 */
	public function attachLists($list)
	{
		if (!$this->lists()->get()->contains($list)) {
			return $this->lists()->attach($list);
		}

		return true;
	}

	/**
	 * @param $list
	 * @return int
	 */
	public function detachLists($list)
	{
		return $this->lists()->detach($list);
	}

	/**
	 * @return int
	 */
	public function detachAllLists()
	{
		return $this->lists()->detach();
	}

	/**
	 * @param $query
	 * @param $list
	 * @return mixed
	 */
	public function scopeLists($query, $list)
	{
		return $query->whereHas('game_lists', function ($query) use ($list) {
			$query->where('slug', $list);
		});
	}

	/**
	 * @param $lists
	 */
	public function setListsAttribute($lists)
	{
		if ( ! $this->exists) $this->save();
		( ! $lists) ? $this->detachAllLists() : $this->lists()->sync($lists);
	}

	/**************
	 * Mode
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function mode()
	{
		return $this->belongsToMany('App\Models\Game\Mode')->orderBy('sort');
	}

	/**
	 * @param $lang
	 * @return bool|void
	 */
	public function attachMode($lang)
	{
		if (!$this->mode()->get()->contains($lang)) {
			return $this->mode()->attach($lang);
		}

		return true;
	}

	/**
	 * @param $lang
	 * @return int
	 */
	public function detachMode($lang)
	{
		return $this->mode()->detach($lang);
	}

	/**
	 * @return int
	 */
	public function detachAllMode()
	{
		return $this->mode()->detach();
	}

	/**
	 * @param $query
	 * @param $lang
	 * @return mixed
	 */
	public function scopeMode($query, $lang)
	{
		return $query->whereHas('game_mode', function ($query) use ($lang) {
			$query->where('slug', $lang);
		});
	}

	/**
	 * @param $langs
	 */
	public function setModeAttribute($langs)
	{
		if ( ! $this->exists) $this->save();
		( ! $langs) ? $this->detachAllMode() : $this->mode()->sync($langs);
	}

	/**************
	 * LANG TEXT
	*************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function langtext()
	{
		return $this->belongsToMany('App\Models\Game\Lang\Text')->orderBy('sort');
	}

	/**
	 * @param $lang
	 * @return bool|void
	 */
	public function attachLangtext($lang)
	{
		if (!$this->langtext()->get()->contains($lang)) {
			return $this->langtext()->attach($lang);
		}

		return true;
	}

	/**
	 * @param $lang
	 * @return int
	 */
	public function detachLangtext($lang)
	{
		return $this->langtext()->detach($lang);
	}

	/**
	 * @return int
	 */
	public function detachAllLangtext()
	{
		return $this->langtext()->detach();
	}

	/**
	 * @param $query
	 * @param $lang
	 * @return mixed
	 */
	public function scopeLangtext($query, $lang)
	{
		return $query->whereHas('game_text_lang', function ($query) use ($lang) {
			$query->where('slug', $lang);
		});
	}

	/**
	 * @param $langs
	 */
	public function setLangtextAttribute($langs)
	{
		if ( ! $this->exists) $this->save();
		( ! $langs) ? $this->detachAllLangtext() : $this->langtext()->sync($langs);
	}

	/**************
	 * PLATFORM
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function platforms()
	{
		return $this->belongsToMany('App\Models\Game\Platform')->orderBy('sort');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachPlatforms($data)
	{
		if (!$this->platforms()->get()->contains($data)) {
			return $this->platforms()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachPlatforms($data)
	{
		return $this->platforms()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllPlatforms()
	{
		return $this->platforms()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopePlatforms($query, $data)
	{
		return $query->whereHas('game_platform', function ($query) use ($data) {
			$query->where('slug', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setPlatformsAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllPlatforms() : $this->platforms()->sync($data);
		/*$this->platforms()->detach();
		if ( ! $data) return;
		if ( ! $this->exists) $this->save();
		$this->platforms()->attach($data);*/
	}

	/**************
	 * JANR
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function janrs()
	{
		return $this->belongsToMany('App\Models\Game\Janr')->orderBy('sort');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachJanrs($data)
	{
		if (!$this->janrs()->get()->contains($data)) {
			return $this->janrs()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachJanrs($data)
	{
		return $this->janrs()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllJanrs()
	{
		return $this->janrs()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopeJanrs($query, $data)
	{
		return $query->whereHas('game_janr', function ($query) use ($data) {
			$query->where('slug', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setJanrsAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllJanrs() : $this->janrs()->sync($data);
	}

	/**************
	 * REGIONS
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function regions()
	{
		return $this->belongsToMany('App\Models\Game\Region')->orderBy('sort');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachRegions($data)
	{
		if (!$this->regions()->get()->contains($data)) {
			return $this->regions()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachRegions($data)
	{
		return $this->regions()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllRegions()
	{
		return $this->regions()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopeRegions($query, $data)
	{
		return $query->whereHas('game_region', function ($query) use ($data) {
			$query->where('slug', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setRegionsAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllRegions() : $this->regions()->sync($data);
	}

	/**************
	 * Addons
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function addons()
	{
		return $this->belongsToMany(Game::class, 'game_addon', 'addon_id', 'game_id')->withPivot('updated_at', 'id')->orderBy('pivot_updated_at')->orderBy('pivot_id');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachAddons($data)
	{
		if (!$this->addons()->get()->contains($data)) {
			return $this->addons()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachAddons($data)
	{
		return $this->addons()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllAddons()
	{
		return $this->addons()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopeAddons($query, $data)
	{
		return $query->whereHas('games', function ($query) use ($data) {
			$query->where('slug', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setAddonsAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllAddons() : $this->addons()->sync($data);
	}

	/**************
	 * Relevants
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function relevants()
	{
		return $this->belongsToMany(Game::class, 'game_relevant', 'relevant_id', 'game_id')->withPivot('updated_at', 'id')->orderBy('pivot_updated_at')->orderBy('pivot_id');
	}

	/**
	 * @param $data
	 * @return bool|void
	 */
	public function attachRelevants($data)
	{
		if (!$this->relevants()->get()->contains($data)) {
			return $this->relevants()->attach($data);
		}

		return true;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function detachRelevants($data)
	{
		return $this->relevants()->detach($data);
	}

	/**
	 * @return int
	 */
	public function detachAllRelevants()
	{
		return $this->relevants()->detach();
	}

	/**
	 * @param $query
	 * @param $data
	 * @return mixed
	 */
	public function scopeRelevants($query, $data)
	{
		return $query->whereHas('games', function ($query) use ($data) {
			$query->where('slug', $data);
		});
	}

	/**
	 * @param $data
	 */
	public function setRelevantsAttribute($data)
	{
		if ( ! $this->exists) $this->save();
		( ! $data) ? $this->detachAllRelevants() : $this->relevants()->sync($data);
	}

	/****************
	 * ADMIN PANEL
	 ***************/
	/**
	 * @return array
	 */
	public function getImageFields()
	{
		return [
			'image' => ['game/', function($directory, $originalName, $extension)
			{
				return $originalName;
			}]
		];
	}

	/**
	 * @return array
	 */
	public static function getList(){
		return self::all()->lists('title', 'id')->all();
	}

	/**************
	 * BountyFree
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function bountyFree()
	{
		return $this->belongsToMany(Bounty\BountyFree::class)
			->withPivot('updated_at', 'id')
			->orderBy('pivot_updated_at')
			->orderBy('pivot_id')
			->where('active_from', '<=', date('Y-m-d'))
			->where('active_to', '>=', date('Y-m-d'));
	}

	/**************
	 * BountyFree
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function bountyFreeCondition()
	{
		return $this->belongsToMany(Bounty\BountyFree::class, 'bounty_free_condition')
			->withPivot('updated_at', 'id')
			->orderBy('pivot_updated_at')
			->orderBy('pivot_id')
		->where('active_from', '<=', date('Y-m-d'))
			->where('active_to', '>=', date('Y-m-d'));
	}
}
