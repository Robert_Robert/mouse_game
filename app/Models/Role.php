<?php namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;
use Bican\Roles\Models\Role as Model;

/**
 * Class Role
 * @package App\Models
 *
 * @property string $name Название роли
 * @property string $slug Псевдоним роли
 * @property string description Описание роли
 * @property int $level Уровень роли (не используется в моих приложениях)
 * @property \Carbon\Carbon $created_at Дата создания роли
 * @property \Carbon\Carbon $updated_at Дата обновления роли
 */
class Role extends Model implements SluggableInterface{
	use SluggableTrait, ReplaceBuilder, LocalizedDateTrait;

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);

	/**
	 * @return array
	 */
	public static function getList(){
		return Role::all()->lists('name', 'id')->all();
	}

	public function users(){
		return $this->belongsToMany(User::class);
	}
}