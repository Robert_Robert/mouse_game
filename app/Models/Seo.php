<?php namespace App\Models;

use AgelxNash\SEOTools\Models\Seo as Model;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

/**
 * Class Seo
 * @package App\Models
 *
 */
class Seo extends Model {
	use LocalizedDateTrait;

	protected $fillable = array(
		'title',
		'description',
		'document_id',
		'document_type',
		'created_at',
		'updated_at',
		'priority',
		'frequency',
		'robots',
		'state',
		'h1',
		'background',
		'background_url',

		'keywords'
	);
}