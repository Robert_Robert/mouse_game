<?php namespace App\Models\Bounty;

use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

class BountyFree extends Model{
	use LocalizedDateTrait;

	protected $table = 'bounty_free';

	protected $fillable = array(
		'active_to',
		'active_from',

		'created_at',
		'updated_at'
	);

	/*** bounty_free_condition */
	/*** bounty_free_games */

	/**************
	 * Games
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function games()
	{
		return $this->belongsToMany('App\Models\Game')->withPivot('id')->orderBy('pivot_id');
	}

	/**
	 * @param $list
	 * @return bool|void
	 */
	public function attachGames($list)
	{
		if (!$this->games()->get()->contains($list)) {
			return $this->games()->attach($list);
		}

		return true;
	}

	/**
	 * @param $list
	 * @return int
	 */
	public function detachGames($list)
	{
		return $this->games()->detach($list);
	}

	/**
	 * @return int
	 */
	public function detachAllGames()
	{
		return $this->games()->detach();
	}

	/**
	 * @param $query
	 * @param $list
	 * @return mixed
	 */
	public function scopeGames($query, $list)
	{
		return $query->whereHas('game', function ($query) use ($list) {
			$query->where('slug', $list);
		});
	}

	/**
	 * @param $lists
	 */
	public function setGamesAttribute($lists)
	{
		if ( ! $this->exists) $this->save();
		( ! $lists) ? $this->detachAllGames() : $this->games()->sync($lists);
	}

	/**************
	 * Condition
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function conditions()
	{
		return $this->belongsToMany('App\Models\Game', 'bounty_free_condition')->withPivot('id')->orderBy('pivot_id');
	}

	/**
	 * @param $list
	 * @return bool|void
	 */
	public function attachConditions($list)
	{
		if (!$this->conditions()->get()->contains($list)) {
			return $this->conditions()->attach($list);
		}

		return true;
	}

	/**
	 * @param $list
	 * @return int
	 */
	public function detachConditions($list)
	{
		return $this->conditions()->detach($list);
	}

	/**
	 * @return int
	 */
	public function detachAllConditions()
	{
		return $this->conditions()->detach();
	}

	/**
	 * @param $query
	 * @param $list
	 * @return mixed
	 */
	public function scopeConditions($query, $list)
	{
		return $query->whereHas('condition', function ($query) use ($list) {
			$query->where('slug', $list);
		});
	}

	/**
	 * @param $lists
	 */
	public function setConditionsAttribute($lists)
	{
		if ( ! $this->exists) $this->save();
		( ! $lists) ? $this->detachAllConditions() : $this->conditions()->sync($lists);
	}
}
