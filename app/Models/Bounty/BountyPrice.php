<?php namespace App\Models\Bounty;

use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

class BountyPrice extends Model{
	use LocalizedDateTrait;

	protected $table = 'bounty_price';

	protected $fillable = array(
		'active_to',
		'active_from',

		'price',

		'created_at',
		'updated_at'
	);
	//** bounty_price_games */

	/**************
	 * Condition
	 *************/
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function conditions()
	{
		return $this->belongsToMany('App\Models\Game', 'bounty_price_condition')->withPivot('id')->orderBy('pivot_id');
	}

	/**
	 * @param $list
	 * @return bool|void
	 */
	public function attachConditions($list)
	{
		if (!$this->conditions()->get()->contains($list)) {
			return $this->conditions()->attach($list);
		}

		return true;
	}

	/**
	 * @param $list
	 * @return int
	 */
	public function detachConditions($list)
	{
		return $this->conditions()->detach($list);
	}

	/**
	 * @return int
	 */
	public function detachAllConditions()
	{
		return $this->conditions()->detach();
	}

	/**
	 * @param $query
	 * @param $list
	 * @return mixed
	 */
	public function scopeConditions($query, $list)
	{
		return $query->whereHas('condition', function ($query) use ($list) {
			$query->where('slug', $list);
		});
	}

	/**
	 * @param $lists
	 */
	public function setConditionsAttribute($lists)
	{
		if ( ! $this->exists) $this->save();
		( ! $lists) ? $this->detachAllConditions() : $this->conditions()->sync($lists);
	}
}
