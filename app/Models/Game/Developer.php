<?php namespace App\Models\Game;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Traits\SEOTrait;
use AgelxNash\SEOTools\Interfaces\SeoInterface;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;
use App\Models as Models;

/**
 * Class Developer
 *
 * @package App\Models\Game
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Developer extends Model implements SluggableInterface, SeoInterface{
	use SluggableTrait, SEOTrait, ReplaceBuilder, LocalizedDateTrait;

	protected $table = 'developers';

	protected $fillable = array(
		'name',
		'content',
		'slug',
		'created_at',
		'updated_at'
	);

	protected $sluggable = [
		'build_from' => 'name',
		'save_to'    => 'slug',
	];

	protected $defaultSeoFields = [
		'priority' => 0.7,
		'robots' => 'index,follow',
		'state' => 'dynamic',
		'frequency' => 'weekly'
	];

	public function url($method = null){
		switch($method){
			case 'lists':{
				$out = action('\App\Http\Controllers\GameController@getIndex');
				break;
			}
			default:{
				if($this->exists){
					$out = action('\App\Http\Controllers\GameController@getDevelopers', ['type' => $this->slug]);
				}else{
					$out = '#';
				}
			}
		}
		return $out;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphOne
	 */
	public function seo()
	{
		return $this->morphOne(Models\Seo::class, 'seoble', 'document_type', 'document_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function games()
	{
		return $this->belongsToMany('\App\Models\Game', 'game_developers');
	}

	public function getFullTitleAttribute(){
		return 'Игры разработчика "'.$this->name.'"';
	}
	public function getTitleAttribute(){
		return $this->name;
	}
	public function setTitleAttribute($val){
		$this->name = $val;
	}

	/**
	 * @return array
	 */
	public static function getList(){
		return self::all()->lists('name', 'id')->all();
	}
}