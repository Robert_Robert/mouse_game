<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

class Order extends Model{
	use LocalizedDateTrait;

	protected $table = 'orders';

	/**
	 * @var array
	 */
	protected $dates = array(
		'payed_at'
	);

	protected $fillable = array(
		'user_id',
		'price',
		'pay',
		'payed_method',
		'payed_at',
		'created_at',
		'updated_at'
	);

	public function user(){
		return $this->belongsTo(User::class, 'user_id');
	}
	public function items(){
		return $this->hasMany(OrderItem::class, 'order_id');
	}

	public function getTextDatePayedAtAttribute()
	{
		return $this->takeCarbonAttributes("payed_at", '%d %f %Y');
	}

	public function getHumanPayedAtAttribute()
	{
		return $this->getHumanTimestampAttribute("payed_at");
	}
}
