<?php namespace App\Models;

use AgelxNash\SEOTools\Models\Keyword as Model;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

/**
 * Class Keyword
 * @package App\Models
 */
class Keyword extends Model {
	use LocalizedDateTrait;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function seo()
	{
		return $this->belongsToMany(Seo::class)->withTimestamps();
	}
}