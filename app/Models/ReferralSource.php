<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOPagination\Eloquent\ReplaceBuilder;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

/**
 * Class ReferralSource
 * @package App\Models
 * @property int $id
 * @property string $source
 * @property string $ip
 * @property int $user_id ID пользователя зарегистрированного
 * @property int $referrer_id ID пользователя который дал ссылку
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Seo $seo
 * @property string $title
 */
class ReferralSource extends Model{
	use ReplaceBuilder, LocalizedDateTrait;

	protected $table = 'referral_sources';

	protected $fillable = array(
		'source',
		'landing',
		'ip',
		'user_id',
		'referrer_id',
		'created_at',
		'updated_at'
	);

	/**
	 * Пользователь который меня пригласил
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function referrer()
	{
		return $this->belongsTo(User::class, 'referrer_id');
	}

	/**
	 * Пользователь который зарегистрировался
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
