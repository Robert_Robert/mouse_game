<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use AgelxNash\SEOTools\Traits\LocalizedDateTrait;

/**
 * Class Status
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property mixed $slug
 * @property bool $buyer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Seo $seo
 * @property string $title
 */
class Status extends Model implements SluggableInterface{
	use SluggableTrait, LocalizedDateTrait;

	protected $table = 'statuses';

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);

	protected $fillable = array(
		'name',
		'slug',
		'buyer',
		'created_at',
		'updated_at'
	);

	public function games(){
		return $this->hasMany(Game::class, 'status_id');
	}

	/**
	 * @return array
	 */
	public static function getList(){
		return self::all()->lists('name', 'id')->all();
	}

	public function getSchemaNameAttribute(){
		switch($this->slug){
			case 'instok':{
				$out = 'https://schema.org/InStock';
				break;
			}
			case 'outstok':{
				$out = 'http://schema.org/OutOfStock';
				break;
			}
			case 'prestok':{
				$out = 'http://schema.org/PreOrder';
				break;
			}
			default:{
				$out = 'http://schema.org/LimitedAvailability';
			}
		}
		return $out;
	}
}
