<?php

Admin::menu()->url('/')->label('Главная')->icon('fa-dashboard');


Admin::menu(\App\Models\News::class)->label('Новости')->icon('fa-newspaper-o');

if(AdminAuth::user() && AdminAuth::user()->isSudoWithoutPermission()){
	Admin::menu(\App\Models\User::class)->label('Пользователи')->icon('fa-users');
}

Admin::menu()->label('Классификатор')->icon('fa-book')->items(function () {
	Admin::menu(\App\Models\Status::class)->label('Статусы')->icon('fa-cart-arrow-down');
	Admin::menu(\App\Models\Game\Lang\Text::class)->label('Языки')->icon('fa-language');
	Admin::menu(\App\Models\Game\Mode::class)->label('Режимы игры')->icon('fa-tags');
	Admin::menu(\App\Models\Game\Region::class)->label('Регионы')->icon('fa-flag');
	Admin::menu(\App\Models\Game\Janr::class)->label('Жанры')->icon('fa-tag');
	Admin::menu(\App\Models\Game\Platform::class)->label('Платформы')->icon('fa-desktop');
	Admin::menu(\App\Models\Game\Developer::class)->label('Разработчики')->icon('fa-gavel');
	Admin::menu(\App\Models\Game\Vendor::class)->label('Издатели')->icon('fa-truck');
	Admin::menu(\App\Models\Game\Year::class)->label('Возрастные ограничения')->icon('fa-heartbeat');
});
Admin::menu(\App\Models\Game::class)->label('Товары')->icon('fa-gamepad');

Admin::menu()->label('Акции')->icon('fa-exclamation')->items(function () {
	Admin::menu(\App\Models\Bounty\BountyPrice::class)->label('Общая стоимость')->icon('fa-object-group');
	Admin::menu(\App\Models\Bounty\BountyDiscount::class)->label('Скидки')->icon('fa-percent');
	Admin::menu(\App\Models\Bounty\BountyFree::class)->label('Бонусы')->icon('fa-gift');
});
Admin::menu(\App\Models\Lister::class)->label('Списки')->icon('fa-list-ol');

Admin::menu()->label('Работа с заказами')->icon('fa-shopping-cart')->items(function () {
	Admin::menu()->label('История заказов')->icon('fa-credit-card'); /** @TODO-agelnash */
	Admin::menu()->url('resend-key')->label('Отправка ключей')->icon('fa-envelope-square');
});

Admin::menu()->label('Администрирование')->icon('fa-cubes')->items(function () {
	Admin::menu(\App\Models\Text::class)->label('Служебные страницы')->icon('fa-file-text-o');
	Admin::menu(\App\Models\Slider::class)->label('Слайдер на главной')->icon('fa-slideshare');
	Admin::menu()->url('log-viewer')->label('Ошибки на сайте')->icon('fa-bug');
	Admin::menu()->url('server')->label('Информация о сервере')->icon('fa-cloud');
});




