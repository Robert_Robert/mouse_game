<?php

Route::get('', ['as' => 'admin.home',function (){
	$content = 'Define your dashboard here.';
	return Admin::view($content, 'Dashboard');
}]);
Route::get('resend-key', '\App\Http\Controllers\Admin\ResendKeyController@getIndex');