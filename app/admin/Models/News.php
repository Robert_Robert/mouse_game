<?php
Admin::model(\App\Models\News::class)->title('Новости')->alias('news')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\News::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::custom()->label('Название')->callback(function ($instance){
			$out = $instance->title;
			if($instance->image) {
				$out .= empty_tag('br');
				$out .= img_tag($instance->thumb('admin'));
			}
			return a_tag($instance->url(), $out, ['target' => '_blank']);
		})->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
	])->apply(function ($query){
		$query->orderBy('created_at', 'DESC');
	})->attributes([
		'ordering' => false,
		'stateSave' => true, /** UPDATE */
	])->setSearchColumns(['title', 'content', 'description', 'slug']);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\News::findOrNew($modelId);
	Admin::model(\App\Models\News::class)->title( $modelObj->getKey() ? $modelObj->title : 'Добавление');
	\Session::set('_redirectBack', Admin::model(\App\Models\News::class)->displayUrl());
	$form = AdminForm::tabbed();
	$form->items([
		'Описание' => [
			FormItem::text('title', 'Название*')->required(),
			FormItem::anMarkitUp('content', 'Текст новости*')->required(),
			FormItem::textarea('description', 'Краткое описание'),
		],
		'SEO' => [
			FormItem::text('slug', 'Псевдоним'),
			FormItem::anSeo()
		],
		'Параметры' => [
			FormItem::date('published_at', 'Дата публикации*')->required(),
			FormItem::image('image', 'Картинка'),
		]
	]);
	return $form;
});