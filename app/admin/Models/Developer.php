<?php
Admin::model(\App\Models\Game\Developer::class)->title('Разработчики')->alias('developer')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Game\Developer::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::string('name')->label('Название')->orderable(false),
		Column::count('games')->label('Товаров')->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
	])->apply(function ($query){
		$query->orderBy('name', 'ASC');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->with(['games'])
		->setSearchColumns(['name', 'content', 'slug']);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\Game\Developer::findOrNew($modelId);
	Admin::model(\App\Models\Game\Developer::class)->title( $modelObj->getKey() ? $modelObj->name : 'Добавление');
	\Session::set('_redirectBack', Admin::model(\App\Models\Game\Developer::class)->displayUrl());

	$form = AdminForm::tabbed();
	$form->items([
		'Описание' => [
			FormItem::text('name', 'Название')->required(),
			FormItem::anMarkitUp('content', 'Описание'),
		],
		'SEO' => [
			FormItem::text('slug', 'Псевдоним'),
			FormItem::anSeo()
		]
	]);
	return $form;
});