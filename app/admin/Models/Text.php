<?php
Admin::model(\App\Models\Text::class)->title('Служебные страницы')->alias('text')->delete(function ($id) {
	$modelObj = \App\Models\Text::findOrNew($id);
	return ($modelObj->system || $modelObj->hide) ? null : true;
})->display(function ()
{
	\Session::set('_redirectBack', Admin::model(\App\Models\Text::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::custom()->label('Название')->callback(function ($instance){
			return a_tag($instance->url(), $instance->name, ['target' => '_blank']);
		})->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
	])->apply(function ($query){
		$query->orderBy('name', 'ASC');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->setSearchColumns(['name', 'content', 'slug']);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\Text::findOrNew($modelId);
	Admin::model(\App\Models\Text::class)->title( $modelObj->getKey() ? $modelObj->title : 'Добавление');
	\Session::set('_redirectBack', Admin::model(\App\Models\Text::class)->displayUrl());

	$form = AdminForm::tabbed();
	$form->items([
		'Описание' => [
			(($modelObj->system || $modelObj->hide) ? '' : FormItem::text('title', 'Имя')->required()),
			FormItem::anMarkitUp('content', 'Описание')->required(),
		],
		'SEO' => [
			(($modelObj->system || $modelObj->hide) ? '' : FormItem::text('slug', 'Псевдоним')),
			FormItem::anSeo()
		]
	]);
	return $form;
});