<?php
Admin::model(\App\Models\Game\Platform::class)->title('Платформы')->alias('platform')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Game\Platform::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::custom()->label('Название')->callback(function ($instance){
			return a_tag($instance->url(), $instance->name, ['target' => '_blank']);
		}),
		Column::string('icon')->label('Класс меню'),
		Column::count('games')->label('Товаров'),
		Column::string('text_date_time_updated_at')->label('Дата обновления'),
		Column::string('text_date_time_created_at')->label('Дата создания'),
		Column::order()
	])->apply(function ($query){
		$query->orderBy('sort', 'asc');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->with(['games'])
		->setSearchColumns(['name', 'content', 'slug', 'icon']);
	return $display;
})->createAndEdit(function ($modelId){
	$form = false;
	switch(Route::getCurrentRoute()->getName()){
		case 'admin.model':{
			$form = true;
			break;
		}
		case 'admin.model.create':{
			Admin::model(\App\Models\Game\Platform::class)->title('Добавление');
			break;
		}
		case 'admin.model.edit':{
			$modelObj = \App\Models\Game\Platform::findOrNew($modelId);
			Admin::model(\App\Models\Game\Platform::class)->title($modelObj->name);
			break;
		}
	}
	if($form !== true){
		\Session::set('_redirectBack', Admin::model(\App\Models\Game\Platform::class)->displayUrl());

		$form = AdminForm::tabbed();
		$form->items([
			'Описание' => [
				FormItem::text('name', 'Название')->required(),
				FormItem::anMarkitUp('content', 'Описание'),
			],
			'SEO' => [
				FormItem::text('slug', 'Псевдоним'),
				FormItem::anSeo()
			],
			'Параметры' => [
				FormItem::text('icon', 'Класс для ссылки'),
			]
		]);
	}
	return $form;
});