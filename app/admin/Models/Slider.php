<?php

Admin::model(\App\Models\Slider::class)->title('Слайдер')->alias('slider')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Slider::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::custom()->label('Картинка')->callback(function ($instance){
			return img_tag($instance->thumb('admin-slide'));
		})->orderable(false),
		Column::custom()->label('Ссылка')->callback(function($instance) {
			return empty($instance->link) ? '-' : a_tag($instance->link, $instance->link, ['target' => '_blank']);
		}),
		Column::string('left')->label('Текст слева'),
		Column::string('right')->label('Текст справа'),
		Column::order()
	])->apply(function ($query){
		$query->orderBy('sort', 'asc');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->setSearchColumns(['left', 'right', 'image', 'link']);
	return $display;
})->createAndEdit(function ($modelId){
	$form = false;
	switch(Route::getCurrentRoute()->getName()){
		case 'admin.model':{
			$form = true;
			break;
		}
		case 'admin.model.create':{
			Admin::model(\App\Models\Slider::class)->title('Добавление');
			break;
		}
		case 'admin.model.edit':{
			$modelObj = \App\Models\Slider::findOrNew($modelId);
			Admin::model(\App\Models\Slider::class)->title($modelObj->name);
			break;
		}
	}
	if($form !== true){
		\Session::set('_redirectBack', Admin::model(\App\Models\Slider::class)->displayUrl());

		$form = AdminForm::tabbed();
		$form->items([
			'Описание' => [
				FormItem::text('link', 'Ссылка'),
				FormItem::text('left', 'Текст слева'),
				FormItem::text('right', 'Текст справа'),
				FormItem::anImage('image', 'Картинка')
			]
		]);
	}
	return $form;
});