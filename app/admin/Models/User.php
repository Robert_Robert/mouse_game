<?php
\Admin::model(\App\Models\User::class)->title('Пользователи')->alias('users')->delete(function ($id) {
	return ( \AdminAuth::user()->isSudoWithoutPermission() &&  $id > 1) ? true : null;
})->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\User::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$columns = new \Illuminate\Support\Collection([
		Column::string('name')->label('Имя')->orderable(false),
		Column::string('email')->label('E-mail')->orderable(false),
		Column::custom()->label('Информаци о регистрации')->callback(function($instance){
			$out = 'Дата: ' .html_wrap('em', $instance->text_date_time_created_at);
			$out .= empty_tag('br');
			$out .= 'IP: ' .html_wrap('em', $instance->reg_ip);
			/*if(!empty($instance->reg_referer_page)) {
				$out .= empty_tag('br');
				$out .= 'URL: ' . html_wrap('em', e($instance->reg_referer_page));
			}*/
			return $out;
		}),
		Column::lists('roles.name')->label('Роли пользователя')->orderable(false),
	]);
	if (\AdminAuth::user()->isSudoWithoutPermission()) {
		$columns->push(Column::lists('userpermissions.name')->label('Права администратора')->orderable(false));
	}
	$display->columns($columns->all())->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->apply(function ($query){
		$query->orderBy('created_at', 'DESC');
	})->setSearchColumns(['name', 'email']);//->with(['roles', 'userpermissions']);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\User::findOrNew($modelId);
	Admin::model(\App\Models\User::class)->title( $modelObj->getKey() ? $modelObj->name : 'Добавление');

	\Session::set('_redirectBack', Admin::model(\App\Models\User::class)->displayUrl());
	$form = null;
	$flag = (\AdminAuth::user()->isSudoWithoutPermission() && (is_null($modelId) || $modelId > 1));
	if ($flag || \AdminAuth::user()->id == $modelId) {
		$form = AdminForm::form();
		$items = new \Illuminate\Support\Collection([
			FormItem::text('name', 'Имя')->required(),
			FormItem::text('email', 'E-mail')->required()->unique(),
			FormItem::password('password', 'Пароль')->required()
		]);
		if($flag) {
			$items->push(FormItem::multiselect('roles', 'Роль пользователя')
				->model(\App\Models\Role::class)
				->display('name')
			);

			$items->push(FormItem::multiselect('userpermissions', 'Права администратора')
				->model(\App\Models\Permission::class)
				->display('name')
				//->validationRule('isSudoWithoutPermission') //@TODO-agelnash: кастомная валидация
			);
		}

		$form->items($items->all());
	}
	return $form;
});