<?php
Admin::model(\App\Models\Game\Region::class)->title('Регионы')->alias('region')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Game\Region::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::string('name')->label('Название')->orderable(false),
		Column::count('games')->label('Товаров')->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
		Column::order()
	])->apply(function ($query){
		$query->orderBy('sort', 'asc');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->with(['games'])
		->setSearchColumns(['name', 'slug']);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\Game\Region::findOrNew($modelId);
	Admin::model(\App\Models\Game\Region::class)->title( $modelObj->getKey() ? $modelObj->name : 'Добавление');
	\Session::set('_redirectBack', Admin::model(\App\Models\Game\Region::class)->displayUrl());
	$form = AdminForm::tabbed();
	$form->items([
		'Описание' => [
			FormItem::text('name', 'Название')->required(),
		],
		/*'SEO' => [
			FormItem::text('slug', 'Псевдоним'),
			//FormItem::anSeo()
		]*/
	]);
	return $form;
});