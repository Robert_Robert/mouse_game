<?php
Admin::model(\App\Models\Bounty\BountyPrice::class)->title('Общая стоимость')->alias('bounty_price')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Bounty\BountyPrice::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::custom()->label('Товары в корзине')->callback(function ($instance){
			return $instance->conditions->lists('title')->implode('<br />');
		})->orderable(false),
		Column::string('price')->label('Общая стоимость')->orderable(false),
		Column::string('active_from')->label('Дата начала')->orderable(false),
		Column::string('active_to')->label('Дата окончания')->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
	])->apply(function ($query){
		$query->orderBy('active_to', 'DESC')
			->orderBy('active_from', 'ASC');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	]);
	return $display;
})->createAndEdit(function (){
	\Session::set('_redirectBack', Admin::model(\App\Models\Bounty\BountyPrice::class)->displayUrl());
	$form = AdminForm::form();
	$form->items([
		FormItem::multiselect('conditions', 'Товары в корзине')->options(\App\Models\Game::getList()),
		FormItem::text('price', 'Общая стоимость')->defaultValue(0),
		FormItem::date('active_from', 'Дата начала')->required(),
		FormItem::date('active_to', 'Дата окончания')->required()
	]);
	return $form;
});