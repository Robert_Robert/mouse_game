<?php
Admin::model(\App\Models\Status::class)->title('Статусы')->alias('status')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Status::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::string('name')->label('Название')->orderable(false),
		Column::custom()->label('Возможность заказа')->callback(function ($instance){
			return $instance->buyer ? 'Да' : 'Нет';
		})->orderable(false),
		Column::count('games')->label('Товаров')->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
	])->apply(function ($query){
		$query->orderBy('name', 'ASC');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->with(['games'])
		->setSearchColumns(['name', 'slug']);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\Status::findOrNew($modelId);
	Admin::model(\App\Models\Status::class)->title( $modelObj->getKey() ? $modelObj->name : 'Добавление');
	\Session::set('_redirectBack', Admin::model(\App\Models\Status::class)->displayUrl());
	$form = AdminForm::tabbed();
	$form->items([
		'Описание' => [
			FormItem::text('name', 'Название')->required(),
			FormItem::checkbox('buyer', 'Доступность для заказа')
		],
		/*'SEO' => [
			FormItem::text('slug', 'Псевдоним'),
			//FormItem::anSeo()
		]*/
	]);
	return $form;
});