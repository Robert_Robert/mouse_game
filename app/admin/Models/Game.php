<?php
Admin::model(\App\Models\Game::class)->title('Товары')->alias('game')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Game::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::custom()->label('Название')->callback(function ($instance){
			$out = $instance->title;
			$out .= empty_tag('br');
			$out .= img_tag($instance->thumb('admin'));
			return a_tag($instance->url(), $out, ['target' => '_blank']);
		})->orderable(false),
		Column::custom()->label('Параметры')->callback(function ($instance){
			$out = 'Платформы: ' .html_wrap('em', $instance->platforms->lists('name')->implode(', '));
			$out .= empty_tag('br');
			$out .= 'Жанры: ' . html_wrap('em', $instance->janrs->lists('name')->implode(', '));
			$out .= empty_tag('br');
			$out .= 'Режимы игры: ' .html_wrap('em', $instance->mode->lists('name')->implode(', '));
			$out .= empty_tag('br');
			$out .= 'Перевод текста: ' . html_wrap('em', $instance->langtext->lists('name')->implode(', '));
			$out .= empty_tag('br');
			$out .= 'Регионы: ' . html_wrap('em', $instance->regions->lists('name')->implode(', '));
			$addons = $instance->addons->lists('title')->toArray();
			if(!empty($addons)) {
				$out .= empty_tag('br');
				$out .= 'Дополнения:';
				$out .= empty_tag('br');
				$out .= html_wrap('ul', html_implode($addons, 'li'));
			}
			return $out;
		})->orderable(false),
		Column::custom()->label('Стоимость')->callback(function ($instance){
			if(!empty($instance->fullprice)){
				$out = html_wrap('s', number_format($instance->price, 0, ',', ' '));
				$out .= empty_tag('br');
				$out .= number_format($instance->fullprice, 0, ',', ' ').' руб.';
			}else{
				$out = number_format($instance->price, 0, ',', ' ').' руб.';
			}
			return $out;
		})->orderable(false),
		Column::custom()->label('Рейтинг')->callback(function($instance){
			$out = 'Локальный: ' .html_wrap('em', $instance->rate);
			$out .= empty_tag('br');
			$out .= 'Metacritic: ' .html_wrap('em', $instance->metacritic);
			return $out;
		}),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false)
	])->apply(function ($query){
		$query->orderBy('rate', 'DESC');
	})->with(['platforms', 'janrs', 'mode', 'langtext', 'regions', 'addons'])->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->setSearchColumns([
		'title', 'content', 'description', 'install', 'list_title', 'slug', 'requirement'
	])->actions([
		Column::action('flushrate')->value('Обнуление рейтинга')->icon('fa-eraser')->callback(function ($collection)
		{
			DB::table(with(new App\Models\Game)->getTable())
				->update(['rate' => 0]);
		})
	]);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\Game::findOrNew($modelId);
	Admin::model(\App\Models\Game::class)->title( $modelObj->getKey() ? $modelObj->title : 'Добавление');
	\Session::set('_redirectBack', Admin::model(\App\Models\Game::class)->displayUrl());
	$form = AdminForm::tabbed();
	$items = [
		'Описание' => [
			FormItem::text('title', 'Имя')->required(),
			FormItem::text('list_title', 'Заголовок для списков в 32 символа'),
			FormItem::anMarkitUp('description', 'Краткое описание'),
			FormItem::anMarkitUp('content', 'Описание')->required(),
			FormItem::anMarkitUp('requirement', 'Системные требования'),
			FormItem::anMarkitUp('install', 'Установка'),
			FormItem::text('video', 'Видео-обзор'),
		],
		'Параметры' => [
			FormItem::checkbox('published', 'Опубликовать')->defaultValue(1),
			FormItem::checkbox('hiddened', 'Не отображать во всех играх')->defaultValue(0),
			FormItem::date('release', 'Дата выхода')->format('Y-m-d'),
			FormItem::anOneSelect('vendors', 'Издатель')->options(\App\Models\Game\Vendor::getList())->required(),
			FormItem::anOneSelect('developers', 'Разработчик')->options(\App\Models\Game\Developer::getList())->required(),

			FormItem::multiselect('platforms', 'Платформа')->options(\App\Models\Game\Platform::getList())->required(),
			FormItem::multiselect('janrs', 'Жанр')->options(\App\Models\Game\Janr::getList())->required(),
			FormItem::multiselect('langtext', 'Язык')->options(\App\Models\Game\Lang\Text::getList())->required(),
			FormItem::multiselect('mode', 'Режимы игры')->options(\App\Models\Game\Mode::getList())->required(),
			FormItem::multiselect('regions', 'Регион')->options(\App\Models\Game\Region::getList()),
			FormItem::multiselect('addons', 'Дополнения')->options(\App\Models\Game::getList()),
			FormItem::multiselect('relevants', 'Похожие игры')->options(\App\Models\Game::getList()),
			FormItem::anOneSelect('years', 'Возрастные ограничения')->options(\App\Models\Game\Year::getList())->required(),

			FormItem::select('metacritic', 'Рейтинг Metacritic (0-100)')->options(range(0, 100, 1)),
			FormItem::text('rate', 'Внутренний рейтинг')
		],
		'Магазин' => [
			FormItem::select('status_id', 'Наличие')->model(App\Models\Status::class)->display('name')->defaultValue(3)->required()
		],
		'Картинки' => [
			FormItem::image('image', 'Картинка')->required(),
			FormItem::anGalleryList('photos', 'Слайдер')->addField(['type' => 'input', 'name' => 'title', 'label' => 'Описание'])
		],
		'SEO' => [
			FormItem::text('slug', 'Псевдоним'),
			FormItem::anSeo(),
		]
	];

	if ( ! \AdminAuth::user()->can('game.price')) {
		$items['Магазин'][] = FormItem::text('price', 'Цена')->defaultValue(0);
		if ( ! \AdminAuth::user()->can('sale')) {
			$items['Магазин'][] = FormItem::text('fullprice', 'Цена со скидкой');
		}
	}
	$form->items($items);
	return $form;
});
