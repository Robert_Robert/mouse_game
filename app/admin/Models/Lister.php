<?php
Admin::model(\App\Models\Lister::class)->title('Списки')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Lister::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::string('name')->label('Название')->orderable(false),
		Column::count('games')->label('Товаров')->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
	])->apply(function ($query){
		$query->orderBy('name', 'ASC');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	])->with(['games'])
		->setSearchColumns(['name', 'slug']);
	return $display;
})->createAndEdit(function ($modelId) {
	$modelObj = \App\Models\Lister::findOrNew($modelId);
	Admin::model(\App\Models\Lister::class)->title( $modelObj->getKey() ? $modelObj->name: 'Добавление');
	\Session::set('_redirectBack', Admin::model(\App\Models\Lister::class)->displayUrl());
	$form = AdminForm::tabbed();
	switch($modelObj->slug){
		case 'preorder': {
			$status = App\Models\Status::with('games')->where('slug', 'prestok')->first();
			if($status){
				$games = $status->games->lists('title', 'id')->all();
			}else{
				$games = [];
			}
			break;
		}
		case 'sale':{
			$games = App\Models\Game::where('fullprice', '>', 0)->lists('title', 'id')->all();
			break;
		}
		default:{
			$games = \App\Models\Game::getList();
			break;
		}
	}
	$form->items([
		'Описание' => [
			FormItem::text('name', 'Название')->required(),
			FormItem::multiselect('games', 'Игры')->options($games),
		],
		/*'SEO' => [
			FormItem::text('slug', 'Псевдоним'),
			//FormItem::anSeo()
		]*/
	]);
	return $form;
});