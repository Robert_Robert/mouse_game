<?php
Admin::model(\App\Models\Bounty\BountyDiscount::class)->title('Скидка на товары')->alias('bounty_discount')->display(function () {
	\Session::set('_redirectBack', Admin::model(\App\Models\Bounty\BountyDiscount::class)->displayUrl());
	$display = AdminDisplay::datatablesSearchAsync();
	$display->columns([
		Column::custom()->label('Товары в корзине')->callback(function ($instance){
			return $instance->conditions->lists('title')->implode('<br />');
		})->orderable(false),
		Column::custom()->label('Скидка на товары')->callback(function ($instance){
			return $instance->games->lists('title')->implode('<br />');
		})->orderable(false),
		Column::string('discount')->label('Размер скидки')->orderable(false),
		Column::string('active_from')->label('Дата начала')->orderable(false),
		Column::string('active_to')->label('Дата окончания')->orderable(false),
		Column::string('text_date_time_updated_at')->label('Дата обновления')->orderable(false),
		Column::string('text_date_time_created_at')->label('Дата создания')->orderable(false),
	])->apply(function ($query){
		$query->orderBy('active_to', 'DESC')
			->orderBy('active_from', 'ASC');
	})->attributes([
		'ordering' => false,
		'stateSave' => true,
	]);
	return $display;
})->createAndEdit(function (){
	\Session::set('_redirectBack', Admin::model(\App\Models\Bounty\BountyDiscount::class)->displayUrl());
	$form = AdminForm::form();
	$form->items([
		FormItem::multiselect('conditions', 'Товары в корзине')->options(\App\Models\Game::getList()),
		FormItem::multiselect('games', 'Скидка на товары')->options(\App\Models\Game::getList()),
		FormItem::select('discount', 'Размер скидки')->options(range(0, 99, 1)),
		FormItem::date('active_from', 'Дата начала')->required(),
		FormItem::date('active_to', 'Дата окончания')->required(),
	]);
	return $form;
});