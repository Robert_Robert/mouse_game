<?php namespace App\Handlers\Events;

use AdminAuth, Auth;

class AuthLoginEventHandler
{

	/**
	 * Handle the event.
	 *
	 * @param  $event
	 * @param bool $remember
	 * @return void
	 */
	public function handle($event, $remember)
	{
		switch(true){
			case (AdminAuth::user() && ! Auth::user()):{
				Auth::loginUsingId(AdminAuth::user()->getKey(), $remember);
				break;
			}
			case (Auth::user() && ! AdminAuth::user()):{
				AdminAuth::loginUsingId( Auth::user()->getKey(), $remember);
				break;
			}
		}
	}
}
