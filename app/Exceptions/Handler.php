<?php namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Redirect, Request;
use Bican\Roles\Exceptions\PermissionDeniedException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyDisplayer;
use App\Models as Models;
use Illuminate\Database\Eloquent\Model;
use AgelxNash\SEOTools\Interfaces\SeoInterface;
use App;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		HttpException::class,
		ModelNotFoundException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Throw a 404 is a model record isn't found
	 * @param \Illuminate\Database\Eloquent\ModelNotFoundException
	 *
	 * @return mixed
	 */
	protected function renderNotFoundException($e)
	{
		if (view()->exists('errors.404'))
		{
			return response()->view('errors.404', [], 404);
		}
		else
		{
			return (new SymfonyDisplayer(config('app.debug')))
				->createResponse($e);
		}
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		$out = null;
		switch(true) {
			case ($e instanceof PermissionDeniedException):
				if(strpos($e->getMessage(), "You don't have a required ['admin.") !== false) {
					$out = response()->view(\AdminTemplate::view('pages.no-permission'), [
						'title' => config('admin.title')
					])->setStatusCode('403');
				}else{
					$out = response()->view('errors.403')->setStatusCode('403');
				}
				break;
			case $this->isHttpException($e):
			case ($e instanceof ModelNotFoundException):
				$out = $this->renderNotFoundException($e);
				break;
			case (!config('app.debug') || App::environment('production')):
				$out = response()->view('errors.500', [], 500);
				break;
			default:
				$out = parent::render($request, $e);
		}
		return $out;
	}

}
