<?php namespace App\Lib;

use Illuminate\Support\Str;
class Helper{
	public static function payment($name){
		$paymens = [
			'visa' => 0,
			'mastercard' => 0,
			'webmoney' => 0.8,
			'kiwi' => 0,
			'mts' => 7.5,
			'megafon' => 11,
			'yandex' => 0,
		];
		return get_key($paymens, $name, 0, 'is_scalar');
	}
	public static function image($tpl, $file){
		$out = $file;
		foreach(config('imagecache.paths') as $path){
			$tmp = public_path(ltrim($file, '/'));

			if(Str::substr($tmp, 0, Str::length($path) + 1) == $path .'/'){
				$file = Str::substr($tmp, Str::length($path) + 1);
				$out = '/'.implode('/', [
						config('imagecache.route'),
						$tpl,
						$file
					]);
				break;
			}
		}
		return $out;
	}
	public static function maxLen($text, $len = 25){
		if(Str::length($text) > $len){
			$out = trim(Str::substr($text, 0, $len - 3));
			$out .= '...';
		}else{
			$out = $text;
		}
		return $out;
	}
	public static function splitTitle($title, $len = 18, $lines = null){
		$out = array();
		$title = explode(" ", $title);

		$allWordsLen = 0;
		$str = 0;
		foreach($title as $num => $word){
			$wordLen = Str::length($word);
			$allWordsLen += $wordLen;
			if($allWordsLen >= $len){
				$allWordsLen = $wordLen;
				$str++;
			}

			$out[$str][] = $word;

			if($wordLen > $len){
				$str++;
			}
		}
		foreach($out as &$line){
			$line = implode(" ", $line);
		}
		$totalLines = count($out);
		$out = array_slice($out, 0, $lines);
		if(!empty($lines) && $lines <= $totalLines){
			$dotted = $totalLines != count($out);
			$end = array_pop($out);
			if((Str::length($end) + 3) > $len){
				$end = Str::substr($end, 0, -3);
				$dotted = true;
			}

			$end = trim($end, '.');
			if($dotted){
				$end .= '...';
			}
			$out[] = $end;
		}

		return implode("<br />", $out);
	}
}