<?php namespace App\Lib\Pagination\Presenters;

use Illuminate\Pagination\BootstrapThreePresenter;

/*<div class="pager">
	<ul>
		<li class="first"><a href="#">‹‹</a></li>
		<li class="prev"><a href="#"></a></li>
		<li class="current page"><a href="#">1</a></li>
		<li class="page"><a href="#">2</a></li>
		<li class="page"><a href="#">3</a></li>
		<li class="next"><a href="#"></a></li>
		<li class="last"><a href="#">››</a></li>
	</ul>
</div>*/
class FrontV1Presenter extends BootstrapThreePresenter {

	/**
	 * Convert the URL window into Bootstrap HTML.
	 *
	 * @return string
	 */
	public function render($class = '')
	{
		if ($this->hasPages())
		{
			return sprintf(
				'<div class="pager  %s"><ul>
					%s
					%s %s %s
					%s
					</ul></div>',
				$class,
				$this->getFirstButton(),
				$this->getPreviousButton(),
				$this->getLinks(),
				$this->getNextButton(),
				$this->getLastButton()
			);
		}

		return '<div class="pager"></div>';
	}

	/**
	 * Get HTML wrapper for active text.
	 *
	 * @param  string  $text
	 * @return string
	 */
	public function getActivePageWrapper($text)
	{
		$url = $this->paginator->url($this->paginator->currentPage());
		return '<li class="page current"><a href="'.htmlentities($url).'">'.$text.'</a></li>';
	}

	/**
	 * Get HTML wrapper for disabled text.
	 *
	 * @param  string  $text
	 * @return string
	 */
	public function getDisabledTextWrapper($text)
	{
		return '';
	}

	/**
	 * Get HTML wrapper for a page link.
	 *
	 * @param  string  $url
	 * @param  int  $page
	 * @param  string  $rel
	 * @return string
	 */
	public function getPageLinkWrapper($url, $page, $rel = null)
	{
		if ($page == $this->paginator->currentPage())
		{
			return $this->getActivePageWrapper($page);
		}

		return $this->getAvailablePageWrapper($url, $page, $rel);
	}

	public function getAvailablePageWrapper($url, $page, $rel = null)
	{
		$rel = is_null($rel) ? '' : ' rel="'.$rel.'"';

		return '<li class="page"><a href="'.htmlentities($url).'"'.$rel.'>'.$page.'</a></li>';
	}

	public function getFirstButton(){
		if($this->paginator->currentPage() <= 1){
			return $this->getDisabledTextWrapper('');
		}
		$url = $this->paginator->url(0);

		return '<li class="first"><a href="'.htmlentities($url).'">‹‹</a></li>';
	}

	public function getLastButton(){
		if($this->lastPage() == $this->paginator->currentPage()){
			return $this->getDisabledTextWrapper('');
		}
		$url = $this->paginator->url($this->lastPage());

		return '<li class="last"><a href="'.htmlentities($url).'">››</a></li>';
	}
	/**
	 * Get the next page pagination element.
	 *
	 * @param  string  $text
	 * @return string
	 */
	public function getNextButton($text = '')
	{
		// If the current page is greater than or equal to the last page, it means we
		// can't go any further into the pages, as we're already on this last page
		// that is available, so we will make it the "next" link style disabled.
		if (! $this->paginator->hasMorePages()) {
			return $this->getDisabledTextWrapper($text);
		}

		$url = $this->paginator->url($this->paginator->currentPage() + 1);

		return '<li class="next"><a href="'.htmlentities($url).'"></a></li>';
	}

	/**
	 * Get the previous page pagination element.
	 *
	 * @param  string  $text
	 * @return string
	 */
	public function getPreviousButton($text = '')
	{
		// If the current page is less than or equal to one, it means we can't go any
		// further back in the pages, so we will render a disabled previous button
		// when that is the case. Otherwise, we will give it an active "status".
		if ($this->paginator->currentPage() <= 1) {
			return $this->getDisabledTextWrapper($text);
		}

		$url = $this->paginator->url(
			$this->paginator->currentPage() - 1
		);

		return '<li class="prev"><a href="'.htmlentities($url).'"></a></li>';
	}

}