<?php namespace App\Lib\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Big implements FilterInterface
{
	public function applyFilter(Image $image)
	{
		return $image->widen(1280, function ($constraint) {
			$constraint->upsize();
		});
	}
}