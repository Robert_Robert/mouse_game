<?php namespace App\Lib\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class MainImage implements FilterInterface
{
	public function applyFilter(Image $image)
	{
		return $image->fit(556, 278);
	}
}