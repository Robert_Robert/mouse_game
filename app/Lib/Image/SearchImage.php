<?php namespace App\Lib\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class SearchImage implements FilterInterface
{
	public function applyFilter(Image $image)
	{
		return $image->fit(110, 55);
	}
}