<?php namespace App\Lib\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class GameCard implements FilterInterface
{
	public function applyFilter(Image $image)
	{
		return $image->fit(369, 184);
	}
}