<?php namespace App\Lib\Image;

use Intervention\Image\Image as iImage;
use Intervention\Image\Filters\FilterInterface;

class Play implements FilterInterface
{
	public function applyFilter(iImage $image)
	{
		$image->fit(109, 54);

		$watermark = \Img::make(base_path().'/resources/image/play.png');
		$image->insert($watermark, 'center');

		return $image;
	}
}