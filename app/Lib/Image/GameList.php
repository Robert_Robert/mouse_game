<?php namespace App\Lib\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class GameList implements FilterInterface
{
	public function applyFilter(Image $image)
	{
		return $image->fit(172, 86);
	}
}