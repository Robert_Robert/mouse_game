<?php namespace App\Observers;

use App\Models\News as Model;

class NewsObserver{
	protected function requiredFields(Model $model){
		return !empty($model->title);
	}
	protected function prepareFields(Model $model){
		$model->title = trim($model->title);
		$model->content = trim($model->content);
		$model->description = trim($model->description);

		$model->image = trim($model->image); /** @TODO-agelnash валидация существования картинки */

		if(empty($model->slug) && $model->slug !== '0'){
			$model->sluggify(true);
		}

		return $this;
	}
	public function creating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}