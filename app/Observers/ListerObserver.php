<?php namespace App\Observers\Game;

use App\Models\Lister as Model;

class ListerObserver{
	protected function requiredFields(Model $model){
		return !empty($model->name);
	}
	protected function prepareFields(Model $model){
		$model->name = trim($model->name);

		if(empty($model->slug) && $model->slug !== '0'){
			$model->sluggify(true);
		}

		return $this;
	}
	public function creating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}