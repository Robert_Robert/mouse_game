<?php namespace App\Observers;

use App\Models\Seo as Model;

class SeoObserver{
	/**
	 * Нельзя хранить связи к несуществующим записям
	 *
	 * (Хотя может оно и не нужно, если никто не будет лезть в базу самостоятельно)
	 */
	protected function requiredFields(Model $model){
		$out = ($model->document_id && $model->document_type);
		if($out){
			$out = (bool) $model->seoble();
		}
		return $out;
	}
	protected function prepareFields(Model $model){
		$model->title = mb_ucfirst( trim($model->title) );
		$model->description = mb_ucfirst( trim($model->description) );

		$model->frequency = trim($model->frequency);

		if(empty($model->background)){
			$model->background_url = '';
		}
		$model->state = trim($model->state);
		if(!in_array($model->state, ['dynamic', 'static'])){
			$model->state = '';
		}
		$model->robots = trim($model->robots);
		return $this;
	}
	public function creating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}