<?php namespace App\Observers;

use App\Models\Slider as Model;
use Request;

class SliderObserver{
	/** проверка минимально обязательных для заполнения полей */
	protected function requiredFields(Model $model){
		return !empty($model->image);
	}
	protected function prepareFields(Model $model){
		$model->left = trim( $model->left );
		$model->right = trim( $model->right );
		$model->image = trim( $model->image );
		$model->sort = (int)$model->sort;
		$model->link = str_replace('http://'.Request::server('HTTP_HOST'), '', trim( $model->link ));
		return $this;
	}
	public function creating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}