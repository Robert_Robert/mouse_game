<?php namespace App\Observers;

use App\Models\Keyword as Model;
use Illuminate\Support\Str;

class KeywordObserver{
	protected function requiredFields(Model $model){
		return !empty($model->name);
	}
	protected function prepareFields(Model $model){
		$model->name = trim( Str::lower($model->name) );
		return $this;
	}
	public function creating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}