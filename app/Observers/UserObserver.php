<?php namespace App\Observers;

use App\Models\User as Model;
use Illuminate\Support\Str;

class UserObserver{
	protected function requiredFields(Model $model){
		return !empty($model->email) && !empty($model->name) && !empty($model->password);
	}
	protected function prepareFields(Model $model){
		$model->name = trim( $model->name);
		$model->email = Str::lower(trim( $model->email ));
		if(!empty($model->referrer_id) && Model::where('id', '=', (int)$model->referrer_id)->exists()){
			$model->referrer_id = (int)$model->referrer_id;
		} else{
			$model->referrer_id = null;
		}
		return $this;
	}
	public function creating(Model $model){
		$model->confirmation_code = str_random(30);
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}