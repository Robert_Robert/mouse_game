<?php namespace App\Observers;

use App\Models\Game as Model;
use Carbon\Carbon;
use App\Lib as Lib;

class GameObserver{
	protected function requiredFields(Model $model){
		/*$empty = value(function() use($model){
			$flag = false;
			$attrs = ['title', 'content', 'image', 'release', 'price', 'developer_id', 'vendor_id'];
			foreach($attrs as $attr){
				if(($flag = empty($model->$attr)) === true) break;
			}
			return $flag;
		});
		$developer = true;//(bool)\App\Models\Game\Developer::find($model->developer_id);
		$vendor = true;//(bool)\App\Models\Game\Vendor::find($model->vendor_id);

		return !$empty && $developer && $vendor;*/
		return true;
	}
	protected function prepareFields(Model $model){
		$model->title = trim( $model->title );
		$model->list_title = trim($model->list_title);
		$model->description = trim( $model->description );
		$model->content = trim( $model->content );
		$model->requirement = trim( $model->requirement );
		$model->install = trim( $model->install );
		$model->video = trim( $model->video );
		$model->price = (int)$model->price;
		$model->metacritic = value(function() use($model){
			$rate = (int)$model->metacritic;

			$rate = $rate >= 100 ? 100 : $rate;
			$rate = $rate <= 0 ? 0 : $rate;

			return $rate;
		});
		$model->fullprice = (int)$model->fullprice;
		$model->realprice = $model->fullprice ? $model->fullprice : $model->price;
		$model->published = (bool)$model->published;
		$model->hiddened = (bool)$model->hiddened;
		$model->rate = (int)$model->rate;
		$model->release = value(function() use($model){
			$date = $model->release;
			if($date instanceof Carbon){
				$date = $model->release->format('Y-m-d');
			}
			$date = validate_date($date);
			if(empty($date)){
				$date = with(new Carbon)->format('Y-m-d');
			}
			return $date;
		});
		if(empty($model->slug) && $model->slug !== '0'){
			$model->sluggify(true);
		}
		$model->char = mb_substr(mb_strtolower($model->title), 0, 1);
		if(empty($model->status_id)){
			$model->status_id = 3;
		}
		return $this;
	}

	public function saved(Model $model){
		/*$filesystem = \App::make('files');
		if( ! $filesystem->exists(public_path().'/images/video/')) {
			$filesystem->makeDirectory(public_path() . '/images/video/');
		}
		if(!empty($model->video)){
			$video = new Lib\Video($model->video);
			file_put_contents(public_path().'/images/video/'.$model->id.'.jpg', fopen($video->getImage(), 'r'));
		}else{
			$filesystem->copy(public_path().'/images/noimage.jpg', public_path().'/images/video/'.$model->id.'.jpg');
		}*/

		if($model->lists->count()){
			foreach($model->lists as $list){
				switch($list->slug){
					case 'preorder':{
						if($model->status->slug != 'prestok') $list->detachGames($model->id);
						break;
					}
					case 'sale':{
						if(empty($model->fullprice) || $model->status->buyer == 0) $list->detachGames($model->id);
						break;
					}
				}
			}
		}
	}
	public function creating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}