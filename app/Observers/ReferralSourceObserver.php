<?php namespace App\Observers;

use App\Models\ReferralSource as Model;

class ReferralSourceObserver{
	/** проверка минимально обязательных для заполнения полей */
	protected function requiredFields(Model $model){
		return true;
	}
	protected function prepareFields(Model $model){
		$model->source = trim( $model->source );
		$model->landing = trim( $model->landing );
		$model->ip = trim( $model->ip );

		return $this;
	}
	public function creating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function updating(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
	public function saving(Model $model){
		return $this->prepareFields($model)->requiredFields($model);
	}
}