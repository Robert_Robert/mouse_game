<?php
/** Правила валидации id параметра */
Route::pattern('id', '[0-9]+');

/*Route::get('test', function(){
	$out = [];

	//Dead Space 3 (47) к нему бонусом идет Dead Space (44)
	$game = App\Models\Game::findOrFail(44);
	if($game->bountyFree){
		$tmp = [];
		foreach($game->bountyFree as $item){
			$tmp[] = [
				$item->active_from, $item->active_to, //Определяем даты до которого числа этот товар поставляется бонусом
				$item->conditions->lists('title', 'id') //Определяем товары которые должны быть в корзине для активации бонуса
			];
		}
		$out[] = $tmp;
	}

	$game = App\Models\Game::findOrFail(47);
	if($game->bountyFreeCondition){
		$tmp = [];
		foreach($game->bountyFreeCondition as $item){
			$tmp[] = [
				$item->active_from, $item->active_to, //Определяем даты до которого числа этот товар поставляет бонусы
				$item->games->lists('title', 'id') //Определяем товары которые поставляются бонусом
			];
		}
		$out[] = $tmp;
	}

	return show_var($out);
});*/
Route::get('robots.txt', array('as' => 'robots.txt', 'uses' => 'TextController@txtRobots'));
Route::get('humans.txt', array('as' => 'humans.txt', 'uses' => 'TextController@txtHumans'));
Route::get('hackers.txt', array('as' => 'hackers.txt', 'uses' => 'TextController@txtHackers'));

//Корзина
Route::controller('cart', 'CartController', array(
	'getIndex' => 'cart.index',
	'getAddItem' => 'cart.add',
	'getSetItem' => 'cart.set',
	'getDelItem' => 'cart.del',
	'getClear' => 'cart.clear',
	'postSaveOrder' => 'order.create'
));

//Поиск
Route::get('search', array('as' => 'search', 'uses' => 'TextController@runSearch'));

// Обзор текста
Route::get('text/{slug}', array('as' => 'text.view', 'uses' => 'TextController@showText'));

// Обзор новости
Route::get('news/{slug}', array('as' => 'news.view', 'uses' => 'NewsController@showNews'));

// Обзор игры
Route::get('game/{slug}', array('as' => 'game.view', 'uses' => 'GameController@showGame'));
Route::get('game', function () {
	return Redirect::action('GameController@getIndex', array(), 301);
});
Route::controller('games', 'GameController', array(
	'getRecommendations' => 'game.recommendations',
	'getSale' => 'game.sale',
	'getNew' => 'game.new',
	'getBestSeller' => 'game.bestsaller',
	'getPreOrder' => 'game.preorder'
));
Route::controllers([
	'text' => 'TextController',
	'news' => 'NewsController',
	//'auth' => 'Auth\AuthController',
	//'password' => 'Auth\PasswordController',
	'ajax' => 'AjaxController',
	'auth' => 'AuthController',
	'profile' => 'ProfileController'
]);
Route::getRoutes()->getByAction('App\Http\Controllers\TextController@getIndex')->setUri('/');