<?php namespace App\Http\Controllers;

use App\Models as Models;
use Mail, Input, Request;
use App\Lib\Helper;

class AjaxController extends Controller {
	public function __construct(){
		$this->middleware('ajax');
		$this->setupTheme('v1');
	}
	public function getToken(){
		return csrf_token();
	}
}