<?php namespace App\Http\Controllers;

use App\Models as Models;
use App\Lib\Helper;
use JavaScript, Cart, Input;
use Illuminate\Contracts\Auth\Guard;

class CartController extends Controller
{
	protected $game = null;
	protected $auth = null;

	public function __construct(Guard $auth, Models\Game $game)
	{
		$this->middleware('ajax',  ['except' => [
			'getIndex'
		]]);
		$this->setupTheme('v1');

		$this->auth = $auth;
		$this->game = $game;
	}
	public function getIndex(){
		if(is_ajax()){
			$out = $this->makeResponse('Способ оплаты успешно изменен', true);
		}else {
			$this->theme->set('middle_class', 'cartPage');
			JavaScript::put([
				'pageType' => 'cart'
			]);

			$obj = Models\Text::findBySlugOrFail('cart');
			$this->theme->breadcrumb()->add($obj->title, $obj->url());
			$this->seoByModel($obj);

			$games = Models\Game::whereIn('id', Cart::getContent()->keys())->get();
			foreach ($games as $game) {
				if ($game->status->buyer == 0) {
					Cart::remove($game->getKey());
				}
			}
			$out = $this->theme->layout('sidebars')->scope('pages.cart', [
				'content' => $obj->content,
				'page' => $obj,
			])->render();
		}

		return $out;
	}
	public function getAddItem($id){
		/** @var Models\Game $game */
		$game = $this->game->findOrNew($id);
		if($game->exists && $game->published){
			if($game->status->buyer){
				if(Cart::has($game->getKey())){
					Cart::update($game->getKey(), ['quantity' => 1]);
				}else {
					Cart::add($game->getKey(), $game->title, $game->user_price, 1);
				}
				$out = $this->makeResponse('Товар добавлен в корзину', true);
			}else{
				Cart::remove($game->getKey());
				$out = $this->makeResponse('Запрещено заказывать товар со статусом ожидается', false);
			}
		}else{
			$out = $this->makeResponse('Товар не существует', false);
		}
		return $out;
	}
	public function getSetItem($id){
		$count = (int)Input::get('count');
		/** @var Models\Game $game */
		$game = $this->game->findOrNew($id);
		if($game->exists && $game->published) {
			if($game->status->buyer){
				if($count > 0) {
					if (Cart::has($game->getKey())) {
						Cart::update($game->getKey(), array(
							'quantity' => array(
								'relative' => false,
								'value' => $count
							),
						));
					} else {
						Cart::add($game->getKey(), $game->title, $game->user_price, $count);
					}
					$out = $this->makeResponse('Обновлено количество позиций в вашей корзине', true);
				}else{
					$out = $this->makeResponse('Указано неверное кол-во товара', false);
				}
			}else{
				Cart::remove($game->getKey());
				$out = $this->makeResponse('Запрещено заказывать товар со статусом ожидается', false);
			}

		}else{
			$out = $this->makeResponse('Товар не существует', false);
		}

		return $out;
	}
	public function getDelItem($id){
		Cart::remove($id);
		return $this->makeResponse('Товар удален из корзины', true);
	}
	public function getClear(){
		Cart::clear();
		return $this->makeResponse('Ваша корзина очищена', true, ['reload' => true]);
	}
	public function postSaveOrder(){
		if($this->auth->guest()){
			$out = $this->makeResponse('Для оформления заказа вам необходимо авторизоваться', false, [
				'reload' => false
			]);
		}else {
			/**
			 * @TODO: Создать модуль для набора аукционных товаров (Указывается стоимость и выбирается несколько товаров).
			 * 		  Если в корзине товары попадают под акцию, то брать цену из таблицы акций
			 * @TODO: Запретить продавать товар лицам с определенной страницы (проверка)
			 * @TODO: сохранение заказа в базу и перекидывать на страницу оплаты
			 */
			$out = $this->makeResponse('@TODO: Ваш заказ успешно оформлен', true, [
				'reload' => true
			]);
		}
		return $out;
	}

	protected function makeResponse($message, $flag = true, $params = array()){
		$payment = Input::get('method', '');
		$percent = Helper::payment($payment);
		$commission = number_format(Cart::getSubTotal() * $percent / 100, 2, '.', ' ');

		return array_merge($params, [
			'alert' => true,
			'reload' => false,
			'sum' => [
				'price' => Cart::getSubTotal(),
				'commission' => $commission,
				'pay' => number_format($commission + Cart::getSubTotal(), 2, '.', ' ')
			],
			'count' => [
				'quantity' => Cart::getTotalQuantity(),
				'positions' => Cart::getContent()->count(),
			]
		], [
			'text' => $message,
			'status' => $flag ? 'ok' : 'error'
		]);
	}
}