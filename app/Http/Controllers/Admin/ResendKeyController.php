<?php namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use Admin;

class ResendKeyController extends Controller {
	public function getIndex(){
		$view = View::make('admin.page.resend-key');
		return Admin::view($view, 'Повторная отправка ключей');
	}
}