<?php namespace App\Http\Controllers;

use App\Models\Text;
use JavaScript;

class ProfileController extends Controller
{
	/**
	 * Create a new sessions controller instance.
	 */
	public function __construct(Text $obj)
	{
		$this->middleware('auth');

		$this->setupTheme('v1');

		$this->obj = $obj;

	}
	/**
	 * Show the login page.
	 *
	 * @return \Response
	 */
	public function getIndex()
	{
		// В Session::get('message') бывает сообщение

		$obj = $this->obj->findBySlugOrFail('profile');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$this->theme->set('middle_class','personalPage');
		JavaScript::put([
			'pageType' => 'profile'
		]);
		return $this->theme->layout('text')->scope('pages.profile', ['page' => $obj])->render();
	}

	public function getPartnership()
	{
		// В Session::get('message') бывает сообщение

		$obj = $this->obj->findBySlugOrFail('profile');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('partnership');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$this->theme->set('middle_class','partnership');
		JavaScript::put([
			'pageType' => 'partnership'
		]);
		return $this->theme->layout('text')->scope('pages.partnership', ['page' => $obj])->render();
	}

	public function getPartnershipRef()
	{
		// В Session::get('message') бывает сообщение

		$obj = $this->obj->findBySlugOrFail('profile');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('partnership');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('partnership-ref');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$this->theme->set('middle_class','partnership');
		JavaScript::put([
			'pageType' => 'partnership'
		]);
		return $this->theme->layout('text')->scope('pages.partnership-ref', ['page' => $obj])->render();
	}

	public function getPartnershipUsers()
	{
		// В Session::get('message') бывает сообщение

		$obj = $this->obj->findBySlugOrFail('profile');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('partnership');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('partnership-users');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$this->theme->set('middle_class','partnership');
		JavaScript::put([
			'pageType' => 'partnership'
		]);
		return $this->theme->layout('text')->scope('pages.partnership-users', ['page' => $obj])->render();
	}

	public function getPartnershipDesc()
	{
		// В Session::get('message') бывает сообщение

		$obj = $this->obj->findBySlugOrFail('profile');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('partnership');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('partnership-descr');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$this->theme->set('middle_class','partnership');
		JavaScript::put([
			'pageType' => 'partnership'
		]);
		return $this->theme->layout('text')->scope('pages.partnership-desc', ['page' => $obj])->render();
	}

	public function getEdit()
	{
		// В Session::get('message') бывает сообщение

		$obj = $this->obj->findBySlugOrFail('profile');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('profile-edit');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$this->theme->set('middle_class','editPage');
		JavaScript::put([
			'pageType' => 'editProfile'
		]);
		return $this->theme->layout('text')->scope('pages.profile-edit', ['page' => $obj])->render();
	}

	public function getPurchases()
	{
		// В Session::get('message') бывает сообщение

		$obj = $this->obj->findBySlugOrFail('profile');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('purchases');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$this->theme->set('middle_class','mypurchases');
		JavaScript::put([
			'pageType' => 'purchases'
		]);
		return $this->theme->layout('text')->scope('pages.purchases', ['page' => $obj])->render();
	}
}