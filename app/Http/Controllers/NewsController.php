<?php namespace App\Http\Controllers;

use JavaScript;
use App\Models as Models;

class NewsController extends Controller {

	protected $obj = null;
	public function __construct(){
		$this->setupTheme('v1');

		$this->obj = '';
	}

	/**
	 * @return mixed
	 * set('middle_class') - класс для блока контента
	 * set('partial') - шаблон сайдбара | если не указано принимает значение cart-rigtSidebar
	 */
	public function getIndex(){
		$this->theme->set('middle_class','news_page');
		JavaScript::put([
			'pageType' => 'news-list'
		]);

		$obj = Models\Text::findBySlugOrFail('news');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		$news = Models\News::orderBy('published_at', 'DESC')
			->orderBy('created_at', 'DESC')
			->paginate(16);
		if(($out = $news->checkPaginate()) === true) {
			$out = $this->theme->layout('sidebars')->scope('pages.news-list', [
				'content' => $obj->content,
				'page' => $obj,
				'news' => $news
			])->render();
		}
		return $out;
	}
	public function showNews($slug){
		$this->theme->set('middle_class','news_page');
		JavaScript::put([
			'pageType' => 'news'
		]);

		$obj = Models\Text::findBySlugOrFail('news');
		$this->theme->breadcrumb()->add($obj->title, $obj->url('news'));

		$news = Models\News::findBySlugOrFail($slug);
		$this->theme->breadcrumb()->add($news->title, $obj->url());
		$this->seo()->opengraph()->addImage(url($news->thumb('big')));
		$this->seoByModel($news);

		return $this->theme->layout('sidebars')->scope('pages.news', [
			'news' => $news
		])->render();
	}
}
