<?php namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Theme;
use JavaScript;
use URL;

abstract class Controller extends BaseController {

	use DispatchesJobs, ValidatesRequests, SEOToolsTrait;

	/** @var Theme */
	protected $theme = null;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupTheme($name)
	{
		$this->theme = Theme::uses($name);
	}
	protected function seoByModel(Model $model){
		$fullTitle = $model->fullTitle ? $model->fullTitle : $model->title;
		if( ! empty($model->seo->title)) {
			$this->seo()->metatags()->setTitle($model->seo->title);
		}else{
			$title = $fullTitle;
			$title .= config('seotools.meta.defaults.separator');
			$title .= 'MouseGame';
			$this->seo()->metatags()->setTitle($title);
		}
		if( ! empty($model->seo->description)) {
			$this->seo()->metatags()->setDescription($model->seo->description);
		}

		$this->seo()->metatags()->addMeta('robots', $model->seo_robots);

		$this->seo()->metatags()->addMeta('document-state', $model->seo_state);

		$this->seo()->metatags()->addMeta('content-language', 'ru');

		if($model->seo) {
			foreach ($model->seo->keywords as $keyword) {
				$this->seo()->metatags()->addKeyword($keyword->name);
			}
		}

		$this->seo()->opengraph()->setUrl($model->url());
		$this->seo()->opengraph()->addProperty('type', 'website');
		$this->seo()->twitter()->setSite('@agel_nash');

		$this->theme->set('h1', empty($model->seo->h1) ? $fullTitle : $model->seo->h1);

		if(!empty($model->seo->background)) {
			$this->theme->set('background', $model->seo->background);
			if(!empty($model->seo->background_url)) {
				$this->theme->set('background_url', $model->seo->background_url);
			}
		}
	}

}
