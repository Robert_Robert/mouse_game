<?php namespace App\Http\Controllers;

use App\Models as Models;
use View;
use JavaScript;
use Input;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Collection;

class TextController extends Controller {

	protected $obj = null;

	//public function __construct(Text $obj){
	public function __construct(){
		$this->setupTheme('v1');

		$this->obj = '';//$obj;
	}

	public function getIndex(){
		$page = Models\Text::with('seo.keywords')->whereSlug('index')->firstOrFail();
		$this->seoByModel($page);

		$games = Models\Game::where('hiddened', '=', 0)->SortPagination();

		if(($out = $games->checkPaginate()) === true){
			JavaScript::put([
				'pageType' => 'index'
			]);
			$out = $this->theme->layout('default')->scope('pages.index', [
				'page' => $page,
				'content' => $page->content,
				'games' => $games
			])->render();
		}

		return $out;
	}
	public function runSearch(){
		$out = '';
		$page = Models\Text::with('seo.keywords')->whereSlug('search')->firstOrFail();
		$query = Input::get('query');
		$gameObj = new Models\Game;

		if(is_ajax()){
			if(trim($query) == ''){
				$out = new Collection;
			}else{
				$out = $gameObj->with('status')->where('published', '=', 1)->orderBy('title', 'ASC')->where(function ($q) use ($query) {
					//По Slug не ищем
					foreach (['title'] as $column) {
						$q->orWhere($column, 'like', '%' . $query . '%');
					}
				});
				$out = $out->limit(9)->get();
			}
			$data = [];
			foreach($out as $item){
				$data[] = ['value' => $item->splitTitle(30, 1), 'data' => [
					'title' => $item->splitTitle(30, 1),//real_list_title,
					'url' => $item->url(),
					'image' => $item->thumb('search-image'),
					'price' => $item->price,
					'fullprice' => $item->fullprice,
					//'sale' => abs($item->sale),
					'status' => [
						'buy' => $item->status->buyer,
						'slug' => $item->status->slug
					]
				]];
			}
			if($out->count() > 1) {
				$data[] = ['value' => 'Показать все', 'data' => [
					'url' => action('\App\Http\Controllers\TextController@runSearch', ['query' => $query]),
					'title' => 'Показать все результаты поиска на одной странице...'
				]];
			}
			$out = [
				'query' => $query,
				'suggestions' => $data
			];
		}else {
			if(trim($query) == '') {
				$games = new Collection;
			}else{
				$games = $gameObj->with('status')->where('published', '=', 1)->where(function ($q) use ($query) {
					//По Slug не ищем
					foreach (['title'] as $column) {
						$q->orWhere($column, 'like', '%' . $query . '%');
					}
				})->SortPagination();
			}

			if ($games->count() == 0 || ($out = $games->checkPaginate()) === true) {
				if($games->count() == 1){
					$game = $games->first();
					$out = \Redirect::to($game->url());
				}else {
					$this->theme->set('middle_class', 'categoryPage');

					$this->theme->breadcrumb()->add(Models\Text::findBySlugOrFail('games')->title, $gameObj->url('lists'));
					$this->theme->breadcrumb()->add($page->title . ' по запросу "' . e($query) . '"');

					$page->fullTitle = $page->title . ' по запросу "' . e($query) . '"';
					$page->seo->title = $page->title . ' по запросу "' . e($query) . '"';

					$this->seoByModel($page);

					JavaScript::put([
						'pageType' => 'search'
					]);
					$out = $this->theme->layout('text')->scope('pages.game-char', [
						'page' => $page,
						'content' => $page->content,
						'games' => $games
					])->render();
				}
			}
		}
		return $out;
	}
	public function showText($type){
		$page = Models\Text::with('seo.keywords')->whereSlug($type)->whereSystem(0)->firstOrFail();
		$this->seoByModel($page);

		$this->theme->breadcrumb()->add($page->title, $page->url());

		$tpl = 'pages.'.$type;
		$path = $this->theme->getThemeNamespace($this->theme->getConfig('containerDir.view').'.'.$tpl);
		if( ! View::exists($path)){
			$type = 'text';
			$tpl = 'pages.text';
		}

		JavaScript::put([
			'pageType' => $type
		]);

		return $this->theme->layout('text')->scope($tpl, compact('page'))->render();
	}

	public function txtRobots(){
		$content = view('txt.robots');
		return (new Response($content))->header('Content-Type', 'text/plain');
	}
	public function txtHackers(){
		$content = view('txt.hackers');
		return (new Response($content))->header('Content-Type', 'text/plain');
	}
	public function txtHumans(){
		$content = view('txt.humans');
		return (new Response($content))->header('Content-Type', 'text/plain');
	}
}
