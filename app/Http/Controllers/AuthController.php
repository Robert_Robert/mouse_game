<?php namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Text;
use Illuminate\Http\Request;
use Auth,Mail;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Redirect;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Models\ReferralSource;

class AuthController extends Controller
{
	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @param 	\App\Models\Text $obj
	 */
	public function __construct(Guard $auth, Registrar $registrar, PasswordBroker $passwords, Text $obj)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);

		$this->setupTheme('v1');

		$this->obj = $obj;
		$this->passwords = $passwords;
	}

	/**
	 * Show the register page.
	 *
	 * @return \Response
	 */
	public function getRegister()
	{
		$obj = $this->obj->findBySlugOrFail('register');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		return $this->theme->layout('text')->scope('pages.register')->render();
	}
	/**
	 * Perform the registration.
	 *
	 * @param  Request   $request
	 * @return \Redirect
	 */
	public function postRegister(Request $request)
	{
		$request->setTrustedProxies(array('127.0.0.1'));

		$this->validate($request, [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed',
			'rule' => 'required'
		]);
		$user = User::create(array_merge(
			$request->only(['name', 'email', 'password']),
			[
				'reg_ip' => $request->getClientIp(),
				'reg_referer_page' => $request->headers->get('referer'),
				'referrer_id' => $request->cookie('referral')
			]
		));
		$source = ReferralSource::findOrNew(session()->get('referral_source'));
		if($source->exists){
			$source->user_id = $user->getKey();
			$source->save();
		}

		Mail::queue(['emails.forms.html.register', 'emails.forms.text.register'], compact('user'), function ($m) use($user){
			$m->to($user->email)->subject('Регистрация на сайте');
		});
		return Redirect::action('\App\Http\Controllers\AuthController@getLogin')->with('status', 'Пожалуйста, подтвердите свой email.');
	}
	/**
	 * Confirm a user's email address.
	 *
	 * @param  string $token
	 * @return mixed
	 */
	public function getConfirmEmail($token)
	{
		if (is_null($token)) {
			throw new NotFoundHttpException;
		}

		$user = User::where('confirmation_code', $token)->firstOrFail();
		$user->confirmed = true;
		$user->confirmation_code = null;
		$user->save();
		Auth::login($user);
		return Redirect::action('\App\Http\Controllers\AuthController@getLogin')->with('status', 'Ваш E-mail подтвержден.');
	}

	/**
	 * Show the login page.
	 *
	 * @return \Response
	 */
	public function getLogin()
	{
		$obj = $this->obj->findBySlugOrFail('login');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		return $this->theme->layout('text')->scope('pages.login')->render();
	}
	/**
	 * Perform the login.
	 *
	 * @param  Request  $request
	 * @return \Redirect
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, ['email' => 'required|email', 'password' => 'required']);
		if ($this->signIn($request)) {
			//flash('Welcome back!');
			return Redirect::intended(action('\App\Http\Controllers\ProfileController@getIndex'));
		}
		//flash('Could not sign you in.');
		return Redirect::back()->withErrors('Пользователь не обнаружен или вы указали не верный пароль');
	}
	/**
	 * Destroy the user's current session.
	 *
	 * @return \Redirect
	 */
	public function getLogout()
	{
		Auth::logout();
		return Redirect::action('\App\Http\Controllers\AuthController@getLogin');
	}

	public function getForgot(){
		$obj = $this->obj->findBySlugOrFail('login');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('forgot');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		return $this->theme->layout('text')->scope('pages.forgot')->render();
	}

	public function postForgot(Request $request){
		$this->validate($request, ['email' => 'required|email']);

		$response = $this->passwords->sendResetLink($request->only('email'), function (Message $message) {
			$message->subject('Восстановление пароля');
		});

		switch ($response) {
			case Password::RESET_LINK_SENT:
				return redirect()->back()->with('status', trans($response));

			case Password::INVALID_USER:
				return redirect()->back()->withErrors(['email' => trans($response)]);
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return \Illuminate\Http\Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) {
			throw new NotFoundHttpException;
		}

		$obj = $this->obj->findBySlugOrFail('login');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('forgot');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());

		$obj = $this->obj->findBySlugOrFail('password-reset');
		$this->theme->breadcrumb()->add($obj->title, $obj->url());
		$this->seoByModel($obj);

		return $this->theme->layout('text')->scope('pages.reset', compact('token'))->render();
	}

	/**
	 * Reset the given user's password.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postReset(Request $request)
	{
		$this->validate($request, [
			'token' => 'required',
			'email' => 'required|email',
			'password' => 'required|confirmed|min:6',
		]);

		$credentials = $request->only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function ($user, $password) {
			$user->password = $password;
			$user->confirmed = true;
			$user->confirmation_code = null;
			$user->save();
			Auth::login($user);
		});

		switch ($response) {
			case Password::PASSWORD_RESET:
				return Redirect::action('\App\Http\Controllers\ProfileController@getIndex')->with('status', 'Пароль обновлен');

			default:
				return Redirect::back()
					->withInput($request->only('email'))
					->withErrors(['email' => trans($response)]);
		}
	}

	/**
	 * Attempt to sign in the user.
	 *
	 * @param  Request $request
	 * @return boolean
	 */
	protected function signIn(Request $request)
	{
		$data = [
			'email'    => $request->input('email'),
			'password' => $request->input('password'),
			'confirmed' => true
		];
		return Auth::attempt($data, $request->has('remember'));
	}
}