<?php namespace App\Http\Controllers;

use App\Models as Models;
use JavaScript;
use Illuminate\Support\Str;
use Sorter;
use AgelxNash\SEOPagination\Paginator\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Input;

class GameController extends Controller {
	protected $obj = null;

	//public function __construct(Text $obj){
	public function __construct(Models\Game $obj){
		$this->setupTheme('v1');

		$this->obj = $obj;
	}

	public function getIndex(){
		$page = Models\Text::findBySlugOrFail('games');

		$games = Models\Game::where('hiddened', '=', 0)->SortPagination();

		if(($out = $games->checkPaginate()) === true){
			$this->theme->set('middle_class','categoryPage');
			$this->theme->set('partial','game-rightSidebar');

			//$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));

			$this->seoByModel($page);

			JavaScript::put([
				'pageType' => 'games-list'
			]);
			$out = $this->theme->layout('text')->scope('pages.game-list', [
				'games' => $games,
				'content' => $page->content,
				'page' => $page
			])->render();
		}

		return $out;
	}

	public function showGame($game){
		$game = Models\Game::with([
			'seo.keywords',
			'platforms',
			'janrs',
			'langtext',
			'mode',
			'regions',
			'developers',
			'vendors',
			'years'
		])->where('published', '=', 1)->whereSlug($game)->firstOrFail();
		$this->seoByModel($game);
		$this->seo()->opengraph()->addImage(url($game->thumb('big')));

		$this->theme->set('itemtype', 'http://schema.org/Product');
		$this->theme->set('game', $game);
		$this->theme->set('middle_class','gamePage');
		$this->theme->set('partial','game-rightSidebar');

		$obj = Models\Text::findBySlugOrFail('games');

		$this->theme->breadcrumb()->add($obj->title, $game->url('lists'));
		$this->theme->breadcrumb()->add($game->title, $game->url());

		JavaScript::put([
			'pageType' => 'game'
		]);

		return $this->theme->layout('sidebars')->scope('pages.game', [
			'game' => $game
		])->render();
	}

	public function getType($type = null){
		$out = null;
		if(empty($type)){
			\App::abort(404);
		}else {
			$type = Models\Game\Janr::findBySlugOrFail($type);

			$this->theme->set('type', $type);

			if($type->games()->count() < 6) {
				$bigGames = new Collection;
				$games = $type->games()->SortPagination();
			} else {
				$bigGames = $type->games()->orderBy('rate', 'DESC')->take(2)->get();
				$games = $type->games()->whereNotIn($this->obj->getTable().'.id', $bigGames->lists('id')->toArray())->SortPagination();
			}
			if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
				$this->theme->set('middle_class', 'categoryPage');

				$obj = Models\Text::findBySlugOrFail('games');
				$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));
				$this->theme->breadcrumb()->add($type->fullTitle, $type->url());

				$this->seoByModel($type);

				JavaScript::put([
					'pageType' => 'game-type'
				]);

				$out = $this->theme->layout('text')->scope('pages.game-list', [
					'type' => $type,
					'content' => $type->content,
					'games' => $games,
					'bigGames' => $bigGames
				])->render();
			}
		}
		return $out;
	}

	public function getMode($mode = null){
		$out = null;
		if(empty($mode)){
			\App::abort(404);
		}else {
			$mode = Models\Game\Mode::findBySlugOrFail($mode);

			$this->theme->set('type', $mode);

			if($mode->games()->count() < 6) {
				$bigGames = new Collection;
				$games = $mode->games()->SortPagination();
			} else {
				$bigGames = $mode->games()->orderBy('rate', 'DESC')->take(2)->get();
				$games = $mode->games()->whereNotIn($this->obj->getTable().'.id', $bigGames->lists('id')->toArray())->SortPagination();
			}
			if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
				$this->theme->set('middle_class', 'categoryPage');

				$obj = Models\Text::findBySlugOrFail('games');
				$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));
				$this->theme->breadcrumb()->add($mode->fullTitle, $mode->url());

				$this->seoByModel($mode);

				JavaScript::put([
					'pageType' => 'game-type'
				]);

				$out = $this->theme->layout('text')->scope('pages.game-list', [
					'type' => $mode,
					'content' => $mode->content,
					'games' => $games,
					'bigGames' => $bigGames
				])->render();
			}
		}
		return $out;
	}

	public function getYear($year = null){
		$out = null;
		if(is_null($year) || $year === '' || is_array($year)){
			\App::abort(404);
		}else {
			$year = Models\Game\Year::findBySlugOrFail($year);

			$this->theme->set('type', $year);

			if($year->games()->count() < 6) {
				$bigGames = new Collection;
				$games = $year->games()->SortPagination();
			} else {
				$bigGames = $year->games()->orderBy('rate', 'DESC')->take(2)->get();
				$games = $year->games()->whereNotIn($this->obj->getTable().'.id', $bigGames->lists('id')->toArray())->SortPagination();
			}
			if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
				$this->theme->set('middle_class', 'categoryPage');

				$obj = Models\Text::findBySlugOrFail('games');
				$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));
				$this->theme->breadcrumb()->add($year->fullTitle, $year->url());

				$this->seoByModel($year);

				JavaScript::put([
					'pageType' => 'game-type'
				]);

				$out = $this->theme->layout('text')->scope('pages.game-list', [
					'type' => $year,
					'content' => $year->content,
					'games' => $games,
					'bigGames' => $bigGames
				])->render();
			}
		}
		return $out;
	}

	public function getPlatforms($platform = null){
		$out = null;
		if(empty($platform)){
			\App::abort(404);
		}else {
			$platform = Models\Game\Platform::findBySlugOrFail($platform);

			$this->theme->set('platform', $platform);
			if($platform->games()->count() < 6) {
				$bigGames = new Collection;
				$games = $platform->games()->SortPagination();
			} else {
				$bigGames = $platform->games()->orderBy('rate', 'DESC')->take(2)->get();
				$games = $platform->games()->whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->SortPagination();
			}
			if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
				$this->theme->set('middle_class', 'categoryPage');

				$obj = Models\Text::findBySlugOrFail('games');

				$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));
				$this->theme->breadcrumb()->add($platform->fullTitle, $platform->url());

				$this->seoByModel($platform);

				JavaScript::put([
					'pageType' => 'game-platform'
				]);

				$out = $this->theme->layout('text')->scope('pages.game-list', [
					'platform' => $platform,
					'games' => $games,
					'content' => $platform->content,
					'bigGames' => $bigGames
				])->render();
			}
		}
		return $out;
	}

	public function getChar($char = null){
		$out = null;
		if(empty($char)){
			\App::abort(404);
		}else {
			if($char == '0-9'){
				$games = Models\Game::whereIn('char', range(0, 9));
			}else{
				$games = Models\Game::where('char', $char);
			}
			$games = $games->SortPagination();
			if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
				$this->theme->set('middle_class','categoryPage');
				$this->theme->set('partial','game-rightSidebar');

				$obj = Models\Text::findBySlugOrFail('games');

				$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));
				$this->theme->breadcrumb()->add('Игры на букву "'.Str::upper($char).'"');

				$obj->fullTitle = 'Игры на букву "'.Str::upper($char).'"';
				$obj->seo->title = 'Игры на букву "'.Str::upper($char).'"';

				$this->seoByModel($obj);

				JavaScript::put([
					'pageType' => 'game-char'
				]);

				$out = $this->theme->layout('text')->scope('pages.game-char', [
					'games' => $games,
					'content' => ''
				])->render();
			}
		}
		return $out;
	}

	/**
	 * @return mixed
	 * set('middle_class') - класс для блока контента
	 * set('partial') - шаблон сайдбара | если не указано принимает значение cart-rigtSidebar
	 */
	public function getRecommendations(){
		/*** тут должна быть сортировка по рейтингу без возможности сменить направление сортировки */
		Sorter::set('sort', 'rate'); /* UPDATE */
		$this->theme->set('pager_sort_disable', true);
		$bigGames = Models\Game::orderBy('rate', 'DESC')->take(2)->get();
		$games = Models\Game::whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->orderBy('rate', 'DESC')->SortPagination();

		if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
			$this->theme->set('middle_class','categoryPage');
			$this->theme->set('partial','game-rightSidebar');

			$obj = Models\Text::findBySlugOrFail('games');
			$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));

			$obj = Models\Text::findBySlugOrFail('recommendations');
			$this->theme->breadcrumb()->add($obj->title, $obj->url());

			$this->seoByModel($obj);

			JavaScript::put([
				'pageType' => 'game-recommendations'
			]);

			$out = $this->theme->layout('text')->scope('pages.game-list', [
				'games' => $games,
				'page' => $obj,
				'content' => $obj->content,
				'bigGames' => $bigGames
			])->render();
		}

		return $out;
	}

	public function getSale(){
		if(Models\Game::where('fullprice', '>', 0)->count() < 6) {
			$bigGames = new Collection;
			$games = Models\Game::where('fullprice', '>', 0)->SortPagination();
		} else {
			$bigGames = Models\Game::where('fullprice', '>', 0)->orderBy('rate', 'DESC')->take(2)->get();
			$games = Models\Game::where('fullprice', '>', 0)->whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->SortPagination();
		}

		if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
			$this->theme->set('middle_class','categoryPage');
			$this->theme->set('partial','game-rightSidebar');

			$obj = Models\Text::findBySlugOrFail('games');
			$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));

			$obj = Models\Text::findBySlugOrFail('sale');
			$this->theme->breadcrumb()->add($obj->title, $obj->url());

			$this->seoByModel($obj);

			JavaScript::put([
				'pageType' => 'game-sale'
			]);

			$out = $this->theme->layout('text')->scope('pages.game-list', [
				'games' => $games,
				'page' => $obj,
				'content' => $obj->content,
				'bigGames' => $bigGames
			])->render();
		}

		return $out;
	}

	public function getNew(){
		/*** тут должна быть сортировка по дате добавления без возможности сменить направление сортировки */
		Sorter::set('sort', 'release');
		$this->theme->set('pager_sort_disable', true);
		$bigGames = Models\Game::orderBy('release', 'DESC')->take(2)->get();
		$games = Models\Game::whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->SortPagination();

		if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
			$this->theme->set('middle_class','categoryPage');
			$this->theme->set('partial','game-rightSidebar');

			$obj = Models\Text::findBySlugOrFail('games');
			$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));

			$obj = Models\Text::findBySlugOrFail('new');
			$this->theme->breadcrumb()->add($obj->title, $obj->url());

			$this->seoByModel($obj);

			JavaScript::put([
				'pageType' => 'game-new'
			]);

			$out = $this->theme->layout('text')->scope('pages.game-list', [
				'games' => $games,
				'page' => $obj,
				'content' => $obj->content,
				'bigGames' => $bigGames
			])->render();
		}

		return $out;
	}

	public function getBestSeller(){
		/*** тут должна быть сортировка по продажам без возможности сменить направление сортировки */
		if(Models\Game::count() < 6) {
			$bigGames = new Collection;
			$games = Models\Game::SortPagination();
		} else {
			$bigGames = Models\Game::orderBy('rate', 'DESC')->take(2)->get();
			$games = Models\Game::whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->SortPagination();
		}

		if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
			$this->theme->set('middle_class','categoryPage');
			$this->theme->set('partial','game-rightSidebar');

			$obj = Models\Text::findBySlugOrFail('games');
			$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));

			$obj = Models\Text::findBySlugOrFail('best-seller');
			$this->theme->breadcrumb()->add($obj->title, $obj->url());

			$this->seoByModel($obj);

			JavaScript::put([
				'pageType' => 'game-best-seller'
			]);

			$out = $this->theme->layout('text')->scope('pages.game-list', [
				'games' => $games,
				'page' => $obj,
				'content' => $obj->content,
				'bigGames' => $bigGames
			])->render();
		}

		return $out;
	}
	public function getPreOrder(){
		$status = Models\Status::findBySlugOrFail('prestok');

		if($status->games()->count() < 6) {
			$bigGames = new Collection;
			$games = $status->games()->SortPagination();
		} else {
			$bigGames = $status->games()->orderBy('rate', 'DESC')->take(2)->get();
			$games =$status->games()->whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->SortPagination();
		}

		if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
			$this->theme->set('middle_class','categoryPage');
			$this->theme->set('partial','game-rightSidebar');

			$obj = Models\Text::findBySlugOrFail('games');
			$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));

			$obj = Models\Text::findBySlugOrFail('pre-order');
			$this->theme->breadcrumb()->add($obj->title, $obj->url());

			$this->seoByModel($obj);

			JavaScript::put([
				'pageType' => 'game-pre-order'
			]);

			$out = $this->theme->layout('text')->scope('pages.game-list', [
				'games' => $games,
				'page' => $obj,
				'content' => $obj->content,
				'bigGames' => $bigGames
			])->render();
		}

		return $out;
	}

	public function getDevelopers($developer){
		$out = null;
		if(empty($developer)){
			\App::abort(404);
		}else {
			$developer = Models\Game\Developer::findBySlugOrFail($developer);

			$this->theme->set('developer', $developer);

			if($developer->games()->count() < 6) {
				$bigGames = new Collection;
				$games = $developer->games()->SortPagination();
			} else {
				$bigGames = $developer->games()->orderBy('rate', 'DESC')->take(2)->get();
				$games =$developer->games()->whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->SortPagination();
			}

			if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
				$this->theme->set('middle_class', 'categoryPage');

				$obj = Models\Text::findBySlugOrFail('games');

				$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));
				$this->theme->breadcrumb()->add($developer->fullTitle, $developer->url());

				$this->seoByModel($developer);

				JavaScript::put([
					'pageType' => 'game-developer'
				]);

				$out = $this->theme->layout('text')->scope('pages.game-list', [
					'developer' => $developer,
					'games' => $games,
					'content' => $developer->content,
					'bigGames' => $bigGames
				])->render();
			}
		}
		return $out;
	}

	public function getVendors($vendor){
		$out = null;
		if(empty($vendor)){
			\App::abort(404);
		}else {
			$vendor = Models\Game\Vendor::findBySlugOrFail($vendor);

			$this->theme->set('vendor', $vendor);

			if($vendor->games()->count() < 6) {
				$bigGames = new Collection;
				$games = $vendor->games()->orderBy('rate', 'DESC')->SortPagination();
			} else {
				$bigGames = $vendor->games()->orderBy('rate', 'DESC')->take(2)->get();
				$games = $vendor->games()->whereNotIn($this->obj->getTable() . '.id', $bigGames->lists('id')->toArray())->SortPagination();
			}

			if(! $games instanceof LengthAwarePaginator || ($games instanceof LengthAwarePaginator && ($out = $games->checkPaginate()) === true)) {
				$this->theme->set('middle_class', 'categoryPage');

				$obj = Models\Text::findBySlugOrFail('games');

				$this->theme->breadcrumb()->add($obj->title, $this->obj->url('lists'));
				$this->theme->breadcrumb()->add($vendor->fullTitle, $vendor->url());

				$this->seoByModel($vendor);

				JavaScript::put([
					'pageType' => 'game-vendor'
				]);

				$out = $this->theme->layout('text')->scope('pages.game-list', [
					'vendor' => $vendor,
					'games' => $games,
					'content' => $vendor->content,
					'bigGames' => $bigGames
				])->render();
			}
		}
		return $out;
	}
}
