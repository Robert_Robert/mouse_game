<?php namespace App\Http\Middleware\Admin;

use Closure;
use Bican\Roles\Exceptions\PermissionDeniedException;

class Permission {

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param  \Closure  $next
	 * @param int|string $permission
	 * @return mixed
	 * @throws \Bican\Roles\Exceptions\PermissionDeniedException
	 */
	public function handle($request, Closure $next, $permission)
	{
		if( \AdminAuth::check() && \AdminAuth::user()->can($permission)) {
			return $next($request);
		}
		throw new PermissionDeniedException($permission);
	}

}