<?php namespace App\Http\Middleware\Admin;

use Closure;

class Role {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (\AdminAuth::guest())
		{
			if (\Request::ajax())
			{
				return response('Unauthorized', 401);
			} else
			{
				return redirect()->guest(route('admin.login'));
			}
		}else{
			if( !(\AdminAuth::user()->isSudo())) {
				return \View::make(\AdminTemplate::view('pages.no-sudo'), [
					'title' => config('admin.title')
				]);
			}
		}
		return $next($request);
	}

}