<?php

namespace App\Http\Middleware;

use Closure;
use App\Models as Model;
use Illuminate\Contracts\Auth\Guard;
use URL;

class ReferralsMiddleware
{
	protected $auth = null;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$response = $next($request);
		if($request->has('code')){
			$user = Model\User::findOrFail((string)$request->get('code'));

			$response = redirect($request->url());

			if ($this->auth->guest()) { //Только переходы гостей сохраняю
				$url = parse_url($request->getRequestUri());
				parse_str(get_key($url, 'query', '', 'is_scalar'), $url['query']);
				if (isset($url['query']['code'])) {
					unset($url['query']['code']);
				}
				$url['query'] = http_build_query($url['query']);
				if (!empty($url['query'])) {
					$url = get_key($url, 'path', '', 'is_scalar') . '?' . $url['query'];
				} else {
					$url = get_key($url, 'path', '', 'is_scalar');
				}
				$source = Model\ReferralSource::create(array(
					'source' => $request->headers->get('referer'),
					'landing' => $url,
					'ip' => $request->getClientIp(),
					'referrer_id' => $user->getKey()
				));
				session()->put('referral_source', $source->getKey());
			}
			$response = $response->withCookie(cookie()->forever('referral', $user->getKey()));
		}

        return $response;
    }
}
