<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Validation\CustomValidation;
use Validator;

class ValidationServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Validator::resolver(function($translator, $data, $rules, $messages){
			return new CustomValidation($translator, $data, $rules, $messages);
		});
	}
	public function register()
	{

	}

}
