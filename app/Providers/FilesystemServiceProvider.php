<?php namespace App\Providers;

use Storage;
use League\Flysystem\Filesystem;
use Dropbox\Client as DropboxClient;
use League\Flysystem\Dropbox\DropboxAdapter;
use League\Flysystem\Sftp\SftpAdapter;
use Illuminate\Support\ServiceProvider;

class FilesystemServiceProvider extends ServiceProvider {

	public function boot()
	{
		Storage::extend('dropbox', function($app, $config){
			$client = new DropboxClient($config['accessToken'], $config['clientIdentifier']);
			return new Filesystem(new DropboxAdapter($client));
		});

		Storage::extend('sftp', function ($app, $config) {
			return new Filesystem(new SftpAdapter([
				'host' => $config['host'],
				'port' => $config['port'],
				'username' => $config['username'],
				'password' => $config['password'],
				'privateKey' => $config['privateKey'],
				'root' => $config['root'],
				'timeout' => $config['timeout'],
			]));
		});
	}

	public function register()
	{
		//
	}

}