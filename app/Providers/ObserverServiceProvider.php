<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models as Models;
use App\Observers as Observers;

class ObserverServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Models\Status::observe( new Observers\StatusObserver );

		Models\Game\Mode::observe( new Observers\Game\ModeObserver );
		Models\Game\Lang\Text::observe( new Observers\Game\Lang\TextObserver );

		Models\Game\Developer::observe( new Observers\Game\DeveloperObserver );
		Models\Game\Janr::observe( new Observers\Game\JanrObserver );
		Models\Game\Platform::observe( new Observers\Game\PlatformObserver );
		Models\Game\Region::observe( new Observers\Game\RegionObserver );
		Models\Game\Vendor::observe( new Observers\Game\VendorObserver );

		Models\Game::observe( new Observers\GameObserver );
		Models\Keyword::observe( new Observers\KeywordObserver );
		Models\News::observe( new Observers\NewsObserver );
		Models\Permission::observe( new Observers\PermissionObserver );
		Models\Role::observe( new Observers\RoleObserver );
		Models\Seo::observe( new Observers\SeoObserver );
		Models\Text::observe( new Observers\TextObserver );
		Models\User::observe( new Observers\UserObserver );
		Models\Slider::observe( new Observers\SliderObserver );

		Models\ReferralSource::observe( new Observers\ReferralSourceObserver );
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
	}

}