<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldForSeo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('seo', function(Blueprint $table)
		{
			$table->string('h1')->nullable();
			$table->string('frequency')->nullable()->change();;
			$table->string('robots')->nullable()->change();;
			$table->string('state')->nullable()->change();;
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('seo', function(Blueprint $table) {
			$table->dropColumn('h1');
		});
    }
}
