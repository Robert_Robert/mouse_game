<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGames extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('games', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable();
			$table->mediumText('description')->nullable();
			$table->string('image');
			$table->integer('rate');
			$table->text('content');
			$table->integer('price');
			$table->integer('fullprice');
			$table->integer('vendor_id')->unsigned()->index();
			$table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('games');
	}

}
