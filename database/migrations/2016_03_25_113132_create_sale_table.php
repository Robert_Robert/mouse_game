<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		/**
		 * - Несколько по одной цене: bounty_price
				Позиции: А, Б, С
				Общая стоимость: ХХХХ
		 */
		Schema::create('bounty_price', function (Blueprint $table) {
			$table->increments('id');

			$table->date('active_to')->nullable()->index();
			$table->date('active_from')->nullable()->index();

			$table->integer('price');
			$table->timestamps();
		});

		Schema::create('bounty_price_condition', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('bounty_price_id')->unsigned()->index();
			$table->foreign('bounty_price_id')->references('id')->on('bounty_price')->onDelete('cascade');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
			$table->timestamps();
		});

		/**
		 - Если определенный товар в корзине, то на другие определенные скидка bounty_discount
				Позиция: А
				Скидка на позицию: Б
				Размер скидки: ХХХ

				Позиция: А, Б
				Скидка на позицию С
				Размер скидки: ХХХ
		 */
		Schema::create('bounty_discount', function (Blueprint $table) {
			$table->increments('id');

			$table->date('active_to')->nullable()->index();
			$table->date('active_from')->nullable()->index();

			$table->integer('discount');
			$table->timestamps();
		});

		Schema::create('bounty_discount_game', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('bounty_discount_id')->unsigned()->index();
			$table->foreign('bounty_discount_id')->references('id')->on('bounty_discount')->onDelete('cascade');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
			$table->timestamps();
		});

		Schema::create('bounty_discount_condition', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('bounty_discount_id')->unsigned()->index();
			$table->foreign('bounty_discount_id')->references('id')->on('bounty_discount')->onDelete('cascade');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
			$table->timestamps();
		});

		/**
		 * - Если один  продается, то другой может идти бонусом bounty_free
				Позиция: А
				Бонус: Б

				Позиция А, Б
				Бонус: С

				Позиция: А
				Бонус: Б, С
		 */
		Schema::create('bounty_free', function (Blueprint $table) {
			$table->increments('id');

			$table->date('active_to')->nullable()->index();
			$table->date('active_from')->nullable()->index();
			$table->timestamps();
		});

		Schema::create('bounty_free_condition', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('bounty_free_id')->unsigned()->index();
			$table->foreign('bounty_free_id')->references('id')->on('bounty_free')->onDelete('cascade');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
			$table->timestamps();
		});

		Schema::create('bounty_free_game', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('bounty_free_id')->unsigned()->index();
			$table->foreign('bounty_free_id')->references('id')->on('bounty_free')->onDelete('cascade');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		\DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		Schema::drop('bounty_price');
		Schema::drop('bounty_price_condition');

		Schema::drop('bounty_discount');
		Schema::drop('bounty_discount_game');
		Schema::drop('bounty_discount_condition');

		Schema::drop('bounty_free');
		Schema::drop('bounty_free_condition');
		Schema::drop('bounty_free_game');

		\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
