<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldToGameModel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('games', function(Blueprint $table)
		{
			$table->text('requirement')->nullable();
			$table->text('install')->nullable();
			$table->binary('have');
			$table->date('release');
			$table->string('video')->nullable();

			$table->integer('year')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('games', function(Blueprint $table)
		{
			$table->dropColumn('requirement');
			$table->dropColumn('install');
			$table->dropColumn('have');
			$table->dropColumn('releaset');
			$table->dropColumn('video');
			$table->dropColumn('year');
		});
	}

}
