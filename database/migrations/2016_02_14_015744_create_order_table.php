<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		\DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		Schema::create('orders', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
			$table->integer('price');
			$table->boolean('pay')->default(0)->index();
			$table->timestamp('payed_at')->nullable();
			$table->string('payed_method')->nullable()->index();
			$table->timestamps();
		});
		\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		\DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		Schema::drop('orders');

		\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
