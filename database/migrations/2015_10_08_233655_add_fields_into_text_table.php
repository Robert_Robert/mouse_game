<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsIntoTextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('texts', function(Blueprint $table){
			$table->text('content');
			$table->string('name');
			$table->string('slug')->unique();
			$table->boolean('system')->default(0)->index();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('texts', function(Blueprint $table){
			$table->dropColumn('content');
			$table->dropColumn('name');
			$table->dropColumn('slug');
			$table->dropColumn('system');
		});
    }
}
