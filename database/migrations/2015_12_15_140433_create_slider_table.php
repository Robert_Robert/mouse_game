<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('sliders', function (Blueprint $table) {
			$table->increments('id');
			$table->string('left')->nullable();
			$table->string('right')->nullable();
			$table->string('image');
			$table->string('link')->nullable();
			$table->integer('sort')->default('0');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('sliders');
    }
}
