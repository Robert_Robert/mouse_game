<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		\DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		Schema::create('order_items', function (Blueprint $table) {
			$table->increments('id');

			$table->string('title')->nullable();
			$table->integer('price');

			$table->integer('quantity');

			$table->integer('order_id')->unsigned()->index();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');

			$table->longtext('keys')->nullable();
			$table->boolean('sent')->default(0);

			$table->longtext('data')->nullable();

			$table->timestamps();
		});

		\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		\DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		Schema::drop('order_items');

		\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
