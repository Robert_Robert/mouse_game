<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('referral_sources', function (Blueprint $table) {
			$table->increments('id');
			$table->string('source')->nullable();
			$table->string('landing')->nullable();
			$table->string('ip')->nullable()->index();

			$table->integer('referrer_id')->unsigned()->index()->nullable();
			$table->foreign('referrer_id')->references('id')->on('users')->onDelete('cascade');

			$table->integer('user_id')->unsigned()->index()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

			$table->timestamps();
		});

		Schema::table('users', function (Blueprint $table) {
			$table->integer('referrer_id')->unsigned()->index()->nullable();

			$table->foreign('referrer_id')->references('id')->on('users')->onDelete('set null');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('referral_sources');

		\DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		Schema::table('users', function(Blueprint $table) {

			$table->dropForeign('users_referrer_id_foreign');
			$table->dropColumn('referrer_id');
		});

		\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
