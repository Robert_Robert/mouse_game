<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewRateFieldForGameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('games', function(Blueprint $table){
			$table->integer('metacritic');
			$table->index('rate');

			$table->dropColumn('have');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

		Schema::table('games', function(Blueprint $table){
			$table->dropColumn('metacritic');
			$table->dropIndex('rate');
			$table->binary('have');
		});
    }
}
