<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortFieldIntoGameParameterFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('mode', function(Blueprint $table)
		{
			$table->integer('sort')->default('0');
		});

		Schema::table('regions', function(Blueprint $table)
		{
			$table->integer('sort')->default('0');
		});

		Schema::table('game_text_lang', function(Blueprint $table)
		{
			$table->integer('sort')->default('0');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mode', function(Blueprint $table)
		{
			$table->dropColumn('sort');
		});

		Schema::table('regions', function(Blueprint $table)
		{
			$table->dropColumn('sort');
		});

		Schema::table('game_text_lang', function(Blueprint $table)
		{
			$table->dropColumn('sort');
		});
	}
}
