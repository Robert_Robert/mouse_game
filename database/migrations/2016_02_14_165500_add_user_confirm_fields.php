<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserConfirmFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('users', function(Blueprint $table)
		{
			$table->boolean('confirmed')->default(0);
			$table->string('confirmation_code', 30)->nullable();
			$table->string('reg_ip')->nullable();
			$table->string('reg_referer_page')->nullable();

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('confirmed');
			$table->dropColumn('confirmation_code');
			$table->dropColumn('reg_ip');
			$table->dropColumn('reg_referer_page');
		});
    }
}
