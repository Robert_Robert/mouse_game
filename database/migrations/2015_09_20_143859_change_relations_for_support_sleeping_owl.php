<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRelationsForSupportSleepingOwl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('games', function(Blueprint $table)
		{
			$table->dropForeign('games_vendor_id_foreign');
			$table->dropColumn('vendor_id');

			$table->dropForeign('games_developer_id_foreign');
			$table->dropColumn('developer_id');
		});

		Schema::create('game_vendors', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');

			$table->integer('vendor_id')->unsigned()->index();
			$table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
			$table->timestamps();
		});

		Schema::create('game_developers', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');

			$table->integer('developer_id')->unsigned()->index();
			$table->foreign('developer_id')->references('id')->on('developers')->onDelete('cascade');
			$table->timestamps();
		});

		Schema::table('keywords', function(Blueprint $table)
		{
			$table->dropColumn('seo_id');
		});

		Schema::create('keyword_seo', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('seo_id')->unsigned()->index();
			$table->foreign('seo_id')->references('id')->on('seo')->onDelete('cascade');
			$table->integer('keyword_id')->unsigned()->index();
			$table->foreign('keyword_id')->references('id')->on('keywords')->onDelete('cascade');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('games', function(Blueprint $table)
		{
			$table->integer('vendor_id')->unsigned()->index();
			$table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
		});
		Schema::drop('game_vendors');

		Schema::drop('game_developers');

		Schema::table('keywords', function(Blueprint $table)
		{
			$table->integer('seo_id');
		});

		Schema::drop('keyword_seo');
    }
}
