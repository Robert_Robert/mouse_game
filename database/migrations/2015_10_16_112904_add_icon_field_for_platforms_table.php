<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconFieldForPlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('platforms', function(Blueprint $table)
		{
			$table->string('icon')->nullable();
			$table->integer('sort')->default('0');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('platforms', function(Blueprint $table)
		{
			$table->dropColumn('icon');
			$table->dropColumn('sort');
		});
	}
}
