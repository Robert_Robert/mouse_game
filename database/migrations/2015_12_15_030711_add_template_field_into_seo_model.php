<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateFieldIntoSeoModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('seo', function(Blueprint $table){
			$table->string('background')->index();
			$table->string('background_url')->index();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('seo', function(Blueprint $table){
			$table->dropColumn('background');
			$table->dropColumn('background_url');
		});
    }
}
