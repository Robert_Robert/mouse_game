<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameGameLangsayToMode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::drop('game_say');
		Schema::drop('game_say_lang');

		Schema::create('mode', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('slug')->unique();
			$table->timestamps();
		});
		Schema::create('game_mode', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('mode_id')->unsigned()->index();
			$table->foreign('mode_id')->references('id')->on('mode')->onDelete('cascade');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('game_mode');
		Schema::drop('mode');
		Schema::create('game_say', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('say_id')->unsigned()->index();
			$table->foreign('say_id')->references('id')->on('game_say_lang')->onDelete('cascade');
			$table->integer('game_id')->unsigned()->index();
			$table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
			$table->timestamps();
		});
		Schema::create('game_say_lang', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('slug')->unique();
			$table->timestamps();
		});
    }
}
