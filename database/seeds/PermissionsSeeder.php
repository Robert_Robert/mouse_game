<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		\App\Models\Permission::create([
			'name' => 'Запретить изменение цены товара',
			'slug' => 'game.price'
		]);

		\App\Models\Permission::create([
			'name' => 'Запретить работу со скидками',
			'slug' => 'sale'
		]);

		\App\Models\Permission::create([
			'name' => 'Запретить просмотр истории продаж',
			'slug' => 'order.history'
		]);
    }
}
