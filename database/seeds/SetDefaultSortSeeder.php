<?php

use Illuminate\Database\Seeder;

use App\Models as Models;

class SetDefaultSortSeeder extends Seeder
{
	private $time = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->time = \Carbon\Carbon::now();

		$this->command->info('Start reSort App\Models\Game\Mode...');
        foreach(Models\Game\Mode::orderBy('name')->get() as $sort => $item){
			$item->sort = $sort;
			$item->save();
		}
		$this->command->info("Done");

		$this->command->info('Start reSort App\Models\Game\Mode...');
		foreach(Models\Game\Region::orderBy('name')->get() as $sort => $item){
			$item->sort = $sort;
			$item->save();
		}
		$this->command->info("Done");

		$this->command->info('Start reSort App\Models\Game\Mode...');
		foreach(Models\Game\Lang\Text::orderBy('name')->get() as $sort => $item){
			$item->sort = $sort;
			$item->save();
		}
		$this->command->info("Done");

		$this->command->info("Total time: ".$this->time->diffForHumans(\Carbon\Carbon::now()));
    }
}
