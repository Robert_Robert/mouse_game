<?php

use Illuminate\Database\Seeder;
use App\Models\Text;

class DefaultTextPages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$text = Text::firstOrCreate([
			'name' => 'Главная',
			'slug' => 'index',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'О магазине',
			'slug' => 'about',
			'system' => false
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Поддержка',
			'slug' => 'support',
			'system' => false
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'FAQ',
			'slug' => 'faq',
			'system' => false
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Все игры',
			'slug' => 'games',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Предзаказ',
			'slug' => 'pre-order',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Рекомендации',
			'slug' => 'recommendations',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Скидки',
			'slug' => 'sale',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Новинки',
			'slug' => 'new',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Лидеры продаж',
			'slug' => 'best-seller',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Вход',
			'slug' => 'login',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Регистрация',
			'slug' => 'register',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Корзина',
			'slug' => 'cart',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Результаты поиска',
			'slug' => 'search',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Подписка на новости',
			'slug' => 'subscribe',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Новости',
			'slug' => 'news',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$this->command->info("Done");
    }
}
