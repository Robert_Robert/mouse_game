<?php

use Illuminate\Database\Seeder;
use App\Models\Lister;

class ListerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Lister::firstOrCreate([
			'name' => 'Игры недели',
			'slug' => 'igry-nedeli'
		]);

		Lister::firstOrCreate([
			'name' => 'Предзаказ',
			'slug' => 'preorder'
		]);

		Lister::firstOrCreate([
			'name' => 'Акции',
			'slug' => 'sale'
		]);

		$this->command->info("Done");
    }
}
