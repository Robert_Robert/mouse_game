<?php
use Illuminate\Database\Seeder;
use App\Models as Models;

class ReSaveSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->reSave(Models\Game\Mode::all())
			->reSave(Models\Game\Lang\Text::all())
			->reSave(Models\Game\Developer::all())
			->reSave(Models\Game\Janr::all())
			->reSave(Models\Game\Platform::all())
			->reSave(Models\Game\Region::all())
			->reSave(Models\Game\Vendor::all())
			->reSave(Models\OrderItem::all())
			->reSave(Models\Game\Year::all())
			->reSave(Models\Game::all())
			->reSave(Models\Keyword::all())
			->reSave(Models\News::all())
			->reSave(Models\Permission::all())
			->reSave(Models\Role::all())
			->reSave(Models\Text::all())
			->reSave(Models\User::all())
			->reSave(Models\Lister::all())
			->reSave(Models\Order::all())
			->reSave(Models\Slider::all())
			->reSave(Models\Status::all());
	}

	protected function reSave($model){
		$this->command->info("Start save ".get_class($model->first()).'...');
		foreach($model as $item){
			$item->save();
		}
		return $this;
	}
}