<?php

use Illuminate\Database\Seeder;
use App\Models as Models;
use Illuminate\Database\Eloquent\Collection;

class TestGameSeeder extends Seeder
{
	protected $imageCat = array(
		'abstract', 'animals', 'business', 'cats', 'city', 'food', 'nightlife',
		'fashion', 'people', 'nature', 'sports', 'technics', 'transport'
	);
	protected $pathImg = null;
	private $time = null;
	private $faker = null;
	private $db = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->time = \Carbon\Carbon::now();
		$this->faker = Faker\Factory::create('ru_Custom_RU');
		$this->pathImg = public_path().'/images/faker';

		if( ! File::isDirectory($this->pathImg)) {
			File::makeDirectory($this->pathImg);
		}

		$this->db = new Collection([
			'vendors' => Models\Game\Vendor::all()->lists('id', 'name')->all(),
			'developers' => Models\Game\Developer::all()->lists('id', 'name')->all(),
			'janrs' => Models\Game\Janr::all()->lists('id', 'name')->all(),
			'mode' => Models\Game\Mode::all()->lists('id', 'name')->all(),
			'langtext' => Models\Game\Lang\Text::all()->lists('id', 'name')->all(),
			'platforms' => Models\Game\Platform::all()->lists('id', 'name')->all(),
			'regions' => Models\Game\Region::all()->lists('id', 'name')->all()
		]);
		$this->games(1000);

		$this->command->info("Total time for generate faker elements: ".$this->time->diffForHumans(\Carbon\Carbon::now()));
    }

	protected function getImage($w, $h){
		$img = $this->faker->image($this->pathImg, $w, $h, $this->imageCat[array_rand($this->imageCat)]);
		return str_replace(array(public_path() , '\\'), array('', '/'), $img);
	}

	protected function games($count){
		$this->command->info("Create ".$count." faker games...");
		$gameObj = new Models\Game;
		for($i=0; $i<$count; $i++){
			$game = $gameObj->create([
				'title' => $this->faker->word,
				'image' => $this->getImage(rand(0,1920), rand(0,1920)),
				'description' => $this->faker->realText(rand(100,1000)),
				'content' => $this->faker->realText(rand(100,3000)),
				'requirement' => $this->faker->realText(rand(100,3000)),
				'install' => $this->faker->realText(rand(100,3000)),
				'price' => $this->faker->numberBetween(3000, 9000),
				'fullprice' => $this->faker->optional(15)->numberBetween(100, 2900),
				'year' => $this->faker->randomElement($gameObj->yearNames),
				'rate' => $this->faker->numberBetween(0, 100),
				'release' => $this->faker->date()
			]);
			$game->attachVendors($this->faker->randomElement($this->db->get('vendors')));
			$game->attachDevelopers($this->faker->randomElement($this->db->get('developers')));

			$tmp = $this->db->get('janrs');
			$tmp = $this->faker->randomElements($tmp, $this->faker->numberBetween(1, count($tmp)));
			foreach($tmp as $item) {
				$game->attachJanrs($item);
			}
			$tmp = $this->db->get('mode');
			$tmp = $this->faker->randomElements($tmp, $this->faker->numberBetween(1, count($tmp)));
			foreach($tmp as $item) {
				$game->attachMode($item);
			}
			$tmp = $this->db->get('langtext');
			$tmp = $this->faker->randomElements($tmp, $this->faker->numberBetween(1, count($tmp)));
			foreach($tmp as $item) {
				$game->attachLangtext($item);
			}
			$tmp = $this->db->get('platforms');
			$tmp = $this->faker->randomElements($tmp, $this->faker->numberBetween(1, count($tmp)));
			foreach($tmp as $item) {
				$game->attachPlatforms($item);
			}
			$tmp = $this->db->get('regions');
			$tmp = $this->faker->randomElements($tmp, $this->faker->numberBetween(1, count($tmp)));
			foreach($tmp as $item) {
				$game->attachRegions($item);
			}

			if($i%10 === 0 && $i>0){
				$this->command->info("\tProcessed ".$i." faker games");
			}
		}
		$this->command->info("Done. Create ".$i." faker games\n");
		return $this;
	}
}
