<?php

use Illuminate\Database\Seeder;
use App\Models\Game\Year;
use App\Models\Game;

class CreateYearsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$data = [
			1 => '18+',
			2 => '12+',
			3 => '0+',
			4 => '16+',
			5 => '6+',
		];
		foreach($data as $id => $name){
			$this->command->info('Create '.$name.' year...');

			$text = Year::firstOrCreate([
				'name' => $name
			]);
			$text->save();

			$games = Game::where('year', $id)->get();
			$this->command->info('Attaced '.$games->count().' games into yearID = '.$text->getKey().'...');
			foreach($games as $game){
				$game->attachYears([$text->getKey()]);
				$game->save();
			}
		}
    }
}
