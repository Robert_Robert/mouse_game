<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->command->info("1 Step: Create user role and permission...");
		$this->call('UserSeeder');

		$this->command->info("2 Step: Install default data...");
		$this->call('DefaultSeeder');

		$this->command->info("\nDone!");
	}

}
