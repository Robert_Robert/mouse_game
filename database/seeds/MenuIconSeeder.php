<?php

use Illuminate\Database\Seeder;
use App\Models\Game\Janr;
use App\Models\Game\Platform;

class MenuIconSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
		$this->setJanrs()
			->setPlatforms();
    }

	protected function setJanrs(){
		$this->command->info('Attach icon for janrs...');
		$data = [
			'Action' => 'i-damir-actions color-cyan-hover',
			'RPG' => 'i-damir-rpg color-cyan-hover',
			'Спортивные' => 'i-damir-sport color-cyan-hover',
			'Стратегии' => 'i-damir-rts color-cyan-hover',
			'Гонки' => 'i-damir-racing color-cyan-hover',
			'Симуляторы' => 'i-damir-sim color-cyan-hover',
			'Другие' => 'i-damir-others color-cyan-hover'
		];
		foreach($data as $name => $icon){
			$text = Janr::firstOrCreate([
				'name' => $name
			]);
			$text->icon = $icon;
			$text->save();
		}

		$this->command->info('Resort janr items...');
		$i = 0;
		foreach(Janr::all() as $item){
			$item->sort = $i++;
			$item->save();
		}

		return $this;
	}

	protected function setPlatforms(){
		$this->command->info('Attach icon for platforms...');
		$data = [
			'Steam' => 'i-damir-steam color-black-hover',
			'Origin' => 'i-damir-origin color-orange-hover',
			'Mac' => 'i-damir-mac color-purple-hover',
			'Xbox' => 'i-damir-xbox color-green-hover',
			'PS' => 'i-damir-ps color-blue-hover',
			'iTunes' => 'i-damir-itunes color-cyan-hover'
		];
		foreach($data as $name => $icon){
			$text = Platform::firstOrCreate([
				'name' => $name
			]);
			$text->icon = $icon;
			$text->save();
		}

		$this->command->info('Resort platform items...');
		$i = 0;
		foreach(Platform::all() as $item){
			$item->sort = $i++;
			$item->save();
		}
	}
}
