<?php

use Illuminate\Database\Seeder;

class RateToMetacriticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->command->info("Move rate to metacritic...");
		foreach(App\Models\Game::all() as $item){
			$item->metacritic = $item->rate;
			$item->rate = 0;
			$item->save();
		}

    }
}
