<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class DefaultStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->command->info("Create status...");

		$status = Status::firstOrCreate([
			'slug' => 'prestok'
		]);
		$status->buyer = true;
		$status->name = 'Предзаказ';

		$status->save();

		$status = Status::firstOrCreate([
			'slug' => 'instok'
		]);
		$status->buyer = true;
		$status->name = 'Есть в наличии';
		$status->save();

		$status = Status::firstOrCreate([
			'slug' => 'outstok'
		]);
		$status->buyer = false;
		$status->name = 'Ожидается';
		$status->save();

		$this->command->info("Resave games...");

		foreach(App\Models\Game::all() as $item){
			$item->status()->associate($status);
			$item->save();
		}
    }
}
