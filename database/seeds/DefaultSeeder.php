<?php
use Illuminate\Database\Seeder;

use \App\Models\Game\Janr;
use \App\Models\Game\Platform;
use \App\Models\Game\Lang\Text;
use \App\Models\Game\Mode;

class DefaultSeeder  extends Seeder {
	public function run()
	{
		$this->clear();

		$this->janrs()
			->platforms()
			->langText()
			->mode();
	}

	protected function janrs(){
		Janr::create([
			'name' => 'Action/Шутеры',
			'slug' => 'actions'
		]);

		Janr::create([
			'name' => 'RPG',
			'slug' => 'rpg'
		]);

		Janr::create([
			'name' => 'Спортивные',
			'slug' => 'sport',
		]);

		Janr::create([
			'name' => 'Стратегии',
			'slug' => 'rts',
		]);

		Janr::create([
			'name' => 'Гонки',
			'slug' => 'racing',
		]);

		Janr::create([
			'name' => 'Симуляторы',
			'slug' => 'sim',
		]);

		Janr::create([
			'name' => 'Другие',
			'slug' => 'others'
		]);

		return $this;
	}

	protected function platforms(){
		Platform::create([
			'name' => 'Steam',
			'slug' => 'steam'
		]);

		Platform::create([
			'name' => 'Origin',
			'slug' => 'origin'
		]);

		Platform::create([
			'name' => 'Mac игры',
			'slug' => 'mac'
		]);

		Platform::create([
			'name' => 'Xbox LIVE',
			'slug' => 'xbox',
		]);

		Platform::create([
			'name' => 'PS Network',
			'slug' => 'ps',
		]);

		Platform::create([
			'name' => 'iTunes',
			'slug' => 'itunes'
		]);
		return $this;
	}

	protected function langText(){
		Text::create([
			'name' => 'English',
			'slug' => 'eng'
		]);

		Text::create([
			'name' => 'Русский',
			'slug' => 'rus'
		]);

		Text::create([
			'name' => 'Украинский',
			'slug' => 'uk'
		]);
		return $this;
	}

	protected function mode(){
		Mode::create([
			'name' => 'English',
			'slug' => 'eng'
		]);

		Mode::create([
			'name' => 'Русский',
			'slug' => 'rus'
		]);

		Mode::create([
			'name' => 'Украинский',
			'slug' => 'uk'
		]);
		return $this;
	}

	/** Удаление из базы левых данных */
	protected function clear(){
		$this->command->info("Clear tables\n");

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Janr::truncate();
		Platform::truncate();
		Say::truncate();
		Text::truncate();


		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}
}