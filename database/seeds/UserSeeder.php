<?php
use Illuminate\Database\Seeder;

use \App\Models\Permission;
use \App\Models\Role;
use \App\Models\User;

class UserSeeder  extends Seeder {
	public function run()
	{
		$this->clear();

		$this->sudoUser()
			->otherUser();
	}

	protected function sudoUser(){
		$role = Role::firstOrNew(['name'=>'sudo', 'slug' => 'sudo'])->save();
		User::create([
			'name' => 'Agel_Nash',
			'email' => 'laravel@agel-nash.ru',
			'password' => '123123'
		])->attachRole($role);

		return $this;
	}

	protected function otherUser(){
		User::create([
			'name' => 'Guest',
			'email' => 'agel-nash@mail.ru',
			'password' => '123123'
		]);

		return $this;
	}
	/** Удаление из базы левых данных */
	protected function clear(){
		$this->command->info("Clear tables\n");

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		User::truncate();
		Role::truncate();
		Permission::truncate();

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}
}