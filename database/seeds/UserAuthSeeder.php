<?php

use Illuminate\Database\Seeder;
use App\Models\Text;
use App\Models\User;

class UserAuthSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->command->info("Create new pages...");

		$text = Text::firstOrCreate([
			'name' => 'Восстановление пароля',
			'slug' => 'forgot',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Сброс пароля',
			'slug' => 'password-reset',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Личный кабинет',
			'slug' => 'profile',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Партнёрская программа',
			'slug' => 'partnership',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Редактировать аккаунт',
			'slug' => 'profile-edit',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Мои покупки',
			'slug' => 'purchases',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Привлеченные пользователи',
			'slug' => 'partnership-users',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Описание партнерской программы',
			'slug' => 'partnership-descr',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$text = Text::firstOrCreate([
			'name' => 'Переходы',
			'slug' => 'partnership-ref',
			'system' => true
		]);
		$text->hide = true;
		$text->save();

		$this->command->info("Update users...");
		foreach(User::all() as $user){
			$user->confirmed = true;
			$user->reg_ip = '127.0.0.1';
			$user->save();
		}
		$this->command->info("Done");
	}
}
