<?php include_once('header.php') ?>

<div class="wrapper">

    <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="crumbs"><a href="/game_menu_el.php?page=Action/Шутеры">Личный кабинет</a></li>
                <li class="lastcrumb">Восстановление пароля</li>
            </ul>
    </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="forgot">
    <div class="pagetitle">
      <h1>Восстановление пароля</h1>
    </div>
    <form action="#">
    <div class="label">Введите e-mail, который Вы указывали при регистрации</div>
    <input type="text" />
    <div>
		<div class="lable captcha">Введите код с картинки</div>
		<input type="text" class="captcha" />
		<a class="capcha captcha" href="#"></a>
   	</div>
	<div class="clr"></div>
    <input type="submit" value="Отправить" />
    </form>
  </section>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
