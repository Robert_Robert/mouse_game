<?php include_once('header.php') ?>

<div class="wrapper">

    <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="crumbs"><a href="/game_menu_el.php?page=Action/Шутеры">Личный кабинет</a></li>
                <li class="lastcrumb">Партнёрская программа</li>
            </ul>
    </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="partnership">
    <div class="pagetitle">
      <h1>Партнерская программа</h1>
    </div>

    <div class="page_filter">
      <ul>
        <li class="active"><a href="/partnership.php">Отчет по продажам</a></li>
        <li><a href="/partnership-ref.php">Источники переходов</a></li>
        <li><a href="/partnership-descr.php">Описание программы</a></li>
      </ul>
    </div>
    <div class="calendar_filter">
    	<form action="#" method="post">
    	<label>Статистика за период: с</label><input type="text" name="from" value="12.01.2012" />
        <label>по</label><input type="text" name="from" value="16.08.2012" />
        <input type="submit" name="from" value="Выбрать"  />
        </form>
    </div>
    <div class="purchases">
    	<table>
        	<tr>
            	<th>Наименование</th>
                <th><span>Дата</span></th>
                <th>Зачислено</th>
            </tr>
            <tr>
            	<td class="title">SimCity - 1 шт.</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">20 руб.</td>
            </tr>
            <tr>
            	<td class="title">Tomb Raider - 1 шт.</td>
                <td class="date">07.08.2012, 09:07</td>
                <td class="num">45 руб.</td>
            </tr>
            <tr>
            	<td class="title">Omerta-City of Gangsters - 1 шт.</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">20 руб.</td>
            </tr>
        </table>
    </div>
    <div class="back">
    	<a href="/personal.php">Вернуться в личный кабинет</a>
    </div>
  </section>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
