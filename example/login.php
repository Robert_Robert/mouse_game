<?php include_once('header.php') ?>

<div class="wrapper">

        <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="lastcrumb">Вход</li>
            </ul>
        </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="login">
    <div class="pagetitle">
      <h1>Вход</h1>
    </div>
    <form action="/" method="get" >
    <div class="label">Введите Ваш e-mail</div>
    <input type="text" name="login" />
    <div class="label">Введите Ваш пароль</div>
    <input type="text" name="password" />
    <a href="/forgot.php" class="forgot">Забыли пароль?</a>
    <input type="submit" value="Вход" name="submit" class="login" />
    </form>
  </section>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
