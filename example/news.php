<?php include_once('header.php') ?>

<div class="wrapper">

        <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="lastcrumb">Новости</li>
            </ul>
        </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="news_page">
    <div class="pagetitle">
      <h1>Новости</h1>
    </div>
    <div class="newslist">
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n1.png" /></a></div>
        <div class="body">
          <div class="date">25.12.2014</div>
          <div class="title"><a href="/news_recent.php">Deluxe издания Assassin's<br />
            Creed IV и Watch Dogs.</a></div>
          <div class="clr"></div>
          <div class="intro">Мы рады Вам предложить Deluxe издания <a href="#">Assassin's Creed IV</a> и <a href="#">Watch Dogs</a> на отличных условиях.</div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n2.png" /></a></div>
        <div class="body">
          <div class="date">11.12.2014</div>
          <div class="title"><a href="/news_recent.php">Borderlands 2 Game of the<br />
            Year Edition - релиз состоялся! </a></div>
          <div class="clr"></div>
          <div class="intro">Новое издание, содержащее несколько дополнений, наборов и
            других интересных бонусов! </div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n3.png" /></a></div>
        <div class="body">
          <div class="date">20.10.2014</div>
          <div class="title"><a href="/news_recent.php">XCOM: Enemy Within -<br />
            старт предзаказов </a></div>
          <div class="clr"></div>
          <div class="intro">Новые навыки, улучшения, оружие, карты, миссии и многое
            другое!</div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n4.png" /></a></div>
        <div class="body">
          <div class="date">19.09.2014</div>
          <div class="title"><a href="/news_recent.php">Season Pass - теперь и для<br />
            Assassin’s Creed IV Black Flag</a></div>
          <div class="clr"></div>
          <div class="intro"> Эксклюзивный пакет "Корабль Кракена", новая одиночная 
            кампания и многое другое!</div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n5.png" /></a></div>
        <div class="body">
          <div class="date">07.07.2014</div>
          <div class="title"><a href="/news_recent.php">NBA 2K14 - уже в продаже </a></div>
          <div class="clr"></div>
          <div class="intro">Отличный баскетбольный симулятор!</div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n6.png" /></a></div>
        <div class="body">
          <div class="date">25.06.2014</div>
          <div class="title"><a href="/news_recent.php">Teleglitch: Die More Edition<br />
            - скидка 75%!
            !</a></div>
          <div class="clr"></div>
          <div class="intro"> Добро пожаловать в мир ужаса, паранойи и страха</div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n7.png" /></a></div>
        <div class="body">
          <div class="date">23.06.2014</div>
          <div class="title"><a href="/news_recent.php">Стартовал ранний доступ<br />
            в War of the Vikings </a></div>
          <div class="clr"></div>
          <div class="intro">Примите участие в яростных схватках на полях сражений эпохи
            викингов! </div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n8.png" /></a></div>
        <div class="body">
          <div class="date">01.05.2014</div>
          <div class="title"><a href="/news_recent.php">Супер скидки от Plug-in-Digital </a></div>
          <div class="clr"></div>
          <div class="intro">В магазине mousegame.ru объявляется грандиозная распродажа
            игр от Plug In Digital! Приобретайте игры со скидками!</div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n9.png" /></a></div>
        <div class="body">
          <div class="date">30.04.2014</div>
          <div class="title"><a href="/news_recent.php">Новое дополнение для <br />
            Battlefield 4 - Final Stand </a></div>
          <div class="clr"></div>
          <div class="intro">Спешим сообщить отличную новость для всех любителей 
            Battlefield 4 - завтра выйдет в свет новое дополнение - 
            Final Stand</div>
        </div>
        <div class="clr"></div>
      </div>
      <div class="item">
        <div class="prev"><a href="/news_recent.php"><img src="/images/n10.png" /></a></div>
        <div class="body">
          <div class="date">02.04.2014</div>
          <div class="title"><a href="/news_recent.php">Скидка на серию игр Mafia 2 - 75% </a></div>
          <div class="clr"></div>
          <div class="intro">02.04.2014 состоится распродажа игр серии Mafia 2 - скидки 75% 
            на все! Преобретай выгодно!</div>
        </div>
        <div class="clr"></div>
      </div>
        <div class="pager">
          <ul>
            <li class="first"><a href="#">‹‹</a></li>
            <li class="prev"><a href="#"></a></li>
            <li class="current page"><a href="#">1</a></li>
            <li class="page"><a href="#">2</a></li>
            <li class="page"><a href="#">3</a></li>
            <li class="next"><a href="#"></a></li>
            <li class="last"><a href="#">››</a></li>
          </ul>
        </div>
    </div>
  </section>
  <?php include_once('news_sidebar.php') ?>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
