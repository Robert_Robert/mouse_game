<?php include_once('header.php') ?>

<div class="wrapper">

    <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="crumbs"><a href="/game_menu_el.php?page=Action/Шутеры">Личный кабинет</a></li>
                <li class="lastcrumb">Партнерская программа</li>
            </ul>
    </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="partnership">
    <div class="pagetitle">
      <h1>Партнерская программа</h1>
    </div>

    <div class="page_filter">
      <ul>
        <li><a href="/partnership.php">Отчет по продажам</a></li>
        <li class="active"><a href="/partnership-ref.php">Источники переходов</a></li>
        <li><a href="/partnership-descr.php">Описание программы</a></li>
      </ul>
    </div>
    <div class="calendar_filter">
    	<form action="#" method="post">
    	<label>Статистика за период: с</label><input type="text" name="from" value="12.01.2012" />
        <label>по</label><input type="text" name="from" value="16.08.2012" />
        <input type="submit" name="from" value="Выбрать"  />
        </form>
    </div>
    <div class="purchases">
    	<table class="ref">
        	<tr>
            	<th><span>HTTP refferer</span></th>
                <th><span>Дата</span></th>
                <th><span>IP адрес</span></th>
            </tr>
            <tr>
            	<td class="title">http://www.all-weapons.ru/comments/1473805</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">37.146.134.28</td>
            </tr>
            <tr>
            	<td class="title">http://www.stars-bd.ru/forumdisplay.php?f=26</td>
                <td class="date">07.08.2012, 09:07</td>
                <td class="num">109.206.133.231</td>
            </tr>
            <tr>
            	<td class="title">http://www.raceforme.ru/ipb/index.php?s=bbe93a746b33bd5bshowtopic=2637</td>
                <td class="date">04.09.2012, 12:53</td>
                <td class="num">212.107.2.156 </td>
            </tr>
        </table>
    </div>
    <div class="back">
    	<a href="/personal.php">Вернуться в личный кабинет</a>
    </div>
  </section>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
