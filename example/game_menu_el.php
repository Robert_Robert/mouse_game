<?php include_once('header.php') ?>
<div class="wrapper">

        <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="lastcrumb"><?php echo $_GET['page'] ?></li>
            </ul>
        </div>

<?php include_once('left_sidebar.php') ?>
    <main id="middle" class="categoryPage">
    <div class="pagetitle"><h1><?php echo $_GET['page'] ?></h1></div>
    <div class="slider">
    	<div class="slide">
        	<img src="/images/gta.png" alt=""/>
            <div class="descr">
            	<div class="title">GTA V (PC)</div>
                <div class="price"><span class="num">499</span>руб.</div>
            </div>
        </div>
        <div class="slide">
        	<img src="/images/thelast.png" alt=""/>
            <div class="descr">
            	<div class="title">The last of us (PC)</div>
                <div class="price"><span class="num">499</span>руб.</div>
            </div>
        </div>
    	<div class="clr"></div>
    </div>
    <?php include_once('game_catalog.php') ?>  
    </main>
<div class="clr"></div>
</div>   
<?php include_once('footer.php') ?>