<?php include_once('header.php') ?>

<div class="wrapper">

    <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="crumbs"><a href="/game_menu_el.php?page=Action/Шутеры">Личный кабинет</a></li>
                <li class="lastcrumb">Пополнить счет</li>
            </ul>
    </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="balanse">
    <div class="pagetitle">
      <h1>Пополнить счет</h1>
    </div>

    <div class="mymoney">Ваш баланс равен <strong>152</strong> руб.</div>
    <div class="payments">
		<table>
        	<tr>
            	<th class="cont"></th>
                <th colspan="2" class="spos-opl">Способ оплаты</th>
                <th class="com">Комиссия</th>
            </tr>
            <tr>
            	<td class="logo"><div class="cell visa"><img src="/images/footer_VISA.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td class="cont">Оплата банковской картой Visa</td>
                <td class="com">0%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell masterCard"><img src="/images/footer_MASTERCARD.svg" alt=""/></div></td>
                <td class="input"><span class="checked"></span></td>
                <td>Оплата банковской картой MasterCard</td>
                <td class="com">0%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell webmoney"><img src="/images/footer_WEBMONEY.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>Платежная система Webmoney</td>
                <td class="com">0.8%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell kiwi"><img src="/images/footer_QIWI.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>Оплата через Киви Кошелек</td>
                <td class="com">0%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell mts"><img src="/images/footer_MTS.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>SMS для абонентов МТС</td>
                <td class="com">7.5%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell megafon"><img src="/images/footer_MEGAFON.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>SMS для абонентов МегаФон</td>
                <td class="com">11%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell yandex"><img src="/images/footer_YANDEX.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>Платежная система Яндекс.Деньги</td>
                <td class="com">0%</td>
            </tr>
        </table>
	</div>	
    <form action="#">
    <label>Введите сумму для пополнения баланса: <input type="text" name="summ" class="popolnenie"/> руб.</label>
    <input type="submit" value="Оплатить" class="oplata"/>
    </form>
	 <div class="back">
    	<a href="/personal.php">Вернуться в личный кабинет</a>
    </div>
  </section>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
