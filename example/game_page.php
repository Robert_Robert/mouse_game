<?php include_once('header.php') ?>

<div class="wrapper">

    <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="crumbs"><a href="/game_menu_el.php?page=Action/Шутеры">Action/Шутеры</a></li>
                <li class="lastcrumb">BioShock Infinite</li>
            </ul>
    </div>

  <?php include_once('left_sidebar.php') ?>
<div>
  <main id="middle" class="gamePage">
    <div class="pagetitle">
      <h1>BioShock Infinite</h1>
    </div>
   <figure>
    <div class="slider">
      <div class="mainimage"><img src="/images/videoslider.png" alt=""/></div>
      <div class="thumbs">
        <div class="thumb"><img src="/images/thumb1.jpg" alt=""/></div>
        <div class="thumb"><img src="/images/thumb2.jpg" alt=""/></div>
        <div class="thumb"><img src="/images/thumb3.jpg" alt=""/></div>
        <div class="thumb"><img src="/images/thumb4.jpg" alt=""/></div>
        <div class="thumb"><img src="/images/thumb5.jpg" alt=""/></div>
      </div>
    </div>
   </figure>
    <div class="page_filter">
      <ul id="game_page">
        <li class="active"><a href="#">Описание</a></li>
        <li><a href="#">Рецензии</a></li>
        <li><a href="#">Системные требования</a></li>
        <li><a href="#">Установка</a></li>
      </ul>
    </div>
    <div class="content">
      <p><img src="/images/reiting.png" alt="" /></p>
      <p>BioShock Infinite - продолжение экшена от первого лица с элементами РПГ, разработанное
        студией Irrational Games.</p>
      <p>Действие BioShock Infinite разворачиваются в начале XX века, за 50 лет до событий в Восторге.
        В 1900 году Америка создала воздушный город Колумбия, который стал чудом среди изобретений
        человека. Но благодаря загадочному инциденту произошло восстание, и город испарился на
        несколько лет.<a href="#content_resume" id="more_link">Далее...</a></p>
    </div>

    <aside>
    <div class="more addons">
      <div class="ttl">Дополнения к этой игре:</div>
      <div class="item"><a href="#"> <img src="/images/bio1.jpg" alt=""/>
        <div class="descr">
          <div class="title">
            <div class="cell">Bioshock infinite:<br />
              columbia's finest</div>
          </div>
          <div class="price"><strong>99</strong> руб.</div>
        </div>
        </a> </div>
      <div class="item"><a href="#"> <img src="/images/bio2.jpg" alt=""/>
        <div class="descr">
          <div class="title">
            <div class="cell">Bioshock infinite:<br />
              Season Pass </div>
          </div>
          <div class="price"><strong>499</strong> руб.</div>
        </div>
        </a> </div>
      <div class="item"><a href="#"> <img src="/images/bio3.jpg" alt=""/>
        <div class="descr">
          <div class="title">
            <div class="cell">Bioshock infinite:<br />
              Clash in the Clouds</div>
          </div>
          <div class="price"><strong>169</strong> руб.</div>
        </div>
        </a> </div>
      <div class="clr"></div>
    </div>
    <div class="more similar">
      <div class="ttl">Похожие игры:</div>
      <div class="item"><a href="#"> <img src="/images/bio4.jpg" alt=""/>
        <div class="descr">
          <div class="title">
            <div class="cell">Метро 2033:<br />
              Луч надежды</div>
          </div>
          <div class="price"><strong>599</strong> руб.</div>
        </div>
        </a> </div>
      <div class="item"><a href="#"> <img src="/images/bio5.jpg" alt=""/>
        <div class="descr">
          <div class="title">
            <div class="cell">BioShock</div>
          </div>
          <div class="price"><strong>249</strong> руб.</div>
        </div>
        </a> </div>
      <div class="item"><a href="#"> <img src="/images/bio6.jpg" alt=""/>
        <div class="descr">
          <div class="title">
            <div class="cell">BioShock2</div>
          </div>
          <div class="price"><strong>299</strong> руб.</div>
        </div>
        </a> </div>
      <div class="clr"></div>
    </div>
    </aside>

    <div id="content_resume" class="content_resume">
      <p>BioShock Infinite - продолжение экшена от первого лица с элементами РПГ, разработанное
        студией Irrational Games.</p>
      <p>Действие BioShock Infinite разворачиваются в начале XX века, за 50 лет до событий в Восторге.
        В 1900 году Америка создала воздушный город Колумбия, который стал чудом среди изобретений
        человека. Но благодаря загадочному инциденту произошло восстание, и город испарился на
        несколько лет.</p>
    </div>

  </main>
  <?php include_once('game_sidebar.php') ?>
</div>
  <div class="clr"></div>
</div>

<?php include_once('footer.php') ?>
