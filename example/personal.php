<?php include_once('header.php') ?>

<div class="wrapper">

        <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="lastcrumb">Личный кабинет</li>
            </ul>
        </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="personalPage">
    <div class="pagetitle">
      <h1>Личный кабинет</h1>
    </div>

    <div class="balans">Ваш баланс равен <strong>152</strong> руб.</div>
    <div class="links">
		<table class="personal">
			<tbody>
				<tr>
					<td class="icons"><img src="/images/coinadd_icon.svg" width="16" /> </td>
					<td><a href="/balance.php">Пополнить счет</a></td>
				</tr>
				<tr>
					<td class="icons"><img src="/images/referal_icon.svg" width="14" /> </td>
					<td><a href="/partnership.php">Партнерская программа</a></td>
				</tr>
				<tr>
					<td class="icons"><img src="/images/sale_icon.svg" width="23" /> </td>
					<td><a href="/balance.php">Накопительная скидка</a></td>
				</tr>
				<tr>
					<td class="icons"><img src="/images/mycart_icon.svg" width="15" /> </td>
					<td><a href="/purchases.php">Мои покупки</a></td>
				</tr>
				<tr>
					<td class="icons"><img src="/images/edit_icon.svg" width="13" /> </td>
					<td><a href="/edit.php">Редактировать аккаунт</a></td>
				</tr>
			</tbody>
		</table>
    </div>
  </section>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
