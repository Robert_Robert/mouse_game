<div class="catalog_block">
<aside>
  <div class="catalog_filter">
    <ul>
      <li><a href="#">Рекомендации</a></li>
      <li><a href="#">Скидки</a></li>
      <li><a href="#">Новинки</a></li>
      <li><a href="#">Лидеры продаж</a></li>
      <li><a href="#">Предзаказ</a></li>
    </ul>
  </div>
</aside>

<article>
  <div class="pager">
    <ul>
      <li class="first"><a href="#">‹‹</a></li>
      <li class="prev"><a href="#"></a></li>
      <li class="current page"><a href="#">1</a></li>
      <li class="page"><a href="#">2</a></li>
      <li class="page"><a href="#">3</a></li>
      <li class="next"><a href="#"></a></li>
      <li class="last"><a href="#">››</a></li>
    </ul>
  </div>
  <div class="sorting">
    <div class="label">Сортировать по </div>
    <div class="select big">
    	<div class="ar">▼</div>
      <input value="по популярности" class="type" readonly />
      <ul>
        <li>по популярности</li>
        <li>по цене</li>
        <li>по названию</li>
      </ul>
    </div>
    <div class="select small">
    	<div class="ar">▼</div>
      <input value="16" class="count" readonly />
      <ul>
        <li>16</li>
        <li>32</li>
        <li>64</li>
      </ul>
    </div>
    <div class="label"> позиций </div>
  </div>



<h2>Продажа цифровых лицензий игр</h2>

<div class="catalog">
    <div class="object"> <a href="game_page.php"> <img src="/images/MXGP.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">MXGP</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/DIABLO_III.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Diablo III:<br />
            Reaper of Souls (RU)<br />
          </div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/BATLEFIELD.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Battlefield 4:<br />
            Premium Service</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/TITANFALL.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Titanfall</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/THIEF.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Thief</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/NBA2K14.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">NBA2K14</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/BLACKGATE.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Batman:<br />
            Arkham Origins</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/BIOSHOCK.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">BioShock Infinite</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/RAYMAN.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Rayman Legends</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/SAINTSROW.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Saints Row: The Third</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/CALLOFDUTY.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Call of Duty:<br />
            Black Ops </div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/SIMS4.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">The sims 4</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/DEADISLAND.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Dead Island</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/MURDERED.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Murdered: Soul Suspect</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/SPLINTERSELL.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Tom Clancy’s<br />
            Splinter Cell Blacklist</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
    <div class="object"> <a href="game_page.php"> <img src="/images/CHIELDOFLIGHT.png" alt=""/>
      <div class="descr">
        <div class="title">
          <div class="cell">Child of Light</div>
        </div>
        <div class="price"><b>349</b> руб.</div>
      </div>
      </a> </div>
  </div>
  <div class="pager">
    <ul>
      <li class="first"><a href="#">‹‹</a></li>
      <li class="prev"><a href="#"></a></li>
      <li class="current page"><a href="#">1</a></li>
      <li class="page"><a href="#">2</a></li>
      <li class="page"><a href="#">3</a></li>
      <li class="next"><a href="#"></a></li>
      <li class="last"><a href="#">››</a></li>
    </ul>
  </div>

  <div><p>Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....</p></div>
</article>
</div>
