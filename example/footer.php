    <div id="footer_guarantor"></div>
</div>
</div>
<footer>
<div class="f_box">
  <div class="wrapper">
  		<div class="payments">
        	<a href="#" class="visa"><img src="/images/footer_VISA.svg" alt=""/></a>
            <a href="#" class="masterCard"><img src="/images/footer_MASTERCARD.svg" alt=""/></a>
            <a href="#" class="webmoney"><img src="/images/footer_WEBMONEY.svg" alt=""/></a>
            <a href="#" class="kiwi"><img src="/images/footer_QIWI.svg" alt=""/></a>
            <a href="#" class="mts"><img src="/images/MTS_logo.png" alt=""/></a>
            <a href="#" class="megafon"><img src="/images/footer_MEGAFON.svg" alt=""/></a>
            <a href="#" class="yandex"><img src="/images/footer_YANDEX.svg" alt=""/></a>
        </div>
		<div class="to_top">
			<a href="#"></a>
		</div>
  		<div class="copy">
        	MOUSEGAME.ru распространяет только легальный контент<br />&copy; 2014. MG.
        </div>
        <div class="top"></div>
  </div>
</div>
</footer>
<?php include('copyrights.php') ;?>

<script type="text/javascript" src="/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/js/init.js"></script>
</body>
</html>