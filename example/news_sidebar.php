<aside id="cart_sidebar">
	<div class="preorder block">
    	<div class="title">Предзаказ</div>
    	<ul>
        	<li><a href="#">Crysis 3 </a></li>
            <li><a href="#">Starcraft II: Heart of..</a></li>
            <li><a href="#">Real Heroes Firefighter</a></li>
            <li><a href="#">Scania Truck Driver</a></li>
        </ul>
		<div class="sidebar_footer">
			<div class="sidebar_border"></div>
		</div>
    </div>
    <div class="actions block">
    	<div class="title">Акции</div>
    	<ul>
        	<li><a href="#">Arma 3</a><span class="percent">-50%</span></li>
            <li><a href="#">Battlefield 4</a><span class="percent">-29%</span></li>
            <li><a href="#">NBA 2k14</a><span class="percent">-25%</span></li>
            <li><a href="#">The Sims 4</a><span class="percent">-17%</span></li>
        </ul>
		<div class="sidebar_footer">
			<div class="sidebar_border"></div>
		</div>
    </div>
</aside>