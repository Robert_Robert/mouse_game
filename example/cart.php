<?php include_once('header.php') ?>

<div class="wrapper">

        <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="lastcrumb">Ваша корзина</li>
            </ul>
        </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="cartPage">
    <div class="pagetitle">
      <h1>Ваша корзина</h1>
    </div>

    <div class="cartTable">
      <table>
        <tr>
          <th class="name">Наименование</th>
          <th>Количество</th>
          <th class="prices">Цена</th>
          <th></th>
        </tr>
        <tr class="product">
          <td class="title"><a href="#">Create (PC)</a></td>
          <td class="count"><div><span class="plus"></span><span class="minus"></span>
              <input type="text" value="2" />
            </div></td>
          <td class="price">598 руб.</td>
          <td class="removeItem"><a href="#"></a></td>
        </tr>
        <tr class="product">
          <td class="title"><a href="#">SimCity Limited Edition (PC)</a></td>
          <td class="count"><div><span class="plus"></span><span class="minus"></span>
              <input type="text" value="1" />
            </div></td>
          <td class="price">1199 руб.</td>
          <td class="removeItem"><a href="#"></a></td>
        </tr>
        <tr class="product">
          <td class="title"><a href="#">Arma 2: Операция Стрела + Day Z (PC)</a></td>
          <td class="count"><div><span class="plus"></span><span class="minus"></span>
              <input type="text" value="1" />
            </div></td>
          <td class="price">249 руб.</td>
          <td class="removeItem"><a href="#"></a></td>
        </tr>
      </table>
    </div>
    <div class="totals">
      <div class="count">
        <div class="label">Количество:</div>
        <div class="value">4шт.</div>
      </div>
      <div class="price">
        <div class="label">Общая цена:</div>
        <div class="value">2046 руб.</div>
      </div>
      <div class="sale">
        <div class="promo">
          <div class="label">Промокод</div>
          <input type="text" value="KAJDFLKJHAf" />
          <input class="small" type="button" value="Применить" />
        </div>
        <div class="label">Накопительная скидка:</div>
        <div class="value">50 руб.</div>
      </div>
      <div class="total">
        <div class="label">Цена с учетом скидки:</div>
        <div class="value">1996 руб.</div>
      </div>
    </div>
    <div class="payments">
    	<table>
        	<tr>
            	<th></th>
                <th colspan="2" class="pay_method">Способ оплаты</th>
                <th class="com">Комиссия</th>
            </tr>
            <tr>
            	<td class="logo"><div class="cell visa"><img src="/images/footer_VISA.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>Оплата банковской картой Visa</td>
                <td class="com">0%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell masterCard"><img src="/images/footer_MASTERCARD.svg" alt=""/></div></td>
                <td class="input"><span class="checked"></span></td>
                <td>Оплата банковской картой MasterCard</td>
                <td class="com">0%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell webmoney"><img src="/images/footer_WEBMONEY.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>Платежная система Webmoney</td>
                <td class="com">0.8%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell kiwi"><img src="/images/footer_QIWI.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>Оплата через Киви Кошелек</td>
                <td class="com">0%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell mts"><img src="/images/footer_MTS.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>SMS для абонентов МТС</td>
                <td class="com">7.5%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell megafon"><img src="/images/footer_MEGAFON.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>SMS для абонентов МегаФон</td>
                <td class="com">11%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell yandex"><img src="/images/footer_YANDEX.svg" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>Платежная система Яндекс.Деньги</td>
                <td class="com">0%</td>
            </tr>
            <tr>
            	<td class="logo"><div class="cell mousegame"><img src="/images/logo.png" alt=""/></div></td>
                <td class="input"><span></span></td>
                <td>С внутреннего баланса mousegame.ru</td>
                <td class="com">0%</td>
            </tr>
        </table>
    </div>
    <div class="paymentTotal">
    	<div class="com"><div>Комиссия платежной системы:<span>0 руб.</span></div></div>
        <div class="total"><div class="pay_summ">Сумма к оплате:</div><div class="gray"><span>1996 руб.</span><input type="button" value="Оплатить"></div>
   	 </div>
	</div>
  </section>
  <?php include_once('cart_sidebar.php') ?>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
