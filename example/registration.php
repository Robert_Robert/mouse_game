<?php include_once('header.php') ?>

<div class="wrapper">

        <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="lastcrumb">Регистрация</li>
            </ul>
        </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="registration">
    <div class="pagetitle">
      <h1>Регистрация</h1>
    </div>

    <form action="#">
    <div class="label">Введите Ваше имя/nickname</div>
    <input type="text" />
    <div class="label">Введите Ваш e-mail</div>
    <input type="text" />
    <div class="label">Введите Ваш пароль</div>
    <input type="text" />
    <div class="label">Подтвердите Ваш пароль</div>
    <input type="text" />
    <div>
    	<input type="checkbox" /> <label>Я согласен (-на) с условиями пользовательского соглашения</label>
    </div>
    <div>
    	<input type="checkbox" /> <label>Я хочу получать новости о новинках и акциях по электронной почте</label>
    </div>
   	<div>
		<div class="lable captcha">Введите код с картинки</div>
		<input type="text" class="captcha" />
		<a class="capcha captcha" href="#"></a>
   	</div>
	<div class="clr"></div>
    <input type="submit" value="Регистрация" class="registration" />
    </form>
  </section>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
