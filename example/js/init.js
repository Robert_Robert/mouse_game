// JavaScript Document
$(function(){
	var request_uri = location.pathname + location.search;
	console.log(request_uri)
	if(request_uri == '/watch_dogs.php'){
		$('.body_new').css('background', '#fff url(/images/body_bg_whatchdogs.jpg) no-repeat top center');
		$('#wrapper_all').addClass('watch_dogs');
		$('header').addClass('watch_dogs');
		$('body #wrapper_all .wrapper').addClass('watch_dogs');
		$('header .wrapper').removeClass('watch_dogs');
	}
	if(request_uri == '/game_page-w.php'){
		$('.body_new').css('background', '#fff url(/images/body_bg_whatchdogs.jpg) no-repeat top center');
		$('#wrapper_all').addClass('watch_dogs');
		$('header').addClass('watch_dogs');
		$('body #wrapper_all .wrapper').addClass('watch_dogs').css('margin-top',110);
		$('header .wrapper').removeClass('watch_dogs').css('margin-top',0);
	}
	
	
	$('.main_slider .wrp').bxSlider({
		auto:true,
		controls: false
	});
	$('.select input, .select .ar').click(function(){
			var  select_input = $('.type', $(this).parent());
			var ul =$('ul',$(this).parent());
			select_input.css('border-bottom-left-radius', '0');
			ul.show();
			var firstClick = true;
        	$(document).bind('click.myEvent', function(e) {
            	if (!firstClick && $(e.target).closest(ul.parents('.select')).length == 0) {
                	ul.hide();
					select_input.css('border-bottom-left-radius', '5px');
               		$(document).unbind('click.myEvent');
            	}
            firstClick = false;
        	});
		});	
	$('.select ul li').click(function(){
			$('input',$(this).parents('.select')).val($(this).text());
			$(this).parent().hide();
			$('.type', $(this).parents('.big')).css('border-bottom-left-radius', '5px');
			$(document).unbind('click.myEvent');
		});	
		
	$('.content .more_next').click(function(e){
		e.preventDefault();
		$(this).remove();
		$('.content_hide').show();
		});	
	$('.params .more_next').click(function(e){
		e.preventDefault();
		$(this).remove();
		$('.hidden').show();
		});	

	    $("#more_link").on("click", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 700);
	    });
});

