<section id="cart_sidebar">
	<div class="preorder block">
    	<div class="title"><h2>Предзаказ</h2></div>
    	<ul>
        	<li><a href="#">Crysis 3 </a></li>
            <li><a href="#">Starcraft II: Heart of..</a></li>
            <li><a href="#">Real Heroes Firefighter</a></li>
            <li><a href="#">Scania Truck Driver</a></li>
        </ul>
		<div class="sidebar_footer">
			<div class="sidebar_border"></div>
		</div>
    </div>
</section>
<section  id="cart_sidebar_rec">
    <div class="recommend block">
    	<div class="title"><h2>Рекомендации</h2></div>
    	<ul>
        	<li><a href="#">Arma 3</a></li>
            <li><a href="#">Battlefield 4</a></li>
            <li><a href="#">NBA 2k14</a></li>
            <li><a href="#">The Sims 4</a></li>
        </ul>
		<div class="sidebar_footer">
			<div class="sidebar_border"></div>
		</div>
    </div>
</section>