<?php
ini_set('display_errors', 0);
	session_start();
if(!isset($_SESSION['login'])&& isset($_GET['login'])){
		$_SESSION['login'] = $_GET['login'];
		}
	if(isset($_SESSION['login'])&& isset($_GET['logout'])){
		unset($_SESSION['login']);
		}
?>
<!DOCTYPE html>

<html lang="ru">

<head>

<meta charset="utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />

<title>MOUSEGAME</title>

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

<!-- Add Libraries API -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- /Add Libraries API -->

<!-- Start pack -->
<link rel="stylesheet" type="text/css" href="/css/layout.css" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<link rel="stylesheet" type="text/css" href="/css/menu.css" />

<link rel="stylesheet" type="text/css" href="/js/jquery.bxslider/jquery.bxslider.css" />
</head>
<body class="body_new">

<div class="wrapper-border">

<div id="wrapper_all">
<div class="top_panel"></div>
    <header>
      <div class="wrapper">
        <div class="logo"> <a href="/"><img src="/images/logo.svg" title="на главную" alt=""/></a> </div>
        <div class="login">
        <?php
			//var_dump($_SESSION['login']);
		if(!isset($_SESSION['login'])){ ?>
        	<div class="not_log">
        	<a href="/login.php" id="login">вход</a>/<a href="/registration.php" id="registration" >регистрация</a>
            </div>
		<?php }
		else{ ?>
        	<div class="log">
        	<a href="/personal.php" id="personal" ><?php echo $_SESSION['login'] ?></a><a href="/?logout=true" id="logout"></a>
           </div>
		<?php }?>
        </div>
        <div class="search"><input type="text" value="Поиск..." plaseholder="Поиск..." /></div>
        <div class="social"><a href="#" class="tw"></a><a href="#" class="vk"></a><a href="#" class="google"></a><a href="#" class="fb"></a></div>
        <div class="menu_block">
        	<div class="filter">
            	<div class="top">
                <nav>
                	<a class="all" <?php if($_GET['page']=='Все') echo "class='all current'"; ?> href="/web_letter.php?page=Все">Все</a>
                    <a <?php if($_GET['page']=='А') echo "class='current'"; ?> href="/web_letter.php?page=А">A</a>
                    <a <?php if($_GET['page']=='Б') echo "class='current'"; ?> href="/web_letter.php?page=Б">Б</a>
                    <a <?php if($_GET['page']=='В') echo "class='current'"; ?> href="/web_letter.php?page=В">В</a>
                    <a <?php if($_GET['page']=='Г') echo "class='current'"; ?> href="/web_letter.php?page=Г">Г</a>
                    <a <?php if($_GET['page']=='Д') echo "class='current'"; ?> href="/web_letter.php?page=Д">Д</a>
                    <a href="#" class="not">Е</a>
                    <a href="#" class="not">Ж</a>
                    <a href="#" class="not">З</a>
                    <a <?php if($_GET['page']=='И') echo "class='current'"; ?> href="/web_letter.php?page=И">И</a>
                    <a <?php if($_GET['page']=='К') echo "class='current'"; ?> href="/web_letter.php?page=К">К</a>
                    <a href="Л" class="not">Л</a>
                    <a <?php if($_GET['page']=='М') echo "class='current'"; ?> href="/web_letter.php?page=М">М</a>
                    <a <?php if($_GET['page']=='Н') echo "class='current'"; ?> href="/web_letter.php?page=Н">Н</a>
                    <a <?php if($_GET['page']=='О') echo "class='current'"; ?> href="/web_letter.php?page=О">О</a>
                    <a <?php if($_GET['page']=='П') echo "class='current'"; ?> href="/web_letter.php?page=П">П</a>
                    <a <?php if($_GET['page']=='Р') echo "class='current'"; ?> href="/web_letter.php?page=Р">Р</a>
                    <a <?php if($_GET['page']=='С') echo "class='current'"; ?> href="/web_letter.php?page=С">С</a>
                    <a <?php if($_GET['page']=='Т') echo "class='current'"; ?> href="/web_letter.php?page=Т">Т</a>
                    <a href="#" class="not">У</a>
                    <a <?php if($_GET['page']=='Ф') echo "class='current'"; ?> href="/web_letter.php?page=Ф">Ф</a>
                    <a <?php if($_GET['page']=='Х') echo "class='current'"; ?> href="/web_letter.php?page=Х">Х</a>
                    <a href="#" class="not">Ц</a>
                    <a <?php if($_GET['page']=='Ч') echo "class='current'"; ?> href="/web_letter.php?page=Ч">Ч</a>
                    <a <?php if($_GET['page']=='Ш') echo "class='current'"; ?> href="/web_letter.php?page=Ш">Ш</a>
                    <a <?php if($_GET['page']=='Щ') echo "class='current'"; ?> href="/web_letter.php?page=Щ">Щ</a>
                    <a <?php if($_GET['page']=='Э') echo "class='current'"; ?> href="/web_letter.php?page=Э">Э</a>
                    <a href="#" class="not">Ю</a>
                    <a href="#" class="not">Я</a>
                </nav>
                </div>
                <div class="bottom">
                <nav>
                	<a <?php if($_GET['page']=='0..9') echo "class='all current'"; ?> href="/web_letter.php?page=0..9" class="all">0..9</a>
                    <a <?php if($_GET['page']=='A') echo "class='current'"; ?> href="/web_letter.php?page=A">A</a>
                    <a <?php if($_GET['page']=='B') echo "class='current'"; ?> href="/web_letter.php?page=B">B</a>
                    <a <?php if($_GET['page']=='C') echo "class='current'"; ?> href="/web_letter.php?page=C">C</a>
                    <a <?php if($_GET['page']=='D') echo "class='current'"; ?> href="/web_letter.php?page=D">D</a>
                    <a <?php if($_GET['page']=='E') echo "class='current'"; ?> href="/web_letter.php?page=E">E</a>
                    <a <?php if($_GET['page']=='F') echo "class='current'"; ?> href="/web_letter.php?page=F">F</a>
                    <a <?php if($_GET['page']=='G') echo "class='current'"; ?> href="/web_letter.php?page=G">G</a>
                    <a <?php if($_GET['page']=='H') echo "class='current'"; ?> href="/web_letter.php?page=H">H</a>
                    <a <?php if($_GET['page']=='I') echo "class='current'"; ?> href="/web_letter.php?page=I">I</a>
                    <a <?php if($_GET['page']=='J') echo "class='current'"; ?> href="/web_letter.php?page=J">J</a>
                    <a <?php if($_GET['page']=='K') echo "class='current'"; ?> href="/web_letter.php?page=K">K</a>
                    <a <?php if($_GET['page']=='L') echo "class='current'"; ?> href="/web_letter.php?page=L">L</a>
                    <a <?php if($_GET['page']=='M') echo "class='current'"; ?> href="/web_letter.php?page=M">M</a>
                    <a <?php if($_GET['page']=='N') echo "class='current'"; ?> href="/web_letter.php?page=N">N</a>
                    <a <?php if($_GET['page']=='O') echo "class='current'"; ?> href="/web_letter.php?page=O">O</a>
                    <a <?php if($_GET['page']=='P') echo "class='current'"; ?> href="/web_letter.php?page=P">P</a>
                    <a href="#" class="not">Q</a>
                    <a <?php if($_GET['page']=='R') echo "class='current'"; ?> href="/web_letter.php?page=R">R</a>
                    <a <?php if($_GET['page']=='S') echo "class='current'"; ?> href="/web_letter.php?page=S">S</a>
                    <a <?php if($_GET['page']=='T') echo "class='current'"; ?> href="/web_letter.php?page=T">T</a>
                    <a <?php if($_GET['page']=='U') echo "class='current'"; ?> href="/web_letter.php?page=U">U</a>
                    <a <?php if($_GET['page']=='V') echo "class='current'"; ?> href="/web_letter.php?page=V">V</a>
                    <a <?php if($_GET['page']=='W') echo "class='current'"; ?> href="/web_letter.php?page=W">W</a>
                    <a <?php if($_GET['page']=='X') echo "class='current'"; ?> href="/web_letter.php?page=X">X</a>
                    <a <?php if($_GET['page']=='Y') echo "class='current'"; ?> href="/web_letter.php?page=Y">Y</a>
                    <a <?php if($_GET['page']=='Z') echo "class='current'"; ?> href="/web_letter.php?page=Z">Z</a>
                </nav>
                </div>
            </div>
            <div class="mainmenu">
            <nav>
            	<ul>
                	<li><a href="/content_page.php">О магазине</a></li>
                    <li><a href="/support.php">Поддержка</a></li>
                    <li><a href="/content_page.php">FAQ</a></li>
                </ul>
            </nav>
            </div>
            <a href="/cart.php" class="minicart">
            	<div class="total">0</div>
            </a>
        </div>
      </div>
    </header>