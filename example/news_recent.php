<?php include_once('header.php') ?>

<div class="wrapper">

        <div class="breadcrumb">
            <ul>
                <li class="firstcrumb"><a href="/index.php"><img src="images/home.png" alt=""></a></li>
                <li class="lastcrumb">Новости</li>
            </ul>
        </div>

  <?php include_once('left_sidebar.php') ?>
  <section id="middle" class="news_page">
    <div class="pagetitle">
      <h1>Новости</h1>
    </div>
    <div class="content">
    <div class="news_banner">
    	<img src="/images/news.png" />
    </div>
    <h1 class="title">Deluxe издания Assassin's Creed IV и Watch Dogs.</h1>
    <div class="date">25.12.2014</div>
    <div class="clr"></div>
    <div class="info">Мы рады Вам предложить <strong>Deluxe издания <a href="#">Assassin's Creed IV</a> и <a href="#">Watch Dogs</a> на хороших условиях.</strong>
<strong>При покупке любого из изданий вы получаете промокод на скидку 50 рублей.</strong></div>
    </div>
    <a class="backto" href="/news.php">Перейти ко всем новостям.</a>
  </section>
  <?php include_once('news_sidebar.php') ?>
  <div class="clr"></div>
</div>
<?php include_once('footer.php') ?>
