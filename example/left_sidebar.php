<aside id="left_sidebar">
	<div class="game_menu">
    	<div class="title">ЖАНРЫ</div>
        <nav>
    	<ul>
        	<li id="all" <?php if($_GET['page']=='Все игры') echo "class='active'"; ?>><a href="/game_menu_el.php?page=Все игры">Все игры</a></li>
            <li id="actions" <?php if($_GET['page']=='Action/Шутеры') echo "class='active'"; ?>><a href="/game_menu_el.php?page=Action/Шутеры">Action/Шутеры</a></li>
            <li id="rpg" <?php if($_GET['page']=='RPG') echo "class='active'"; ?>><a href="/game_menu_el.php?page=RPG">RPG</a></li>
            <li id="sport" <?php if($_GET['page']=='Спортивные') echo "class='active'"; ?>><a href="/game_menu_el.php?page=Спортивные">Спортивные</a></li>
            <li id="rts" <?php if($_GET['page']=='Стратегии') echo "class='active'"; ?>><a href="/game_menu_el.php?page=Стратегии">Стратегии</a></li>
            <li id="racing" <?php if($_GET['page']=='Гонки') echo "class='active'"; ?>><a href="/game_menu_el.php?page=Гонки">Гонки</a></li>
            <li id="sim" <?php if($_GET['page']=='Симуляторы') echo "class='active'"; ?>><a href="/game_menu_el.php?page=Симуляторы">Симуляторы</a></li>
            <li id="others" <?php if($_GET['page']=='Другие') echo "class='active'"; ?>><a href="/game_menu_el.php?page=Другие">Другие</a></li>
        </ul>
        </nav>
    </div>
    <div class="boards_menu">
    	<div class="title">ПЛАТФОРМЫ</div>
        <nav>
    	<ul>
        	<li id="Steam" <?php if($_GET['page']=='Steam') echo "class='active'"; ?>><a <?php if($_GET['page']=='Steam') echo "href='#'"; ?> href="/game_menu_el.php?page=Steam">Steam</a></li>
            <li id="Origin" <?php if($_GET['page']=='Origin') echo "class='active'"; ?>><a <?php if($_GET['page']=='Origin') echo "href='#'"; ?> href="/game_menu_el.php?page=Origin">Origin</a></li>
			<li id="Mac" <?php if($_GET['page']=='Mac игры') echo "class='active'"; ?>><a <?php if($_GET['page']=='Mac игры') echo "href='#'"; ?>  href="/game_menu_el.php?page=Mac игры">Mac игры</a></li>
            <li id="Xbox" <?php if($_GET['page']=='Xbox LIVE') echo "class='active'"; ?>><a <?php if($_GET['page']=='Xbox LIVE') echo "href='#'"; ?>  href="/game_menu_el.php?page=Xbox LIVE">Xbox LIVE</a></li>
            <li id="PS" <?php if($_GET['page']=='PS Network') echo "class='active'"; ?>><a <?php if($_GET['page']=='PS Network') echo "href='#'"; ?>  href="/game_menu_el.php?page=PS Network">PS Network</a></li>
            <li id="iTunes" <?php if($_GET['page']=='iTunes') echo "class='active'"; ?>><a <?php if($_GET['page']=='iTunes') echo "href='#'"; ?>  href="/game_menu_el.php?page=iTunes">iTunes</a></li>
        </ul>
        </nav>
    </div>
    <section>
    <div class="news">
    	<h3 class="title">НОВОСТИ</h3>
        <ul class="news_list">
        	<li class="item">
            	<div class="ttl"><a href="/news_recent.php">Deluxe издания Assassin's Creed IV и Watch Dogs </a></div>
                <div class="intro">Множество интересных бонусов и дополнительное игровое время!</div>
            </li>
            <li class="item">
            	<div class="ttl"><a href="/news_recent.php">Borderlands 2 Game of the Year Edition - релиз состоялся!</a></div>
                <div class="intro">Новое издание, содержащее несколько дополнений, наборов и других интересных бонусов!</div>
            </li>
            <li class="item">
            	<div class="ttl"><a href="/news_recent.php">XCOM: Enemy Within - старт предзаказов </a></div>
                <div class="intro">Новые навыки, улучшения, оружие, карты, миссии и многое другое!</div>
            </li>
            <li class="item">
            	<div class="ttl"><a href="/news_recent.php">Season Pass - теперь и для Assassin’s Creed IV Black Flag</a></div>
                <div class="intro">Эксклюзивный пакет "Корабль Кракена", новая одиночная кампания и многое другое!</div>
            </li>
            <li class="item">
            	<div class="ttl"><a href="/news_recent.php">NBA 2K14 - уже в продаже</a></div>
                <div class="intro">Отличный баскетбольный симулятор!</div>
            </li>
            <li class="item">
            	<div class="ttl"><a href="/news_recent.php">Teleglitch: Die More Edition - скидка 75%!</a></div>
                <div class="intro">Добро пожаловать в мир ужаса, паранойи и страха!</div>
            </li>
            <li class="item">
            	<div class="ttl"><a href="/news_recent.php">Стартовал ранний доступ в War of the Vikings</a></div>
                <div class="intro">Примите участие в яростных схватках на полях сражений эпохи викингов!</div>
            </li>
        </ul>
    </div>
    </section>
    <div class="email_res">
    	<div class="box">
    	<div class="title">Подписка на новости</div>
        <div class="email">
		<a href="/rss.php" class="rss"></a>
        <input type="text" value="E-mail" placeholder="Email" />
        </div>
        </div>
    </div>
</aside>