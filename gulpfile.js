var elixir = require('laravel-elixir'),
    gulp = require('gulp');

elixir.config.production = true;
elixir.config.bowerDir = 'resources/bower';

// отключаем генерацию sourcemaps
elixir.config.sourcemaps = true;

gulp.task('GulpUse', function(){
    console.log('');
    console.log('');
    console.log('Example use: gulp --<task>');
    console.log('Available tasks:');
    console.log('    --admin');
    console.log('    --agelnash');
    console.log('    --v1');
    console.log('');
});

elixir(function(mix) {
    // Получаю аргументы для gulp.
    var isWatch = process.argv.indexOf('watch') == 2,
        options = [],
        anyRun = false;
    process.argv.forEach(function(item, i, arr){
        if(i > (isWatch ? 2 : 1) ){
            options.push(item);
        }
    });

    if (options.indexOf('--admin') >= 0) {
        anyRun = true;

        console.log('Copy FontAwesome files');
        mix.styles(["bower/components-font-awesome/css/font-awesome.min.css"], 'public/assets/admin/css/font-awesome.min.css', 'resources/')
            .copy('resources/bower/components-font-awesome/fonts', 'public/assets/admin/fonts/');

        console.log('Compile Agel Nash helper files');
        mix.scripts([
            "assets/js/agel_nash/Hash.js",
            "assets/js/agel_nash/jHelper.js",
            "assets/js/agel_nash/jquery.dataSelector.js",
            "assets/js/agel_nash/jquery.dataSerialize.js"
        ], 'resources/assets/js/agel_nash.min.js', 'resources/')
            .copy('resources/assets/js/agel_nash.min.js', 'public/assets/admin/js/agel_nash.min.js');
    }

    if (options.indexOf('--agelnash') >= 0) {
        anyRun = true;

        console.log('Compile Agel Nash helper files');
        mix.scripts([
            "assets/js/agel_nash/Hash.js",
            "assets/js/agel_nash/jHelper.js",
            "assets/js/agel_nash/jquery.dataSelector.js",
            "assets/js/agel_nash/jquery.dataSerialize.js"
        ], 'resources/assets/js/agel_nash.min.js', 'resources/');
    }

    if(options.indexOf('--v1') >= 0) {
        anyRun = true;

        // Browserify APP JS
        mix.browserify('assets/v1/script.js', 'public/themes/v1/assets/app.js', "resources/");

        mix.copy('resources/bower/fancybox/source/blank.gif', 'public/themes/v1/assets/blank.gif')
            .copy('resources/bower/fancybox/source/fancybox_loading.gif', 'public/themes/v1/assets/fancybox_loading.gif')
            .copy('resources/bower/fancybox/source/fancybox_loading@2x.gif', 'public/themes/v1/assets/fancybox_loading@2x.gif')
            .copy('resources/bower/fancybox/source/fancybox_overlay.png', 'public/themes/v1/assets/fancybox_overlay.png')
            .copy('resources/bower/fancybox/source/fancybox_sprite.png', 'public/themes/v1/assets/fancybox_sprite.png')
            .copy('resources/bower/fancybox/source/fancybox_sprite@2x.png', 'public/themes/v1/assets/fancybox_sprite@2x.png')
            .copy('resources/bower/fancybox/source/helpers/fancybox_buttons.png', 'public/themes/v1/assets/fancybox_buttons.png');

        mix.copy('resources/bower/bxslider-4/dist/images/bx_loader.gif', 'public/themes/v1/assets/images/bx_loader.gif')
            .copy('resources/bower/bxslider-4/dist/images/controls.png', 'public/themes/v1/assets/images/controls.png');

        mix.copy('resources/assets/v1/images/', 'public/themes/v1/assets/images/')
            .copy('resources/assets/v1/iepngfix/', 'public/themes/v1/assets/iepngfix/')
            .copy('resources/assets/v1/fonts/', 'public/themes/v1/assets/fonts/');

        // Core CSS
        mix.styles([
            "bower/bxslider-4/dist/jquery.bxslider.css",
            //"bower/jquery-ui/themes/smoothness/jquery-ui.min.css",
            "bower/fancybox/source/jquery.fancybox.css",
            "bower/jGrowl/jquery.jgrowl.min.css"
        ], 'public/themes/v1/assets/core.min.css', 'resources/');

        // App CSS
        mix.styles([
            "assets/v1/layout.css",
            "assets/v1/forms.css",
            "assets/v1/modules.css",
            "assets/v1/grid.css",
            "assets/v1/style.css",
            "assets/v1/menu.css",
            "assets/v1/class.css",
            "assets/v1/custom.css"
        ], 'public/themes/v1/assets/app.min.css', 'resources/');

        // Fonts CSS
        mix.styles([
            "assets/v1/fonts.css"
        ], 'public/themes/v1/assets/fonts.min.css', 'resources/');

        // Core JS
        mix.scripts([
            'assets/js/no-console.js',
            'bower/jquery/dist/jquery.js',
            'bower/jGrowl/jquery.jgrowl.min.js',
            //'bower/jquery-ui/jquery-ui.js',
            'bower/jsUri/Uri.js',
            'bower/jquery-validation/dist/jquery.validate.js',
            'assets/js/validate.ru.js',
            'bower/jquery.maskedinput/dist/jquery.maskedinput.js',
            'bower/jquery.cookie/jquery.cookie.js',
            'bower/html5-history-api/history.js',
            'bower/bxslider-4/dist/jquery.bxslider.js',
            'bower/fancybox/source/jquery.fancybox.js',
            'bower/devbridge-autocomplete/dist/jquery.autocomplete.min.js',
            "bower/jquery_lazyload/jquery.lazyload.js",
            "bower/jquery_lazyload/jquery.scrollstop.js",
            "assets/js/agel_nash/Hash.js",
            "assets/js/agel_nash/jHelper.js",
            "assets/js/agel_nash/jquery.dataSelector.js",
            "assets/js/agel_nash/jquery.dataSerialize.js",
            "assets/js/anAjax.js"
        ], 'public/themes/v1/assets/core.min.js', 'resources/');

        // Core APP
        mix.scripts([
            'themes/v1/assets/app.js'
        ], 'public/themes/v1/assets/app.min.js', 'public/');

        /*mix.version([
            "public/themes/v1/assets/fonts.min.css",
            "public/themes/v1/assets/core.min.css",
            "public/themes/v1/assets/app.min.css",
            "public/themes/v1/assets/core.min.js",
            "public/themes/v1/assets/app.min.js"
        ]).copy('public/themes/v1/assets/images/', 'public/build/themes/v1/assets/images/')
          .copy('resources/assets/v1/iepngfix/', 'public/build/themes/v1/assets/iepngfix/')
          .copy('public/themes/v1/assets/fonts/', 'public/build/themes/v1/assets/fonts/')
          .copy('public/themes/v1/assets/blank.gif', 'public/build/themes/v1/assets/blank.gif')
          .copy('public/themes/v1/assets/fancybox_buttons.png', 'public/build/themes/v1/assets/fancybox_buttons.png')
          .copy('public/themes/v1/assets/fancybox_loading@2x.gif', 'public/build/themes/v1/assets/fancybox_loading@2x.gif')
          .copy('public/themes/v1/assets/fancybox_overlay.png', 'public/build/themes/v1/assets/fancybox_overlay.png')
          .copy('public/themes/v1/assets/fancybox_sprite.png', 'public/build/themes/v1/assets/fancybox_sprite.png')
          .copy('public/themes/v1/assets/fancybox_sprite@2x.png', 'public/build/themes/v1/assets/fancybox_sprite@2x.png');*/
    }

    if( ! anyRun) mix.task('GulpUse');
});