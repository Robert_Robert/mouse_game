<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Upload dir
    |--------------------------------------------------------------------------
    |
    | The dir where to store the images (relative from public)
    |
    */
    'dir' => ['storage'],

    /*
    |--------------------------------------------------------------------------
    | Routes group config
    |--------------------------------------------------------------------------
    |
    | The default group settings for the elFinder routes.
    |
    */

    'route' => [
        'prefix' => 'elfinder',
        //'middleware' => 'admin.checkrole',
    ],

    /*
    |--------------------------------------------------------------------------
    | Access filter
    |--------------------------------------------------------------------------
    |
    | Filter callback to check the files
    |
    */

    //'access' => 'Barryvdh\Elfinder\Elfinder::checkAccess',

    /*
    |--------------------------------------------------------------------------
    | Roots
    |--------------------------------------------------------------------------
    |
    | By default, the roots file is LocalFileSystem, with the above public dir.
    | If you want custom options, you can set your own roots below.
    |
    */

    'roots' => [
		[
			'autoload' => true,
			//'filesystem' => new Filesystem(new LocalAdapter('/path/to/public_html')),
			'alias' => 'images',
			'mimeDetect' => 'auto',
			'driver' => 'LocalFileSystem',
			'path' => public_path('images/'),
			'URL' => '/images/',
			'tmbCrop'    => false,
			'dotFiles' => false,
			'dirSize' => false,
			//'accessControl' => '\Scard\Helpers\Elfinder::checkAccess',
			'defaults'   => array('read' => true, 'write' => true),
			'uploadAllow'  => array('image'),
			'uploadDeny'   => array('all'),
			'uploadOrder'  => 'deny,allow',
			'disabled'    => array('edit','extract', 'rename', 'archive', 'mkfile'),
		]
	],

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | These options are merged, together with 'roots' and passed to the Connector.
    | See https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options-2.1
    |
    */

    'options' => array(),

);
