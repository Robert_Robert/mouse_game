<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */

	'route' => 'i',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */
    
    'paths' => array(
        public_path('images')
    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation callbacks.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    */

	/**
	 * @TODO-agelnash: Шаблоны под нужные размеры
	 */
    'templates' => array(

        'game-list' => App\Lib\Image\GameList::class,
		'main-image' => App\Lib\Image\MainImage::class,
		'admin'		=> App\Lib\Image\Admin::class,
		'slide' 	=> App\Lib\Image\Slide::class,
		'play' => App\Lib\Image\Play::class,
		'big' => App\Lib\Image\Big::class,
		'admin-slide' => App\Lib\Image\AdminSlide::class,
		'news-list' => App\Lib\Image\NewsList::class,
		'game-card' => App\Lib\Image\GameCard::class,
		'search-image' => App\Lib\Image\SearchImage::class
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */
   
    'lifetime' => 43200,

);
