<?php

return [
	/*
	 * Admin title
	 * Displays in page title and header
	 */
	'title'                 => 'MouseGame',

	/*
	 * Admin url prefix
	 */
	'prefix'                => 'admin',

	/*
	 * Before filters to protect admin from unauthorized users
	 */
	'middleware'         => ['admin.checkrole'],

	/*
	 * Path to admin bootstrap files directory in app directory
	 * Default: 'app/admin'
	 */
	'bootstrapDirectory'    => app_path('admin'),

	/*
	 * Path to images directory
	 * Default: 'public/images'
	 */
	//'imagesDirectory'       => public_path('images'),

	/*
	 * Path to files directory
 	 * Default: 'public/files'
 	 */
	//'filesDirectory'        => public_path('files'),

	/*
	 * Path to images upload directory within 'imagesDirectory'
	 * Default: 'uploads'
	 */
	'imagesUploadDirectory' => implode('/', array('images', 'uploads', date('Y'), date('m'), date('d'))),

	/*
	 * Authentication config
	 */
	'auth'                  => [
		'model'  => '\App\Models\User', //'model' => '\SleepingOwl\AdminAuth\Entities\Administrator',
		'rules' => [
			'email' => 'required',
			'password' => 'required',
		]
	],

    /*
	 * Blade template prefix, default admin::
	 */
    //'bladePrefix'                => 'manager.',

	/*
	 * Template to use
	 */
	//'template'                => 'SleepingOwl\Admin\Templates\TemplateDefault',
	'template'                => 'AgelxNash\Admin\Template',

	/*
	 * Default date and time formats
	 */
	'datetimeFormat'          => 'd.m.Y H:i',
	'dateFormat'              => 'd.m.Y',
	'timeFormat'              => 'H:i',
];
