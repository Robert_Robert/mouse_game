<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => env('APP_DEBUG'),

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => env('HTTP_HOST', 'http://localhost'),

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'Europe/Moscow',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'ru',

	/*
	|--------------------------------------------------------------------------
	| Application Fallback Locale
	|--------------------------------------------------------------------------
	|
	| The fallback locale determines the locale to use when the current one
	| is not available. You may change the value to correspond to any of
	| the language folders that are provided through your application.
	|
	*/

	'fallback_locale' => 'ru',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => env('APP_KEY', 'SomeRandomString'),

	'cipher' => MCRYPT_RIJNDAEL_128,

	/*
	|--------------------------------------------------------------------------
	| Logging Configuration
	|--------------------------------------------------------------------------
	|
	| Here you may configure the log settings for your application. Out of
	| the box, Laravel uses the Monolog PHP logging library. This gives
	| you a variety of powerful log handlers / formatters to utilize.
	|
	| Available Settings: "single", "daily", "syslog", "errorlog"
	|
	*/

	'log' => 'daily',

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => [

		/*
		 * Laravel Framework Service Providers...
		 */
		'Illuminate\Foundation\Providers\ArtisanServiceProvider',
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Bus\BusServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Routing\ControllerServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Foundation\Providers\FoundationServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'AgelxNash\SEOPagination\PaginationServiceProvider',
		'Illuminate\Pipeline\PipelineServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		'Illuminate\Auth\Passwords\PasswordResetServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		'Illuminate\Broadcasting\BroadcastServiceProvider',

		/*
		 * Application Service Providers...
		 */
		App\Providers\AppServiceProvider::class,
		App\Providers\BusServiceProvider::class,
		App\Providers\ConfigServiceProvider::class,
		App\Providers\EventServiceProvider::class,
		App\Providers\RouteServiceProvider::class,
		App\Providers\ObserverServiceProvider::class,
		App\Providers\ValidationServiceProvider::class,
		App\Providers\FilesystemServiceProvider::class,

		/*
		 * Package Service Providers
		 */
		Intervention\Image\ImageServiceProvider::class,
		Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider::class,
		Teepluss\Theme\ThemeServiceProvider::class,
		Barryvdh\Debugbar\ServiceProvider::class,
		Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
		Collective\Html\HtmlServiceProvider::class,
		Bican\Roles\RolesServiceProvider::class,
		Conner\Tagging\TaggingServiceProvider::class,
		Laravelrus\LocalizedCarbon\LocalizedCarbonServiceProvider::class,
		Cviebrock\EloquentSluggable\SluggableServiceProvider::class,
		Barryvdh\Elfinder\ElfinderServiceProvider::class,
		Caffeinated\Menus\MenusServiceProvider::class,
		SleepingOwl\Admin\AdminServiceProvider::class,
		SleepingOwl\AdminLteTemplate\AdminLteTemplateServiceProvider::class,
		Artesaos\SEOTools\Providers\SEOToolsServiceProvider::class,
		AgelxNash\Admin\ServiceProvider::class,
		AgelxNash\SEOTools\ServiceProvider::class,
		Laracasts\Utilities\JavaScript\JavaScriptServiceProvider::class,
		Spatie\Backup\BackupServiceProvider::class,
		AgelxNash\Sorter\SorterServiceProvider::class,
		Darryldecode\Cart\CartServiceProvider::class
	],

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => [

		'App'       => 'Illuminate\Support\Facades\App',
		'Artisan'   => 'Illuminate\Support\Facades\Artisan',
		'Auth'      => 'Illuminate\Support\Facades\Auth',
		'Blade'     => 'Illuminate\Support\Facades\Blade',
		'Bus'       => 'Illuminate\Support\Facades\Bus',
		'Cache'     => 'Illuminate\Support\Facades\Cache',
		'Config'    => 'Illuminate\Support\Facades\Config',
		'Cookie'    => 'Illuminate\Support\Facades\Cookie',
		'Crypt'     => 'Illuminate\Support\Facades\Crypt',
		'DB'        => 'Illuminate\Support\Facades\DB',
		'Eloquent'  => 'Illuminate\Database\Eloquent\Model',
		'Event'     => 'Illuminate\Support\Facades\Event',
		'File'      => 'Illuminate\Support\Facades\File',
		'Hash'      => 'Illuminate\Support\Facades\Hash',
		'Input'     => 'Illuminate\Support\Facades\Input',
		'Inspiring' => 'Illuminate\Foundation\Inspiring',
		'Lang'      => 'Illuminate\Support\Facades\Lang',
		'Log'       => 'Illuminate\Support\Facades\Log',
		'Mail'      => 'Illuminate\Support\Facades\Mail',
		'Password'  => 'Illuminate\Support\Facades\Password',
		'Queue'     => 'Illuminate\Support\Facades\Queue',
		'Redirect'  => 'Illuminate\Support\Facades\Redirect',
		'Redis'     => 'Illuminate\Support\Facades\Redis',
		'Request'   => 'Illuminate\Support\Facades\Request',
		'Response'  => 'Illuminate\Support\Facades\Response',
		'Route'     => 'Illuminate\Support\Facades\Route',
		'Schema'    => 'Illuminate\Support\Facades\Schema',
		'Session'   => 'Illuminate\Support\Facades\Session',
		'Storage'   => 'Illuminate\Support\Facades\Storage',
		'URL'       => 'Illuminate\Support\Facades\URL',
		'Validator' => 'Illuminate\Support\Facades\Validator',
		'View'      => 'Illuminate\Support\Facades\View',

		/*
		 * Package Aliases
		 */
		'Img' => Intervention\Image\Facades\Image::class,
		'Theme' => Teepluss\Theme\Facades\Theme::class,
		'Debugbar' => Barryvdh\Debugbar\Facade::class,
		'LocalizedCarbon'   => Laravelrus\LocalizedCarbon\LocalizedCarbon::class,
		'DiffFormatter'     => Laravelrus\LocalizedCarbon\DiffFactoryFacade::class,

		'Menu' => Caffeinated\Menus\Facades\Menu::class,
		'Admin'         => SleepingOwl\Admin\Admin::class,
		'AdminAuth'     => SleepingOwl\AdminAuth\Facades\AdminAuth::class,
		'Column'        => SleepingOwl\Admin\Columns\Column::class,
		'ColumnFilter'  => SleepingOwl\Admin\ColumnFilters\ColumnFilter::class,
		'Filter'        => SleepingOwl\Admin\Filter\Filter::class,
		'AdminDisplay'  => SleepingOwl\Admin\Display\AdminDisplay::class,
		'AdminForm'     => SleepingOwl\Admin\Form\AdminForm::class,
		'AdminTemplate' => SleepingOwl\Admin\Templates\Facade\AdminTemplate::class,
		'FormItem'      => SleepingOwl\Admin\FormItems\FormItem::class,

		'Form' => Collective\Html\FormFacade::class,
		'Html' => Collective\Html\HtmlFacade::class,

		'SEO' => Artesaos\SEOTools\Facades\SEOTools::class,
		'JavaScript' => Laracasts\Utilities\JavaScript\JavaScriptFacade::class,
		'Sorter' => AgelxNash\Sorter\Facades\Sorter::class,
		'Cart' => Darryldecode\Cart\Facades\CartFacade::class
	],

];
