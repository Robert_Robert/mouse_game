;(function(window,document,$){
    window.anAjax = (function(){
        var anAjax = {
            lang: {
                errorSend: 'Не удалось получить ответ от сервера. Попробуйте повторить попытку позже',
                required: 'Это поле необходимо заполнить',
                errorLoad: 'Не удалось открыть страницу'
            },
            install: function(){
                $( document ).ajaxStart(function() {
                    $( 'html' ).addClass('ajaxStart');
                    $("body").append('<div class="ajax_loader"><div class="ajax_loader_shadow"></div><div class="ajax_loader_image"></div></div>');
                    $(".ajax_loader").fadeIn("fast");
                }).ajaxStop(function(){
                    $( 'html' ).removeClass('ajaxStart').addClass('ajaxComplete');
                    $(".ajax_loader").fadeOut("fast", function () {
                        $(this).remove();
                    });
                });
                $('body').on('click', '.no-click', function(e){
                    e.preventDefault();
                }).on('click', '.modal-link', function(e){
                    e.preventDefault();
                    var selector = $.jHelper.defaultData($(this), 'inline', '');
                    if(selector !== '' && $(selector).length){
                        anAjax.fancy($(selector).html());
                        anAjax.ajaxFormSend();
                    }
                }).on('click', 'a.ajax', function(e){
                    e.preventDefault();
                    $.ajax({
                        url: $(this).attr('href'),
                        data: {
                            data: $(this).data()
                        },
                        type: "GET"
                    }).done(function(responce){
                        anAjax.fancy(responce);
                        anAjax.init();
                    }).fail(function(){
                        anAjax.msg.error(anAjax.lang.errorLoad);
                    });
                });
                anAjax.init();
            },
            init: function(){
                $("input[name=phone]").mask("+7 (999) 999-99-99")
                    .bind("blur", function () {
                        if(this.value.length==0){ $(this).parent().find('abbr').show();}
                    });
                this.ajaxFormSend();
            },
            ajaxFormSend: function(){
                var validateForm = $('form:not(.no-validate)').not('.ajax');
                if(validateForm.length){
                    var noAjaxConfig = $.extend({}, anAjax.validatorConfig, {
                        submitHandler: function(form){
                            form.submit();
                        }
                    });
                    validateForm.each(function() {
                        $(this).validate(noAjaxConfig);
                    });
                }

                var ajaxForm = $('form.ajax');
                if(ajaxForm.length){
                    ajaxForm.each(function() {
                        $(this).validate(anAjax.validatorConfig);
                    });
                }
                anAjax.token();
            },
            validatorConfig: {
                errorPlacement: function(error, element) { },
                highlight: function( element, errorClass, validClass ) {
                    switch( element.type ){
                        case 'radio':{
                            this.findByName( element.name ).addClass( errorClass ).removeClass( validClass );
                            break;
                        }
                        case 'select-one':{
                            $( element ).removeClass( errorClass ).addClass( validClass );
                            if($( element ).parent().hasClass('ik_select')){
                                $( element ).parent().addClass( errorClass ).removeClass( validClass );
                            }
                            break;
                        }
                        default:{
                            $( element ).addClass( errorClass ).removeClass( validClass );
                        }
                    }
                },
                unhighlight: function( element, errorClass, validClass ) {
                    switch( element.type ){
                        case 'radio':{
                            this.findByName( element.name ).removeClass( errorClass ).addClass( validClass );
                            break;
                        }
                        case 'select-one':{
                            $( element ).removeClass( errorClass ).addClass( validClass );
                            if($( element ).parent().hasClass('ik_select')){
                                $( element ).parent().removeClass( errorClass ).addClass( validClass );
                            }
                            break;
                        }
                        default:{
                            $( element ).removeClass( errorClass ).addClass( validClass );
                            break;
                        }
                    }
                },
                submitHandler: function(form){
                    anAjax.sender(form);
                }
            },
            msg: {
                info: function(text, mode){
                    mode = mode || 'modal';
                    switch(mode){
                        case 'alert':{
                            (typeof $.fn.jGrowl !== 'undefined') ? $.jGrowl(text, {theme: 'msg-info'}) : alert(text);
                            break;
                        }
                        default:{
                            anAjax.fancy(text);
                        }
                    }
                },
                notice: function(text, mode){
                    mode = mode || 'modal';
                    switch(mode){
                        case 'alert':{
                            (typeof $.fn.jGrowl !== 'undefined') ? $.jGrowl(text, {theme: 'msg-success'}) : alert(text);
                            break;
                        }
                        default:{
                            anAjax.fancy(text);
                        }
                    }
                },
                error: function(text, mode){
                    mode = mode || 'modal';
                    switch(mode){
                        case 'alert':{
                            (typeof $.fn.jGrowl !== 'undefined') ? $.jGrowl(text, {theme: 'msg-error'}) : alert(text);
                            break;
                        }
                        default:{
                            anAjax.fancy('<div class="errorMessage"><h1>Произошла ошибка</h1><br />' + text + '</div>');
                        }
                    }
                },
                fail: function(text, mode){
                    mode = mode || 'modal';
                    switch(mode){
                        case 'alert':{
                            (typeof $.fn.jGrowl !== 'undefined') ? $.jGrowl(text, {theme: 'msg-error'}) : alert(text);
                            break;
                        }
                        default:{
                            anAjax.fancy('<div class="errorMessage"><h1>Произошла ошибка</h1><br />' + text + '</div>', true);
                        }
                    }
                }
            },
            fancy: function(text, reload){
                $.event.trigger("onFancyOpen", [{content: text}]);
                $.fancybox({
                    content: text,
                    overlayColor:"#25437d",
                    overlayOpacity: 0.40,
                    autoWidth: true,
                    autoResize: true,
                    scrolling: 'auto',
                    ajax: {
                        dataType : 'html',
                        headers  : {
                            'X-fancyBox': true,
                            'X-ANSender': true
                        }
                    },
                    openSpeed : 450,
                    openEffect:'fade',
                    closeEffect:'fade',
                    closeSpeed : 50,
                    afterClose: function () {
                        if(reload){
                            location.reload();
                        }
                    }
                });
            },
            token: function(){
                var url = '',
                    el = $('meta[name=csrf_token]'),
                    _token = el.attr('content');
                if(el.length){
                    url = $.jHelper.defaultData(el, 'reload', '');
                }
                if(url !== ''){
                    $.ajax({
                        type: 'GET',
                        url: url ,
                        async: false,
                        global: false,
                        success: function (token) {
                            _token = token;
                            el.attr('content', token);
                        }
                    });
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': _token
                    }
                });
                return _token;
            },
            sender: function(form, callback){
                if (typeof callback === 'undefined') callback = $.noop;
                anAjax.token();
                $.ajax({
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    cache: false,
                    type: $(form).attr('method')
                }).done(function (responce) {
                    var msg = (typeof responce.text !== 'undefined') ? responce.text : anAjax.lang.errorSend,
                        mode = typeof responce.alert !== 'undefined' && responce.alert ? 'alert' : '';
                    if(typeof responce.status !== 'undefined' && responce.status == 'ok'){
                        if ($.isFunction( callback )) {
                            callback.call(this, responce);
                        }
                        var el = (typeof responce.replace !== 'undefined') ? responce.replace : null;
                        if(el && $(el).length){
                            el = $(el);
                            el.fadeToggle('slow', function(){
                                el.replaceWith(responce.text)
                            }).fadeToggle('slow');
                        }else{
                            var url = (typeof responce.url !== 'undefined') ? responce.url : null;
                            if(url){
                                $(location).attr('href', url);
                            }else{
                                if(msg){
                                    if(typeof responce.reload !== 'undefined' && responce.reload ? responce.reload : false) {
                                        anAjax.fancy(msg, true);
                                    }else{
                                        anAjax.msg.notice(msg, mode);
                                    }
                                }
                            }
                        }
                    }else{

                        anAjax.msg.error(msg, mode);
                    }
                }).fail(function () {
                    anAjax.msg.error(anAjax.lang.errorSend);
                });
            }
        };
        return anAjax;
    })();
})(window,window.document,jQuery);