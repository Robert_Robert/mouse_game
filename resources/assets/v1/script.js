(function($){
    $(function() {
        if (typeof $.fn.jGrowl !== 'undefined') {
            $.jGrowl.defaults.closerTemplate = '<div>[ Закрыть ]</div>';
            $.jGrowl.defaults.life = 10000;
        }

        window.anAjax.install();

        /** Кнопки увеличения и уменьшения кол-ва товара */
        require('./requireJS/elements/counter.js');

        /** Сворачивание и разворачивание элементов */
        require('./requireJS/elements/spoiler.js');

        /** Выпадающие списки (сортировка) */
        require('./requireJS/elements/select.js');

        /** Переключение табов */
        require('./requireJS/elements/tabs.js');

        /** Поиск */
        require('./requireJS/elements/search.js');

        /*
        var request_uri = location.pathname + location.search;
         console.log(request_uri)
         if(request_uri == '/watch_dogs.php'){
            $('.body_new').css('background', '#fff url(/images/body_bg_whatchdogs.jpg) no-repeat top center');
            $('#wrapper_all').addClass('watch_dogs');
            $('header').addClass('watch_dogs');
            $('body #wrapper_all .wrapper').addClass('watch_dogs');
            $('header .wrapper').removeClass('watch_dogs');
         }
         if(request_uri == '/game_page-w.php'){
            $('.body_new').css('background', '#fff url(/images/body_bg_whatchdogs.jpg) no-repeat top center');
            $('#wrapper_all').addClass('watch_dogs');
            $('header').addClass('watch_dogs');
            $('body #wrapper_all .wrapper').addClass('watch_dogs').css('margin-top',110);
            $('header .wrapper').removeClass('watch_dogs').css('margin-top',0);
         }*/

        switch(pageType) {
            case 'index':{
                require('./requireJS/pages/index.js');
                break;
            }
            case 'game':{
                require('./requireJS/pages/game.js');
                break;
            }
            case 'cart':{
                require('./requireJS/pages/cart.js');
                break;
            }
            default:{
                break;
            }
        }

        $('a.fancy').fancybox({
            'nextEffect': 'fade',
            'prevEffect': 'elastic'
        });

        $("img.lazy").lazyload({
            event: "scrollstop",
            effect : "fadeIn"
        });

        $('form input[type="text"],form input[type="password"],form textarea').bind('focus',function(e){
            if(this.value.length<=0){
                $(this).parent().find('abbr').hide();
            }
        }).bind('blur',function(e){
            if(this.value.length==0){
                $(this).parent().find('abbr').show();
            }
        });

        /*$('a[href^="#"]').click(function (e) {
            e.preventDefault();

            var elementClick = $(this).attr("href").substr(1);
            if(elementClick == ''){
                elementClick = 'top';
            }
            var destination = $('a[name="' + elementClick + '"]');
            if( ! destination.length){
                destination = $('#' + elementClick);
            }
            if(destination.length) {
                $('html, body').animate({scrollTop: destination.offset().top}, 1500);
            }
        });*/
    });
})(jQuery);