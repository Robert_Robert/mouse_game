if($(".cartTable").length && $("#cartForm").length){
    var $cartTable = $('.cartTable'),
        $cartForm = $("#cartForm");
    $('form .counter input[type="text"]', $cartTable).on('change', function(e){
        e.preventDefault();
        var form = $(this).closest('form').clone(),
            method = $('input[name=method]:checked', $cartForm).val();
        form.append('<input type="hidden" name="method" value="' + method +'" />');
        anAjax.sender(form, function(responce){
            if(responce.count.positions != $('.product', $cartTable).length) {
                location.reload();
            }else {
                $.event.trigger("onCartPageUpdate", [{responce: responce}]);
            }
        });
   });
    $(".removeItem a", $cartTable).on("click", function(e){
        e.preventDefault();
        var _self = $(this);
        anAjax.token();
        $.ajax({
            url: _self.attr('href'),
            data: {
                method: $('input[name=method]:checked', $cartForm).val()
            },
            cache: false,
            type: 'GET'
        }).done(function (responce) {
           var mode = typeof responce.alert !== 'undefined' && responce.alert ? 'alert' : '';
            var msg = (typeof responce.text !== 'undefined') ? responce.text : anAjax.lang.errorSend;
            if(typeof responce.status !== 'undefined' && responce.status == 'ok'){
                _self.closest('tr').detach();
                if(responce.count.positions == 0 || responce.count.positions != $('.product', $cartTable).length) {
                    location.reload();
                }else {
                    $.event.trigger("onCartPageUpdate", [{responce: responce}]);
                    anAjax.msg.notice(msg, mode);
                }
            }else{
                anAjax.msg.error(msg, mode);
            }
        }).fail(function () {
            anAjax.msg.error(anAjax.lang.errorSend);
        });
    });

    $('.payments .input span', $cartForm).on('click', function(e){
        e.preventDefault();
        var _self = $(this);

        $('.payments .input span', $cartForm).removeClass('checked');
        $(this).toggleClass('checked');
        $('input[type=radio]', _self).prop('checked', $(this).hasClass('checked'));

        $.ajax({
            url: _self.attr('href'),
            data: {
                method: $('input[name=method]:checked', $cartForm).val()
            },
            cache: false,
            type: 'GET'
        }).done(function (responce) {
            var mode = typeof responce.alert !== 'undefined' && responce.alert ? 'alert' : '';
            var msg = (typeof responce.text !== 'undefined') ? responce.text : anAjax.lang.errorSend;
            if(typeof responce.status !== 'undefined' && responce.status == 'ok'){
                if(responce.count.positions == 0 || responce.count.positions != $('.product', $cartTable).length) {
                    location.reload();
                }else {
                    $.event.trigger("onCartPageUpdate", [{responce: responce}]);
                    anAjax.msg.notice(msg, mode);
                }
            }else{
                anAjax.msg.error(msg, mode);
            }
        }).fail(function () {
            anAjax.msg.error(anAjax.lang.errorSend);
        });
    });

    $cartForm.on('submit', function(e){
        e.preventDefault();
        anAjax.sender($cartForm, function(responce){
            if(responce.count.positions != $('.product', $cartTable).length) {
                location.reload();
            }else {
                $.event.trigger("onCartPageUpdate", [{responce: responce}]);
            }
        });
    });

    $(document).on("onCartPageUpdate", function(e, content){
        e.preventDefault();

        if($('#miniCart .total').length) $('#miniCart .total').text(content.responce.count.quantity);
        if($("#count_quantity").length) $("#count_quantity").text(content.responce.count.quantity);
        if($("#sum_price").length) $("#sum_price").text(content.responce.sum.price);
        if($("#sum_commission").length) $("#sum_commission").text(content.responce.sum.commission);
        if($("#sum_pay").length) $("#sum_pay").text(content.responce.sum.pay);
    });
}