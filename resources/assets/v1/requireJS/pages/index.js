$('.main_slider .wrp').bxSlider({
    auto: true,
    controls: true,
    autoControls: false,
    autoHover: true,
    mode: 'fade',
    //mode: 'horizontal',
    //useCSS: false,
    pause: 5000,
    //speed: 500,
    tickerHover: true
});

$(".main_slider .bx-viewport, .main_slider .bx-controls-direction").hover( function(){
    $('.main_slider .bx-controls-direction').toggleClass('hover-bx-controls');
});