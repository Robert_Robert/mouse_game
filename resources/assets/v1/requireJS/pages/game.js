if($('.game_pageslider .thumbs .thumb').length){
    $('.game_pageslider .thumbs .thumb').on('click', function(e){
        e.preventDefault();
        var _self = this;
        var data = $(_self).data();
        var tpl = '';
        if(data.type == 'image'){
            tpl = $.jHelper.renderTPL('<a href="<%=full%>" class="fancy" rel="slider" title="<%=title%>"><img src="<%=main%>" alt="<%=title%>" title="<%=title%>" /></a>', data);
            $('.game_pageslider .thumbs .thumb').each(function(){
                var sliderData = $(this).data();
                if(sliderData.full != data.full ){
                    tpl += $.jHelper.renderTPL('<a href="<%=full%>" class="fancy hide" rel="slider" title="<%=title%>"></a>', sliderData);
                }
            });

        }else{
            tpl = $.jHelper.renderTPL('<div class="embedVideo" style="background: url(\'<%=main%>\')">' +
                '<iframe src="<%=embed%>" width="<%=width%>" height="<%=height%>" frameborder="0" allowfullscreen></iframe>' +
            '</div>', data);
        }
        $('.game_pageslider .mainimage').html(tpl);
    });
    $('body').on('click', '.mainimage .embedVideo', function(e){
        e.preventDefault();
        var $iframe = $('iframe', $(this));

        var videoURL = $iframe.prop('src');
        videoURL += "&autoplay=1";
        $iframe.prop('src',videoURL);
        $(this).html($iframe);
        $(this).addClass('play');
    });
}

if($('#addItemToCart').length){
   $('#addItemToCart').on('click', function(e){
       e.preventDefault();
       anAjax.sender(this.closest('form'), function(responce){
           $('#miniCart .total').text(responce.count.quantity);
       });
   });
}