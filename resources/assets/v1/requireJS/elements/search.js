if($('#query_field_search_form').length) {
    var search = $('#query_field_search_form');
    search.autocomplete({
        serviceUrl: search.parents('form').attr('action'),
        noSuggestionNotice: 'Ничего не найдено',
        showNoSuggestionNotice: true,
        autoSelectFirst: false,
        preventBadQueries: false,
        triggerSelectOnValidInput: false,
        width: 366,
        maxHeight: 600,
        noCache: true,
        ajaxSettings: {global: false},
        onSelect: function (suggestion) {
            $(this).val("").focus();
            window.location.href = suggestion.data.url;
        },
        formatResult: function (suggestion, currentValue) {
            var pattern = '(' + currentValue.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + ')';
            var data = suggestion.data;
            var out = data.title;
            var price = '';
            if (data.price) {
                if (data.status.buy) {
                    price = '<div class="search-price">' +
                    (data.fullprice > 0 ? ('<s>' + data.price + '</s> ' + data.fullprice) : data.price) +
                    '<span class="rub">руб.</span>' +
                    '</div>';
                    //(data.fullprice > 0 ? '<span class="price-sale">Скидка ' + data.sale + '%</span>' : '');
                }
                price += '<div class="search-status status-' + data.status.slug + '"></div>';
            }
            if (price != '') {
                out = '<div class="can-buy-' + data.status.buy + '">' +
                '<div class="search-image">' +
                '<img src="' + data.image + '" />' +
                '</div>' +
                '<div class="search-info">' +
                '<div class="search-title">' +
                data.title.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>') +
                '</div>' +
                '<div class="search-data">' +
                price +
                '</div>' +
                '</div>' +
                '</div>';
            }
            return out;
        }
    });
}