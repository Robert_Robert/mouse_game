/** Выпадающие списки (сортировка) */

$('.select input, .select .ar').click(function(){
    var  select_input = $('.type', $(this).parent());
    var ul =$('ul',$(this).parent());
    select_input.css('border-bottom-left-radius', '0');
    ul.show();
    var firstClick = true;
    $(document).bind('click.myEvent', function(e) {
        if (!firstClick && $(e.target).closest(ul.parents('.select')).length == 0) {
            ul.hide();
            select_input.css('border-bottom-left-radius', '5px');
            $(document).unbind('click.myEvent');
        }
        firstClick = false;
    });
});
$('.select ul li').click(function(){
    var input = $('input[type=text]', $(this).parents('.select'));
    input.val($(this).text());

    var select = $.jHelper.defaultData($(this), 'value', $(this).text());

    input = $('input[type=hidden]', $(this).parents('.select'));
    input.val(select);
    $.cookie(input.attr('name'), select);

    $(this).parent().hide();
    $('.type', $(this).parents('.big')).css('border-bottom-left-radius', '5px');
    $(document).unbind('click.myEvent');

    location.reload();
});