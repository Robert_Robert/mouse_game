$('form .counter span').click(function (e) {
    e.preventDefault();
    var but = $(this).attr('class'),
        inp = $(this).parent().find('input[type="text"]'),
        c = parseInt(inp.val());

    if(!c){c = parseInt(0);}
    if(but == 'minus'){
        c-=1;
        if(c == '0'){c=1}
    }
    if(but == 'plus'){
        c+=1
    }
    inp.val(c).change();
});

$('form .counter input[type="text"]').keypress(function(e){
    return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
});