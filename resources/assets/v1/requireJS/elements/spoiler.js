/***
 * Прячем текст в контенте под спойлер:
 * <p><a href="#text" data-parent="1" class="show_hidden">more ...</a></p>
 * <div id="text" class="hide">text</div>
 * При клике на ссылку, она спрячется вместе с параграфом. А скрытый текст покажется.
 */
$('.show_hidden').click(function(e){
    e.preventDefault();
    var selector = $(this).attr('href');
    $.jHelper.defaultData(this, 'parent', 0) ? $(this).parent().hide() : $(this).hide();

    var top = $(selector).show().offset().top;
    //$('body,html').animate({scrollTop: top}, 700);
});


$('.content .more_next').click(function(e){
    e.preventDefault();
    $('.content_hide').show();
    $(this).remove();
});

/***
 * Показываем параметры игры (на странице игры). Виджет moreNext
 */
$('.params .more_next').click(function(e){
    e.preventDefault();
    $('.hide', $(this).parent()).show();
    $(this).remove();
});