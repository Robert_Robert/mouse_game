/**
 * Переключение табов на странице с игрой
 */
$('.page_filter a:not(.link)').click(function(e){
    e.preventDefault();
    var tab = $(this).parent();
    var id = $(this).attr('href');
    if (tab.hasClass('active')) { return false ;}
    else {
        $('.page_filter li, .filter_content div').removeClass('active');
        tab.addClass('active');
        $('div'+id).addClass('active');
    }
});