<!DOCTYPE html>

<html lang="ru">

<head>

	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<title>MOUSEGAME</title>

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="{{ elixir("css/v1-fonts.css") }}">
	<link rel="stylesheet" href="{{ elixir("css/v1-core.css") }}">
	<link rel="stylesheet" href="{{ elixir("css/v1-app.css") }}">
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	<script src="{{ elixir("js/v1-core.js") }}"></script>
	<script>
		var pageType = 'index';
	</script>
	<script src="{{ elixir("js/v1-app.js") }}"></script>
</head>
<body class="body_new">

	<div class="wrapper-border">

		<div id="wrapper_all">
			<div class="top_panel"></div>
			<header>
				<div class="wrapper">
					<div class="logo"> <a href="/"><img src="/themes/v1/assets/img/logo.svg" title="на главную" alt=""/></a> </div>
					<div class="login">
						<div class="not_log">
							<a href="/login.php" id="login">вход</a>/<a href="/registration.php" id="registration" >регистрация</a>
						</div>
					</div>
					<div class="search"><input type="text" value="Поиск..." plaseholder="Поиск..." /></div>
					<div class="social"><a href="#" class="tw"></a><a href="#" class="vk"></a><a href="#" class="google"></a><a href="#" class="fb"></a></div>
					<div class="menu_block">
						<div class="filter">
							<div class="top">
								<nav>
									<a class="all" href="/web_letter.php?page=Все">Все</a>
									<a href="/web_letter.php?page=А">A</a>
									<a href="/web_letter.php?page=Б">Б</a>
									<a href="/web_letter.php?page=В">В</a>
									<a href="/web_letter.php?page=Г">Г</a>
									<a href="/web_letter.php?page=Д">Д</a>
									<a href="#" class="not">Е</a>
									<a href="#" class="not">Ж</a>
									<a href="#" class="not">З</a>
									<a href="/web_letter.php?page=И">И</a>
									<a href="/web_letter.php?page=К">К</a>
									<a href="Л" class="not">Л</a>
									<a href="/web_letter.php?page=М">М</a>
									<a href="/web_letter.php?page=Н">Н</a>
									<a href="/web_letter.php?page=О">О</a>
									<a href="/web_letter.php?page=П">П</a>
									<a href="/web_letter.php?page=Р">Р</a>
									<a href="/web_letter.php?page=С">С</a>
									<a href="/web_letter.php?page=Т">Т</a>
									<a href="#" class="not">У</a>
									<a href="/web_letter.php?page=Ф">Ф</a>
									<a href="/web_letter.php?page=Х">Х</a>
									<a href="#" class="not">Ц</a>
									<a href="/web_letter.php?page=Ч">Ч</a>
									<a href="/web_letter.php?page=Ш">Ш</a>
									<a href="/web_letter.php?page=Щ">Щ</a>
									<a href="/web_letter.php?page=Э">Э</a>
									<a href="#" class="not">Ю</a>
									<a href="#" class="not">Я</a>
								</nav>
							</div>
							<div class="bottom">
								<nav>
									<a href="/web_letter.php?page=0..9" class="all">0..9</a>
									<a href="/web_letter.php?page=A">A</a>
									<a href="/web_letter.php?page=B">B</a>
									<a href="/web_letter.php?page=C">C</a>
									<a href="/web_letter.php?page=D">D</a>
									<a href="/web_letter.php?page=E">E</a>
									<a href="/web_letter.php?page=F">F</a>
									<a href="/web_letter.php?page=G">G</a>
									<a href="/web_letter.php?page=H">H</a>
									<a href="/web_letter.php?page=I">I</a>
									<a href="/web_letter.php?page=J">J</a>
									<a href="/web_letter.php?page=K">K</a>
									<a href="/web_letter.php?page=L">L</a>
									<a href="/web_letter.php?page=M">M</a>
									<a href="/web_letter.php?page=N">N</a>
									<a href="/web_letter.php?page=O">O</a>
									<a href="/web_letter.php?page=P">P</a>
									<a href="#" class="not">Q</a>
									<a href="/web_letter.php?page=R">R</a>
									<a href="/web_letter.php?page=S">S</a>
									<a href="/web_letter.php?page=T">T</a>
									<a href="/web_letter.php?page=U">U</a>
									<a href="/web_letter.php?page=V">V</a>
									<a href="/web_letter.php?page=W">W</a>
									<a href="/web_letter.php?page=X">X</a>
									<a href="/web_letter.php?page=Y">Y</a>
									<a href="/web_letter.php?page=Z">Z</a>
								</nav>
							</div>
						</div>
						<div class="mainmenu">
							<nav>
								<ul>
									<li><a href="/content_page.php">О магазине</a></li>
									<li><a href="/support.php">Поддержка</a></li>
									<li><a href="/content_page.php">FAQ</a></li>
								</ul>
							</nav>
						</div>
						<a href="/cart.php" class="minicart">
							<div class="total">0</div>
						</a>
					</div>
				</div>
			</header>
			<div class="wrapper">
				<aside id="left_sidebar">
					<div class="game_menu">
						<div class="title">ЖАНРЫ</div>
						<nav>
							<ul>
								<li id="all"><a href="/game_menu_el.php?page=Все игры">Все игры</a></li>
								<li id="actions"><a href="/game_menu_el.php?page=Action/Шутеры">Action/Шутеры</a></li>
								<li id="rpg"><a href="/game_menu_el.php?page=RPG">RPG</a></li>
								<li id="sport"><a href="/game_menu_el.php?page=Спортивные">Спортивные</a></li>
								<li id="rts"><a href="/game_menu_el.php?page=Стратегии">Стратегии</a></li>
								<li id="racing"><a href="/game_menu_el.php?page=Гонки">Гонки</a></li>
								<li id="sim"><a href="/game_menu_el.php?page=Симуляторы">Симуляторы</a></li>
								<li id="others"><a href="/game_menu_el.php?page=Другие">Другие</a></li>
							</ul>
						</nav>
					</div>
					<div class="boards_menu">
						<div class="title">ПЛАТФОРМЫ</div>
						<nav>
							<ul>
								<li id="Steam"><a href="/game_menu_el.php?page=Steam">Steam</a></li>
								<li id="Origin"><a href="/game_menu_el.php?page=Origin">Origin</a></li>
								<li id="Mac"><a href="/game_menu_el.php?page=Mac игры">Mac игры</a></li>
								<li id="Xbox"><a href="/game_menu_el.php?page=Xbox LIVE">Xbox LIVE</a></li>
								<li id="PS"><a href="/game_menu_el.php?page=PS Network">PS Network</a></li>
								<li id="iTunes"><a href="/game_menu_el.php?page=iTunes">iTunes</a></li>
							</ul>
						</nav>
					</div>
					<div class="news">
						<div class="title">НОВОСТИ</div>
						<ul class="news_list">
							<li class="item">
								<div class="ttl"><a href="/news_recent.php">Deluxe издания Assassin's Creed IV и Watch Dogs </a></div>
								<div class="intro">Множество интересных бонусов и дополнительное игровое время!</div>
							</li>
							<li class="item">
								<div class="ttl"><a href="/news_recent.php">Borderlands 2 Game of the Year Edition - релиз состоялся!</a></div>
								<div class="intro">Новое издание, содержащее несколько дополнений, наборов и других интересных бонусов!</div>
							</li>
							<li class="item">
								<div class="ttl"><a href="/news_recent.php">XCOM: Enemy Within - старт предзаказов </a></div>
								<div class="intro">Новые навыки, улучшения, оружие, карты, миссии и многое другое!</div>
							</li>
							<li class="item">
								<div class="ttl"><a href="/news_recent.php">Season Pass - теперь и для Assassin’s Creed IV Black Flag</a></div>
								<div class="intro">Эксклюзивный пакет "Корабль Кракена", новая одиночная кампания и многое другое!</div>
							</li>
							<li class="item">
								<div class="ttl"><a href="/news_recent.php">NBA 2K14 - уже в продаже</a></div>
								<div class="intro">Отличный баскетбольный симулятор!</div>
							</li>
							<li class="item">
								<div class="ttl"><a href="/news_recent.php">Teleglitch: Die More Edition - скидка 75%!</a></div>
								<div class="intro">Добро пожаловать в мир ужаса, паранойи и страха!</div>
							</li>
							<li class="item">
								<div class="ttl"><a href="/news_recent.php">Стартовал ранний доступ в War of the Vikings</a></div>
								<div class="intro">Примите участие в яростных схватках на полях сражений эпохи викингов!</div>
							</li>
						</ul>
					</div>
					<div class="email_res">
						<div class="box">
							<div class="title">Подписка на новости</div>
							<div class="email">
								<a href="/rss.php" class="rss"></a>
								<input type="text" value="E-mail" placeholder="Email" />
							</div>
						</div>
					</div>
				</aside>
				<main id="middle">
					<figure>
						<div class="main_slider">
							<div class="wrp">
								<div class="slide">
									<img src="/themes/v1/assets/img/BANNER_1.png" alt=""/>
									<div class="descr">
										<div class="title">Injustice: Gods Among Us (PC)</div>
										<div class="price">499 руб.</div>
									</div>
								</div>
								<div class="slide">
									<img src="/themes/v1/assets/img/BANNER_1.png" alt=""/>
									<div class="descr">
										<div class="title">Injustice: Gods Among Us (PC)</div>
										<div class="price">499 руб.</div>
									</div>
								</div>
								<div class="slide">
									<img src="/themes/v1/assets/img/BANNER_1.png" alt=""/>
									<div class="descr">
										<div class="title">Injustice: Gods Among Us (PC)</div>
										<div class="price">499 руб.</div>
									</div>
								</div>
								<div class="slide">
									<img src="/themes/v1/assets/img/BANNER_1.png" alt=""/>
									<div class="descr">
										<div class="title">Injustice: Gods Among Us (PC)</div>
										<div class="price">499 руб.</div>
									</div>
								</div>
								<div class="slide">
									<img src="/themes/v1/assets/img/BANNER_1.png" alt=""/>
									<div class="descr">
										<div class="title">Injustice: Gods Among Us (PC)</div>
										<div class="price">499 руб.</div>
									</div>
								</div>
							</div>
						</div>
					</figure>
					<aside id="right_sidebar">
						<div class="top10">
							<div class="title">Игры недели</div>
							<ol>
								<li><a href="#">Guild Wars 2 </a></li>
								<li><a href="#">The Elder Scrolls V: ..</a></li>
								<li><a href="#">World of Warcraft: Mi..</a></li>
								<li><a href="#">Guild Wars 2 Gem Card..</a></li>
								<li><a href="#">Playstation Network C..</a></li>
								<li><a href="#">Crysis 3</a></li>
								<li><a href="#">Guild Wars 2 Digital ..</a></li>
								<li><a href="#">World of Warcraft: Ba..</a></li>
								<li><a href="#">World of Warcraft - 6..</a></li>
								<li><a href="#">Citadels</a></li>
							</ol>
							<div class="right_sidebsr_footer">
								<div class="right_sidebar_border"></div>
							</div>
						</div>
					</aside>
					<div class="clr"></div>
					<div class="catalog_block">
						<aside>
							<div class="catalog_filter">
								<ul>
									<li><a href="#">Рекомендации</a></li>
									<li><a href="#">Скидки</a></li>
									<li><a href="#">Новинки</a></li>
									<li><a href="#">Лидеры продаж</a></li>
									<li><a href="#">Предзаказ</a></li>
								</ul>
							</div>
						</aside>

						<article>
							<div class="pager">
								<ul>
									<li class="first"><a href="#">‹‹</a></li>
									<li class="prev"><a href="#"></a></li>
									<li class="current page"><a href="#">1</a></li>
									<li class="page"><a href="#">2</a></li>
									<li class="page"><a href="#">3</a></li>
									<li class="next"><a href="#"></a></li>
									<li class="last"><a href="#">››</a></li>
								</ul>
							</div>
							<div class="sorting">
								<div class="label">Сортировать по </div>
								<div class="select big">
									<div class="ar">▼</div>
									<input value="по популярности" class="type" readonly />
									<ul>
										<li>по популярности</li>
										<li>по цене</li>
										<li>по названию</li>
									</ul>
								</div>
								<div class="select small">
									<div class="ar">▼</div>
									<input value="16" class="count" readonly />
									<ul>
										<li>16</li>
										<li>32</li>
										<li>64</li>
									</ul>
								</div>
								<div class="label"> позиций </div>
							</div>



							<h1>Продажа цифровых лицензий игр</h1>

							<div class="catalog">
                            	<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/MXGP.png" alt=""/>
                            		<div class="descr">
                            			<div class="title">
                            				<div class="cell">MXGP</div>
                            			</div>
                            			<div class="price"><b>349</b> руб.</div>
                            		</div>
                            	</a> </div>
                            	<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/DIABLO_III.png" alt=""/>
                            		<div class="descr">
                            			<div class="title">
                            				<div class="cell">Diablo III:<br />
                            					Reaper of Souls (RU)<br />
                            				</div>
                            			</div>
                            			<div class="price"><b>349</b> руб.</div>
                            		</div>
                            	</a> </div>
                            	<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/BATLEFIELD.png" alt=""/>
                            		<div class="descr">
                            			<div class="title">
                            				<div class="cell">Battlefield 4:<br />
                            					Premium Service</div>
                            				</div>
                            				<div class="price"><b>349</b> руб.</div>
                            			</div>
                            		</a> </div>
                            		<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/TITANFALL.png" alt=""/>
                            			<div class="descr">
                            				<div class="title">
                            					<div class="cell">Titanfall</div>
                            				</div>
                            				<div class="price"><b>349</b> руб.</div>
                            			</div>
                            		</a> </div>
                            		<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/THIEF.png" alt=""/>
                            			<div class="descr">
                            				<div class="title">
                            					<div class="cell">Thief</div>
                            				</div>
                            				<div class="price"><b>349</b> руб.</div>
                            			</div>
                            		</a> </div>
                            		<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/NBA2K14.png" alt=""/>
                            			<div class="descr">
                            				<div class="title">
                            					<div class="cell">NBA2K14</div>
                            				</div>
                            				<div class="price"><b>349</b> руб.</div>
                            			</div>
                            		</a> </div>
                            		<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/BLACKGATE.png" alt=""/>
                            			<div class="descr">
                            				<div class="title">
                            					<div class="cell">Batman:<br />
                            						Arkham Origins</div>
                            					</div>
                            					<div class="price"><b>349</b> руб.</div>
                            				</div>
                            			</a> </div>
                            			<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/BIOSHOCK.png" alt=""/>
                            				<div class="descr">
                            					<div class="title">
                            						<div class="cell">BioShock Infinite</div>
                            					</div>
                            					<div class="price"><b>349</b> руб.</div>
                            				</div>
                            			</a> </div>
                            			<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/RAYMAN.png" alt=""/>
                            				<div class="descr">
                            					<div class="title">
                            						<div class="cell">Rayman Legends</div>
                            					</div>
                            					<div class="price"><b>349</b> руб.</div>
                            				</div>
                            			</a> </div>
                            			<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/SAINTSROW.png" alt=""/>
                            				<div class="descr">
                            					<div class="title">
                            						<div class="cell">Saints Row: The Third</div>
                            					</div>
                            					<div class="price"><b>349</b> руб.</div>
                            				</div>
                            			</a> </div>
                            			<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/CALLOFDUTY.png" alt=""/>
                            				<div class="descr">
                            					<div class="title">
                            						<div class="cell">Call of Duty:<br />
                            							Black Ops </div>
                            						</div>
                            						<div class="price"><b>349</b> руб.</div>
                            					</div>
                            				</a> </div>
                            				<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/SIMS4.png" alt=""/>
                            					<div class="descr">
                            						<div class="title">
                            							<div class="cell">The sims 4</div>
                            						</div>
                            						<div class="price"><b>349</b> руб.</div>
                            					</div>
                            				</a> </div>
                            				<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/DEADISLAND.png" alt=""/>
                            					<div class="descr">
                            						<div class="title">
                            							<div class="cell">Dead Island</div>
                            						</div>
                            						<div class="price"><b>349</b> руб.</div>
                            					</div>
                            				</a> </div>
                            				<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/MURDERED.png" alt=""/>
                            					<div class="descr">
                            						<div class="title">
                            							<div class="cell">Murdered: Soul Suspect</div>
                            						</div>
                            						<div class="price"><b>349</b> руб.</div>
                            					</div>
                            				</a> </div>
                            				<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/SPLINTERSELL.png" alt=""/>
                            					<div class="descr">
                            						<div class="title">
                            							<div class="cell">Tom Clancy’s<br />
                            								Splinter Cell Blacklist</div>
                            							</div>
                            							<div class="price"><b>349</b> руб.</div>
                            						</div>
                            					</a> </div>
                            					<div class="object"> <a href="game_page.php"> <img src="/themes/v1/assets/img/CHIELDOFLIGHT.png" alt=""/>
                            						<div class="descr">
                            							<div class="title">
                            								<div class="cell">Child of Light</div>
                            							</div>
                            							<div class="price"><b>349</b> руб.</div>
                            						</div>
                            					</a> </div>
                            </div>
											<div class="pager">
												<ul>
													<li class="first"><a href="#">‹‹</a></li>
													<li class="prev"><a href="#"></a></li>
													<li class="current page"><a href="#">1</a></li>
													<li class="page"><a href="#">2</a></li>
													<li class="page"><a href="#">3</a></li>
													<li class="next"><a href="#"></a></li>
													<li class="last"><a href="#">››</a></li>
												</ul>
											</div>

											<div><p>Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....Многабукв текста....</p></div>
						</article>
					</div>

				</main>
								<div class="clr"></div>
							</div>
							<div id="footer_guarantor"></div>
						</article>
					</div>
					<footer>
						<div class="f_box">
							<div class="wrapper">
								<div class="payments">
									<a href="#" class="visa"><img src="/themes/v1/assets/img/footer_VISA.svg" alt=""/></a>
									<a href="#" class="masterCard"><img src="/themes/v1/assets/img/footer_MASTERCARD.svg" alt=""/></a>
									<a href="#" class="webmoney"><img src="/themes/v1/assets/img/footer_WEBMONEY.svg" alt=""/></a>
									<a href="#" class="kiwi"><img src="/themes/v1/assets/img/footer_QIWI.svg" alt=""/></a>
									<a href="#" class="mts"><img src="/themes/v1/assets/img/MTS_logo.png" alt=""/></a>
									<a href="#" class="megafon"><img src="/themes/v1/assets/img/footer_MEGAFON.svg" alt=""/></a>
									<a href="#" class="yandex"><img src="/themes/v1/assets/img/footer_YANDEX.svg" alt=""/></a>
								</div>
								<div class="to_top">
									<a href="#"></a>
								</div>
								<div class="copy">
									MOUSEGAME.ru распространяет только легальный контент<br />&copy; 2014. MG.
								</div>
								<div class="top"></div>
							</div>
						</div>
					</footer>
					<script src="{{ elixir("js/v1-app.js") }}"></script>
				</body>
				</html>