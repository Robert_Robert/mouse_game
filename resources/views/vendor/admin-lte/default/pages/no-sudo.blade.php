@extends(AdminTemplate::view('_layout.base'))

@section('body', '<body class="login-page">')

@section('content')
	<div class="login-box">
		<div class="login-logo">
			<b>{{ config('admin.title') }}</b>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Вам запрещен доступ в панель администрирования.</p>
		</div>
	</div>
@stop
