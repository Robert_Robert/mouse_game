<!DOCTYPE html>
<html lang="ru-RU">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>[404] Страница не найдена</title>
<link rel="shortcut icon" href="{{ asset("favicon.ico") }}" type="image/x-icon">
<link rel="stylesheet" href="{{ asset("themes/v1/assets/fonts.min.css") }}">
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<style>
html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center,  dl, dt, dd, ol, ul, li, fieldset, form, label, legend, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary, time, mark, audio, video {margin: 0;padding: 0;border: 0;outline: 0;vertical-align: baseline;background: transparent;font-size: 100%;}
* {margin: 0;padding: 0;}
td, td img {vertical-align: top;}
img{vertical-align:bottom}
ul,ol{list-style:none}
html,body,ul,ol,li,p,td,h1,h2,h3,h4,h5,h6,form,fieldset,a,img,iframe{margin:0;padding:0;border:0}
html,body{font-family: 'Trebuchet MS', sans-serif; font-weight:400;color:#7d7d7d;font-size: 12px;line-height: 24px;background: url('/themes/v1/assets/images/body_bg_simple.png') top center repeat-y;}
.wrap{width:758px;height: 312px;padding: 20px;position: absolute;margin: auto;top: 0;left: 0;bottom: 0;right: 0;background:#fff;border: 1px solid #b7c4d1;  }
.wrap .background{height: 190px;background: url('/themes/v1/assets/images/logo.svg') 20px 20px no-repeat;background-size: 760px;}
.wrap .message{position: relative;top: 20px;left: 115px;width: 540px;text-align: center; color: #999; }
.wrap .message h1{font-weight: 700;color: #cfcfcf;font-size: 28px;line-height: 34px;display: inline-block;}
a{color: #999; border: 0; text-decoration: underline; cursor: auto;}
a:hover{color:#63acdc;}
</style>
</head>
<body>
    <div class="wrap">
        <div class="background"></div>
        <div class="message">
        <h1>Страница не найдена</h1>
            <p>Попробуйте открыть <a href="{{ action('\App\Http\Controllers\TextController@getIndex') }}" title="Главная">главную</a> страницу сайта и найти интересующую вас информации при помощи навигационного меню.</p>
        </div>
    </div>
</body>
</html>