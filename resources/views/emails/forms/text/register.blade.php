Ваше имя: {{ $user->name }}
Контактый e-mail: {{ $user->email }}

Для активации учетной записи вам необходимо открыть ссылку {{ action('\App\Http\Controllers\AuthController@getConfirmEmail', ['token' => $user->confirmation_code]) }}