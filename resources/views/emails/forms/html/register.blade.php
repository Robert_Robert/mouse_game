<table>
    <tr><td><strong>Ваше имя:</strong></td><td>{{ $user->name }}</td></tr>
    <tr><td><strong>Контактный e-mail:</strong></td><td>{{ $user->email }}</td></tr>
</table>
<p>Для активации учетной записи вам необходимо открыть ссылку <a href="{{ action('\App\Http\Controllers\AuthController@getConfirmEmail', ['token' => $user->confirmation_code]) }}">{{ action('\App\Http\Controllers\AuthController@getConfirmEmail', ['token' => $user->confirmation_code]) }}</a></p>