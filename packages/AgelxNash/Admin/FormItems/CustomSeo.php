<?php namespace AgelxNash\Admin\FormItems;

use App\Models\Seo as SeoModel;
use App\Models\Keyword as KeywordModel;
use SleepingOwl\Admin\AssetManager\AssetManager;

Class CustomSeo extends Seo{

	public function __construct(SeoModel $seoModel, KeywordModel $keyModel){
		$this->seoModel = $seoModel;
		$this->keyModel = $keyModel;
	}

	public function initialize()
	{
		parent::initialize();

		AssetManager::addStyle(asset('packages/agelxnash/admin/colorbox/colorbox.css'));
		AssetManager::addStyle(asset('packages/barryvdh/elfinder/css/elfinder.min.css'));
		AssetManager::addStyle(asset('packages/barryvdh/elfinder/css/theme.css'));

		AssetManager::addScript(asset('packages/agelxnash/admin/colorbox/jquery.colorbox-min.js'));
		AssetManager::addScript(asset('packages/agelxnash/admin/colorbox/i18n/jquery.colorbox-'.config('app.locale').'.js'));
		AssetManager::addScript(asset('packages/barryvdh/elfinder/js/elfinder.min.js'));
		AssetManager::addScript(asset('packages/agelxnash/admin/js/elfinderPopUp.js'));
	}
}