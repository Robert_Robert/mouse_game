<?php
return [
	'sort' => [
		'default' => 'rate',
		'values' => [
			'по популярности' => 'rate',
			'по цене' => 'realprice',
			'по названию' => 'title'
		]
	],
	'display' => [
		'default' => 16,
		'values' => [
			16,
			32,
			64
		]
	]
];