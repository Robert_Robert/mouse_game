<?php namespace AgelxNash\Sorter;

use Illuminate\Config\Repository;

class Sorter{
	/**
	 * @var \Illuminate\Config\Repository
	 */
	protected $config;
	protected $options = [];
	protected $data = [];

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Config\Repository     $config
	 */
	public function __construct(Repository $config)
	{
		$this->config = $config;
		$this->loadConfig();
	}

	/**
	 * @return array
	 */
	public function loadConfig()
	{
		$this->options = $this->config->get('sorter');
		foreach($this->options as $key => $data){
			$default = get_key($data, 'default', null, 'is_scalar');
			$values = get_key($data, 'values');
			$this->data[$key] = get_key($_COOKIE, $key, $default, function ($val) use ($values) {
				return (empty($values) || (is_scalar($values) && $val == $values) || (is_array($values) && in_array($val, $values)));
			});
		}
	}

	public function title($key){
		$val = $this->get($key);
		$values = $this->values($key);
		$key = (string)get_key(array_copy_key($values), $val, $val, 'is_scalar');
		return (string)get_key((is_assoc($values) ? array_flip($values) : $values), $key, $val, 'is_scalar');
	}
	public function values($key){
		return $this->config->get('sorter.'.$key.'.values');
	}
	public function get($key){
		return is_scalar($key) ? get_key($this->data, $key, null, 'is_scalar') : null;
	}
	public function set($key, $val){
		$this->data[$key] = $val;
		return $val;
	}
}