<?php namespace AgelxNash\Sorter;

use Illuminate\Support\ServiceProvider;

class SorterServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//Не будем публиковать
		/*$configPath = __DIR__.'/../config/sorter.php';

		// Publish config.
		$this->publishes([$configPath => config_path('sorter.php')], 'config');*/
	}

	public function provides()
	{
		return ['sorter'];
	}

	public function register()
	{
		$configPath = __DIR__.'/../config/sorter.php';
		$this->mergeConfigFrom($configPath, 'sorter');

		$this->app->singleton('sorter', function($app) {
			return new Sorter($app['config']);
		});
	}

}
