<?php
namespace AgelxNash\Sorter\Facades;

use Illuminate\Support\Facades\Facade;

class Sorter extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'sorter';
	}
}